module.exports = {
	extends: '@wpackio',
	rules: {
		"react/prefer-stateless-function": "off",
		"react/jsx-filename-extension": "off",
		"react/no-danger": "off",
		"prefer-destructuring": "off",
		"react/destructuring-assignment": "off",
		"react/prop-types": "off",
		"import/prefer-default-export": "off",
		"react/jsx-props-no-spreading": "off"
	}
};
