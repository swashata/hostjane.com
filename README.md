## DOCUMENTATION

#### To change things on New Sidebar (HOMEPAGE)

Edit `gatsby-config.js`

Change things inside `mainSidebarNav` directive.

```js
module.exports = {
	siteMetadata: {
		// ...
		mainSidebarNav: [
			{
				link: 'https://hostjane.com/marketplace/categories',
				internal: false,
				icon: 'categoriesIcon',
				label: 'Explore',
			},
		],
		// ...
	},
};
```

#### To add _NEW_ or Custom badge on sliders (HOMEPAGE)

Open
`site-data/home/{design-art-video-slider|web-mobile-sw-slider}/contents/*/content.mdx`.
Right now their frontmatter is something like

```md
---
title: Web Designers
link: https://www.hostjane.com/marketplace/categories/design-art-video-voice/graphic-design-logos
image: ./image.jpg
order: 1
---

From wireframing to UI/UX, planning & design, build amazing websites.
```

Change it to

```diff
---
title: Web Designers
link: https://www.hostjane.com/marketplace/categories/design-art-video-voice/graphic-design-logos
image: ./image.jpg
order: 1
+ isNew: true
+ newBadge: NEW
---

From wireframing to UI/UX, planning & design, build amazing websites.
```

#### Change URL of Explore Button on Sliders (HOMEPAGE)

Open `site-data/home/{design-art-video-slider|web-mobile-sw-slider}/index.js`

Change the section that says

```js
export const explore = {
	url: 'https://hostjane.com/marketplace/',
	label: 'Explore all',
	internal: false,
};
```

To hide explore button altogether

```js
export const explore = {
	url: null, // give null value to URL
	label: 'Explore all',
	internal: false,
};
```

#### To Change HERO section of HOSTING page

Edit `site-data/hosting/telstra-hero/content.mdx`

```md
---
heading: Ultra-fast web hosting from $10 / month
linkLabel: Configure your server
image: ./heroimage.jpg
---

Managed cPanel or WordPress VPS with free backups or us self-managed linux VPS.
```

To change the image, replace the `heroimage.jpg`, but keep the aspect ratio
same.

To Change the buttons and links, change the content of
`site-data/hosting/telstra-hero/buttons.js`

```js
export const buttons = [
	{
		internal: true,
		url: '/hosting/vps-hosting-wizard/',
		icon: LaunchVPSIcon,
		label: 'Launch a VPS',
	},
];
```

#### To Change BUILD YOUR SERVER PLAN on HOSTING page

Edit `site-data/hosting/vps-box/index.js` and make changes there.

To preselect anything on the VPS Wizard, add URL Query parameters, like

> `/hosting/vps-hosting-wizard/?appType=0&app=0`

-   `appType` - Starting from 0, it chooses the main tab.
-   `app` (_optional_) - Starting from 0, it chooses the application available
    under the main tab.

#### To Change SCREENSHOT Section of HOSTING Page

> Manage from anywhere

-   To change the title and subtitle, edit
    `site-data/hosting/screenshots/index.js`.
-   To change content of screenshot slides, edit
    `site-data/hosting/screenshots/contents/**/{content.mdx,image.png}`.

Use any image size that suits you, square or otherwise. It will fit in
automatically.

To change the order of content, the directory name
`contents/{1|2|3}/content.mdx` **DOES NOT** matter. Rather edit the frontmatter
of `content.mdx`.

```diff
---
title: Keep it all together
- order: 1
+ order: 3
image: ./image.png
---

Unify your conversations across channels, like chat, email, voice and messaging
for faster replies.
```

Lower number will appear on top.

#### TO Change FAQs of HOSTING Page

-   To change the title edit `site-data/hosting/faqs/index.js`.
-   To change content of screenshot slides, edit
    `site-data/hosting/faqs/contents/**/{content.mdx}`.

To change the order of content, the directory name
`contents/{1|2|3}/content.mdx` **DOES NOT** matter. Rather edit the frontmatter
of `content.mdx`.

```diff
---
title: How do I sign up for the free trials?
- order: 1
+ order: 4
---

New customers to DigitalOcean with a valid credit card are eligible. You are
eligible if you have never been a paying customer of DigitalOcean and have not
previously signed up for the free trial.
```

Lower number will appear on top.

#### Remove Alert (Free Backups...) on HOSTING Page

Edit `form-data/index.js` and change `alertMessage` to null.

```diff
const data = {
	alertIcon: 'hostjane',
	alertTitle: 'Free Backups. 24/7 Support.',
-	alertMessage: 'Use our hosting wizards to launch a server. View <a href="https://support.hostjane.com/servers">hosting features</a> and <a href="https://support.hostjane.com/server-setup">setup tutorials</a>.',
+	alertMessage: null,
};
```

#### To Add Custom Head Script

Edit `site-data/head/index.js` and put your HTML inside it.

**Make sure tags are always closed.**

##### `LINKS`

Say you'd like to add

```html
<link
	href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,300;0,400;0,700;0,900;1,300;1,400;1,700;1,900&display=swap"
	rel="stylesheet"
/>
```

Make the following changes

```diff
import React from 'react';
import { Helmet } from 'react-helmet';

export default function HeadHelmet() {
	return (
		<Helmet>
			{/** Custom Head Metadata goes here */}
			<script type="text/javascript">
				{`
				console.log("%cWelcome to", "color: green; font-size: 40px");
				console.log("%cHostJane", "color: red; font-size: 80px");
				`}
			</script>
+			<link
+				href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,300;0,400;0,700;0,900;1,300;1,400;1,700;1,900&display=swap"
+				rel="stylesheet"
+			/>
		</Helmet>
	);
}
```

##### `SCRIPTS`

Inserting scripts is a bit tricky and you have to put the script values inside

```jsx
<script type="text/javascript">
	{`
	// insert javascript here
	// can go multiline
	`}
</script>
```

For example Google Tag Manager says, we have to insert the following script

```html
<script>
	(function (w, d, s, l, i) {
		w[l] = w[l] || [];
		w[l].push({ 'gtm.start': new Date().getTime(), event: 'gtm.js' });
		var f = d.getElementsByTagName(s)[0],
			j = d.createElement(s),
			dl = l != 'dataLayer' ? '&l=' + l : '';
		j.async = true;
		j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
		f.parentNode.insertBefore(j, f);
	})(window, document, 'script', 'dataLayer', 'GTM-XXXX');
</script>
```

So we take everything inside the `<script>` tags and place it in the placeholder

```jsx
<script type="text/javascript">
	{`
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-XXXX');
	`}
</script>
```

**END OF DOCUMENTATION**

---

<!-- AUTO-GENERATED-CONTENT:START (STARTER) -->
<p align="center">
  <a href="https://www.gatsbyjs.org">
    <img alt="Gatsby" src="https://www.gatsbyjs.org/monogram.svg" width="60" />
  </a>
</p>
<h1 align="center">
  Gatsby's blog starter
</h1>

Kick off your project with this blog boilerplate. This starter ships with the
main Gatsby configuration files you might need to get up and running blazing
fast with the blazing fast app generator for React.

_Have another more specific idea? You may want to check out our vibrant
collection of
[official and community-created starters](https://www.gatsbyjs.org/docs/gatsby-starters/)._

## 🚀 Quick start

1.  **Create a Gatsby site.**

    Use the Gatsby CLI to create a new site, specifying the blog starter.

    ```sh
    # create a new Gatsby site using the blog starter
    gatsby new my-blog-starter https://github.com/gatsbyjs/gatsby-starter-blog
    ```

1.  **Start developing.**

    Navigate into your new site’s directory and start it up.

    ```sh
    cd my-blog-starter/
    gatsby develop
    ```

1.  **Open the source code and start editing!**

    Your site is now running at `http://localhost:8000`!

    _Note: You'll also see a second link: _`http://localhost:8000/___graphql`_.
    This is a tool you can use to experiment with querying your data. Learn more
    about using this tool in the
    [Gatsby tutorial](https://www.gatsbyjs.org/tutorial/part-five/#introducing-graphiql)._

    Open the `my-blog-starter` directory in your code editor of choice and edit
    `src/pages/index.js`. Save your changes and the browser will update in real
    time!

## 🧐 What's inside?

A quick look at the top-level files and directories you'll see in a Gatsby
project.

    .
    ├── node_modules
    ├── src
    ├── .gitignore
    ├── .prettierrc
    ├── gatsby-browser.js
    ├── gatsby-config.js
    ├── gatsby-node.js
    ├── gatsby-ssr.js
    ├── LICENSE
    ├── package-lock.json
    ├── package.json
    └── README.md

1.  **`/node_modules`**: This directory contains all of the modules of code that
    your project depends on (npm packages) are automatically installed.

2.  **`/src`**: This directory will contain all of the code related to what you
    will see on the front-end of your site (what you see in the browser) such as
    your site header or a page template. `src` is a convention for “source
    code”.

3.  **`.gitignore`**: This file tells git which files it should not track / not
    maintain a version history for.

4.  **`.prettierrc`**: This is a configuration file for
    [Prettier](https://prettier.io/). Prettier is a tool to help keep the
    formatting of your code consistent.

5.  **`gatsby-browser.js`**: This file is where Gatsby expects to find any usage
    of the [Gatsby browser APIs](https://www.gatsbyjs.org/docs/browser-apis/)
    (if any). These allow customization/extension of default Gatsby settings
    affecting the browser.

6.  **`gatsby-config.js`**: This is the main configuration file for a Gatsby
    site. This is where you can specify information about your site (metadata)
    like the site title and description, which Gatsby plugins you’d like to
    include, etc. (Check out the
    [config docs](https://www.gatsbyjs.org/docs/gatsby-config/) for more
    detail).

7.  **`gatsby-node.js`**: This file is where Gatsby expects to find any usage of
    the [Gatsby Node APIs](https://www.gatsbyjs.org/docs/node-apis/) (if any).
    These allow customization/extension of default Gatsby settings affecting
    pieces of the site build process.

8.  **`gatsby-ssr.js`**: This file is where Gatsby expects to find any usage of
    the
    [Gatsby server-side rendering APIs](https://www.gatsbyjs.org/docs/ssr-apis/)
    (if any). These allow customization of default Gatsby settings affecting
    server-side rendering.

9.  **`LICENSE`**: Gatsby is licensed under the MIT license.

10. **`package-lock.json`** (See `package.json` below, first). This is an
    automatically generated file based on the exact versions of your npm
    dependencies that were installed for your project. **(You won’t change this
    file directly).**

11. **`package.json`**: A manifest file for Node.js projects, which includes
    things like metadata (the project’s name, author, etc). This manifest is how
    npm knows which packages to install for your project.

12. **`README.md`**: A text file containing useful reference information about
    your project.

## 🎓 Learning Gatsby

Looking for more guidance? Full documentation for Gatsby lives
[on the website](https://www.gatsbyjs.org/). Here are some places to start:

-   **For most developers, we recommend starting with our
    [in-depth tutorial for creating a site with Gatsby](https://www.gatsbyjs.org/tutorial/).**
    It starts with zero assumptions about your level of ability and walks
    through every step of the process.

-   **To dive straight into code samples, head
    [to our documentation](https://www.gatsbyjs.org/docs/).** In particular,
    check out the _Guides_, _API Reference_, and _Advanced Tutorials_ sections
    in the sidebar.

## 💫 Deploy

[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://github.com/gatsbyjs/gatsby-starter-blog)

<!-- AUTO-GENERATED-CONTENT:END -->
