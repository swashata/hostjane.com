const data = {
	alertIcon: 'cloud',
	alertTitle: 'Managed cloud compute from $0.0139/hr',
	alertMessage:
		'<a href="/hosting/cloud-hosting-wizard/">Configure Your Server</a> - 100% SSD / Latest-gen Intel CPUs / Diverse OS combinations.',
};

export default data;
