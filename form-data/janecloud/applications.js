const applications = [
	{
		id: 'no-app',
		name: 'Barebones',
		icon: 'noapplication',
	},
	{
		id: 'docker',
		name: 'Docker',
		icon: 'docker',
	},
	{
		id: 'lamp',
		name: 'LAMP',
		icon: 'lamp',
	},
	{
		id: 'lemp',
		name: 'LEMP',
		icon: 'lemp',
	},
	{
		id: 'minecraft',
		name: 'Minecraft',
		icon: 'minecraft',
	},
	{
		id: 'openvpn',
		name: 'OpenVPN',
		icon: 'openvpn',
	},
	{
		id: 'gitlab',
		name: 'GitLab',
		icon: 'gitlab',
	},
];

export default applications;
