import ubuntu64 from './os/64/ubuntu';
import centos64 from './os/64/centos';
import freebsd64 from './os/64/freebsd';
import fedora64 from './os/64/fedora';
import debian64 from './os/64/debian';
import coreos64 from './os/64/coreos';

import ubuntu32 from './os/32/ubuntu';
import centos32 from './os/32/centos';
import debian32 from './os/32/debian';

const data = {
	title: 'Configure Your Compute',
	seoDescription: 'Deploy cloud computes in 120s. 17 global datacenters. Build, test, manage or scale your web application or developer environement.',
	helpText:
		'Deploy linux virtual machines (VMs) in 120s. Choose from 17 global datacenters. Build, test, manage or scale your web application or developer environement.',
	oses: [
		{
			name: '64 bit OS',
			items: [
				ubuntu64,
				centos64,
				freebsd64,
				fedora64,
				debian64,
				coreos64,
			],
		},
		{
			name: '32 bit OS',
			items: [ubuntu32, centos32, debian32],
		},
	],
	stepOneTitle: 'Select your Linux Distribution',
	stepTwoTitle: 'Customize your {{OS}} server',
	stepThreeTitle: 'Choose a Size - {{App}}',
	footerSummary: '{{App}} on {{Size}} {{OS}}',
	banner:
		'<span class="news-label">Product Docs:</span> See <a href="https://support.hostjane.com/servers/cloud" target="_blank">Cloud Server Features</a> and <a href="https://support.hostjane.com/server-setup" target="_blank">Linux Tutorials</a>',
	alertIcon: 'cloud',
	alertTitle: 'Managed SSD VMs from $10/mo',
	alertMessage: null,
};

export default data;
