const data = {
	name: 'CentOS',
	icon: 'centOS',
	versions: [
		{
			name: '6 i386',
			applications: [
				{
					id: 'no-app',
					pricings: [
						{
							id: 'pricing-25',
							link:
								'https://www.hostjane.com/webhost/order/config/index/janeCloud/?group_id=24&pricing_id=384',
						},
						{
							id: 'pricing-40',
							link:
								'https://www.hostjane.com/webhost/order/config/index/janeCloud/?group_id=24&pricing_id=385',
						},
						{
							id: 'pricing-60',
							link:
								'https://www.hostjane.com/webhost/order/config/index/janeCloud/?group_id=24&pricing_id=386',
						},
						{
							id: 'pricing-100',
							link:
								'https://www.hostjane.com/webhost/order/config/index/janeCloud/?group_id=24&pricing_id=387',
						},
					],
				},
			],
		},
	],
};

export default data;
