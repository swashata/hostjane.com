const data = {
	name: 'Debian',
	icon: 'debian',
	versions: [
		{
			name: '8 i386 (jessie)',
			applications: [
				{
					id: 'no-app',
					pricings: [
						{
							id: 'pricing-25',
							link:
								'https://www.hostjane.com/webhost/order/config/index/janeCloud/?group_id=24&pricing_id=442',
						},
						{
							id: 'pricing-40',
							link:
								'https://www.hostjane.com/webhost/order/config/index/janeCloud/?group_id=24&pricing_id=443',
						},
						{
							id: 'pricing-60',
							link:
								'https://www.hostjane.com/webhost/order/config/index/janeCloud/?group_id=24&pricing_id=444',
						},
						{
							id: 'pricing-100',
							link:
								'https://www.hostjane.com/webhost/order/config/index/janeCloud/?group_id=24&pricing_id=447',
						},
					],
				},
			],
		},
	],
};

export default data;
