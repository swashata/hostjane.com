const data = {
	name: 'Ubuntu',
	icon: 'ubuntu',
	versions: [
		{
			name: '16.04 i386',
			applications: [
				{
					id: 'no-app',
					pricings: [
						{
							id: 'pricing-25',
							link:
								'https://www.hostjane.com/webhost/order/config/index/janeCloud/?group_id=24&pricing_id=336',
						},
						{
							id: 'pricing-40',
							link:
								'https://www.hostjane.com/webhost/order/config/index/janeCloud/?group_id=24&pricing_id=337',
						},
						{
							id: 'pricing-60',
							link:
								'https://www.hostjane.com/webhost/order/config/index/janeCloud/?group_id=24&pricing_id=338',
						},
						{
							id: 'pricing-100',
							link:
								'https://www.hostjane.com/webhost/order/config/index/janeCloud/?group_id=24&pricing_id=339',
						},
					],
				},
			],
		},
	],
};

export default data;
