const data = {
	name: 'CentOS',
	icon: 'centOS',
	versions: [
		{
			name: '6 x64',
			applications: [
				{
					id: 'no-app',
					pricings: [
						{
							id: 'pricing-25',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=380',
						},
						{
							id: 'pricing-40',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=381',
						},
						{
							id: 'pricing-60',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=382',
						},
						{
							id: 'pricing-100',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=383',
						},
					],
				},
				{
					id: 'lamp',
					pricings: [
						{
							id: 'pricing-25',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=399',
						},
						{
							id: 'pricing-40',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=400',
						},
						{
							id: 'pricing-60',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=401',
						},
						{
							id: 'pricing-100',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=402',
						},
					],
				},
				{
					id: 'lemp',
					pricings: [
						{
							id: 'pricing-25',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=410',
						},
						{
							id: 'pricing-40',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=411',
						},
						{
							id: 'pricing-60',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=412',
						},
						{
							id: 'pricing-100',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=413',
						},
					],
				},
			],
		},
		{
			name: '7 x64',
			applications: [
				{
					id: 'no-app',
					pricings: [
						{
							id: 'pricing-25',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=376',
						},
						{
							id: 'pricing-40',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=377',
						},
						{
							id: 'pricing-60',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=378',
						},
						{
							id: 'pricing-100',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=379',
						},
					],
				},
				{
					id: 'docker',
					pricings: [
						{
							id: 'pricing-25',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=388',
						},
						{
							id: 'pricing-40',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=390',
						},
						{
							id: 'pricing-60',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=391',
						},
						{
							id: 'pricing-100',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=393',
						},
					],
				},
				{
					id: 'lamp',
					pricings: [
						{
							id: 'pricing-25',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=394',
						},
						{
							id: 'pricing-40',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=396',
						},
						{
							id: 'pricing-60',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=397',
						},
						{
							id: 'pricing-100',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=398',
						},
					],
				},
				{
					id: 'lemp',
					pricings: [
						{
							id: 'pricing-25',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=404',
						},
						{
							id: 'pricing-40',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=407',
						},
						{
							id: 'pricing-60',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=408',
						},
						{
							id: 'pricing-100',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=409',
						},
					],
				},
			],
		},
		{
			name: '8 x64',
			applications: [
				{
					id: 'no-app',
					pricings: [
						{
							id: 'pricing-25',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=638',
						},
						{
							id: 'pricing-40',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=637',
						},
						{
							id: 'pricing-60',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=636',
						},
						{
							id: 'pricing-100',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=635',
						},
					],
				},
			],
		},
	],
};

export default data;
