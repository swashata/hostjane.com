const data = {
	name: 'CoreOS',
	icon: 'coreOS',
	versions: [
		{
			name: 'Stable x64',
			applications: [
				{
					id: 'no-app',
					pricings: [
						{
							id: 'pricing-25',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=448',
						},
						{
							id: 'pricing-40',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=449',
						},
						{
							id: 'pricing-60',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=450',
						},
						{
							id: 'pricing-100',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=451',
						},
					],
				},
			],
		},
	],
};

export default data;
