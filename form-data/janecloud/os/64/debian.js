const data = {
	name: 'Debian',
	icon: 'debian',
	versions: [
		{
			name: '9 x64 (stretch)',
			applications: [
				{
					id: 'no-app',
					pricings: [
						{
							id: 'pricing-25',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=434',
						},
						{
							id: 'pricing-40',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=435',
						},
						{
							id: 'pricing-60',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=436',
						},
						{
							id: 'pricing-100',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=437',
						},
					],
				},
			],
		},
		{
			name: '10 x64 (buster)',
			applications: [
				{
					id: 'no-app',
					pricings: [
						{
							id: 'pricing-25',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=577',
						},
						{
							id: 'pricing-40',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=576',
						},
						{
							id: 'pricing-60',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=575',
						},
						{
							id: 'pricing-100',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=574',
						},
					],
				},
			],
		},
	],
};

export default data;
