const data = {
	name: 'Fedora',
	icon: 'fedora',
	versions: [
		{
			name: '30 x64',
			applications: [
				{
					id: 'no-app',
					pricings: [
						{
							id: 'pricing-25',
							link: 
							    'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=425',
						},
						{
							id: 'pricing-40',
							link: 
							    'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=426',
						},
						{
							id: 'pricing-60',
							link: 
							    'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=427',
						},
						{
							id: 'pricing-100',
							link: 
							    'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=428',
						},
					],
				},
			],
		},
		{
			name: '31 x64',
			applications: [
				{
					id: 'no-app',
					pricings: [
						{
							id: 'pricing-25',
							link: 
							    'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=429',
						},
						{
							id: 'pricing-40',
							link: 
							    'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=430',
						},
						{
							id: 'pricing-60',
							link: 
							    'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=432',
						},
						{
							id: 'pricing-100',
							link: 
							    'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=433',
						},
					],
				},
			],
		},		
	],
};

export default data;
