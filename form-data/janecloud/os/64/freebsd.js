const data = {
	name: 'FreeBSD',
	icon: 'freeBSD',
	versions: [
		{
			name: '11 x64',
			applications: [
				{
					id: 'no-app',
					pricings: [
						{
							id: 'pricing-25',
							link: 
							    'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=414',
						},
						{
							id: 'pricing-40',
							link: 
							    'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=415',
						},
						{
							id: 'pricing-60',
							link: 
							    'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=416',
						},
						{
							id: 'pricing-100',
							link: 
							    'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=419',
						},
					],
				},
			],
		},
		{
			name: '12 x64',
			applications: [
				{
					id: 'no-app',
					pricings: [
						{
							id: 'pricing-25',
							link: 
							    'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=420',
						},
						{
							id: 'pricing-40',
							link: 
							    'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=421',
						},
						{
							id: 'pricing-60',
							link: 
							    'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=422',
						},
						{
							id: 'pricing-100',
							link: 
							    'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=424',
						},
					],
				},
			],
		},
	],
};

export default data;
