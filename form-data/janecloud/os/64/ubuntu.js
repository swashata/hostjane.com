const data = {
	name: 'Ubuntu',
	icon: 'ubuntu',
	versions: [
		{
			name: '16.04 x64',
			applications: [
				{
					id: 'no-app',
					pricings: [
						{
							id: 'pricing-25',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=310',
						},
						{
							id: 'pricing-40',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=311',
						},
						{
							id: 'pricing-60',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=313',
						},
						{
							id: 'pricing-100',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=315',
						},
					],
				},
			],
		},
		{
			name: '18.04 x64',
			applications: [
				{
					id: 'no-app',
					pricings: [
						{
							id: 'pricing-25',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=316',
						},
						{
							id: 'pricing-40',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=317',
						},
						{
							id: 'pricing-60',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=319',
						},
						{
							id: 'pricing-100',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=320',
						},
					],
				},
				{
					id: 'docker',
					pricings: [
						{
							id: 'pricing-25',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=345',
						},
						{
							id: 'pricing-40',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=346',
						},
						{
							id: 'pricing-60',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=347',
						},
						{
							id: 'pricing-100',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=349',
						},
					],
				},
				{
					id: 'lamp',
					pricings: [
						{
							id: 'pricing-25',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=351',
						},
						{
							id: 'pricing-40',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=352',
						},
						{
							id: 'pricing-60',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=354',
						},
						{
							id: 'pricing-100',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=356',
						},
					],
				},
				{
					id: 'lemp',
					pricings: [
						{
							id: 'pricing-25',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=358',
						},
						{
							id: 'pricing-40',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=360',
						},
						{
							id: 'pricing-60',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=361',
						},
						{
							id: 'pricing-100',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=363',
						},
					],
				},
				{
					id: 'minecraft',
					pricings: [
						{
							id: 'pricing-40',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=365',
						},
						{
							id: 'pricing-60',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=367',
						},
						{
							id: 'pricing-100',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=368',
						},
					],
				},
				{
					id: 'openvpn',
					pricings: [
						{
							id: 'pricing-25',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=369',
						},
						{
							id: 'pricing-40',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=370',
						},
						{
							id: 'pricing-60',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=371',
						},
						{
							id: 'pricing-100',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=372',
						},
					],
				},
				{
					id: 'gitlab',
					pricings: [
						{
							id: 'pricing-60',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=374',
						},
						{
							id: 'pricing-100',
							link:
								'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=375',
						},
					],
				},
			],
		},
		{
			name: '19.10 x64',
			applications: [
				{
					id: 'no-app',
					pricings: [
						{
							id: 'pricing-25',
							link: 'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=582',
						},
						{
							id: 'pricing-40',
							link: 'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=585',
						},
						{
							id: 'pricing-60',
							link: 'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=584',
						},
						{
							id: 'pricing-100',
							link: 'https://www.hostjane.com/webhost/order/config/index/JaneCloud/?group_id=24&pricing_id=583',
						},
					],
				},
			],
		},
	],
};

export default data;
