const pricing = [
	{
		id: 'pricing-25',
		title: '25 GB SSD',
		price: '$0.0139/hr',
		banner: '50% off',
		features: ['1000 GB Bandwidth', '1 GB Guaranteed RAM', '1 Intel Core CPU', 'Free Daily Backups', 'Managed', '100% SLA', '1 Dedicated IPv4 Address', 'IPv6 Available', 'Root Access', 'DDoS Protection'],
		rrp: '30 days',
	},
	{
		id: 'pricing-40',
		title: '55 GB SSD',
		price: '$0.025/hr',
		banner: '40% off',
		features: ['2000 GB Bandwidth', '2 GB Guaranteed RAM', '1 Intel Core CPU', 'Free Daily Backups', 'Managed', '100% SLA', '1 Dedicated IPv4 Address', 'IPv6 Available', 'Root Access', 'DDoS Protection'],
		rrp: '30 days',
	},
	{
		id: 'pricing-60',
		title: '80 GB SSD',
		price: '$0.0458/hr',
		banner: '34% off',
		features: ['3000 GB Bandwidth', '4 GB Guaranteed RAM', '2 Intel Core CPU', 'Free Daily Backups', 'Managed', '100% SLA', '1 Dedicated IPv4 Address', 'IPv6 Available', 'Root Access', 'DDoS Protection'],
		rrp: '30 days',
	},
	{
		id: 'pricing-100',
		title: '160 GB SSD',
		price: '$0.0833/hr',
		banner: '25% off',
		features: ['4000 GB Bandwidth', '8 GB Guaranteed RAM', '4 Intel Core CPU','Free Daily Backups', 'Managed', '100% SLA', '1 Dedicated IPv4 Address', 'IPv6 Available', 'Root Access', 'DDoS Protection'],
		rrp: '30 days',
	},
];

export default pricing;
