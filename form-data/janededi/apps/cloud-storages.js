const data = [
	{
		name: 'Nextcloud',
		icon: 'nextcloud',
		pricings: [
			{
				id: 'pricing-200',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=586',
			},
			{
				id: 'pricing-300',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=587',
			},
			{
				id: 'pricing-400',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=588',
			},
		],
	},
	{
		name: 'Owncloud',
		icon: 'owncloud',
		pricings: [
			{
				id: 'pricing-200',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=589',
			},
			{
				id: 'pricing-300',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=590',
			},
			{
				id: 'pricing-400',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=591',
			},
		],
	},
];

export default data;
