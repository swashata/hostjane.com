const data = [
	{
		name: 'WordPress',
		icon: 'wordpress',
		pricings: [
			{
				id: 'pricing-200',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=539',
			},
			{
				id: 'pricing-300',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=540',
			},
			{
				id: 'pricing-400',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=541',
			},
		],
	},
	{
		name: 'Drupal',
		icon: 'drupal',
		pricings: [
			{
				id: 'pricing-200',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=533',
			},
			{
				id: 'pricing-300',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=534',
			},
			{
				id: 'pricing-400',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=535',
			},
		],
	},
	{
		name: 'Joomla',
		icon: 'joomla',
		pricings: [
			{
				id: 'pricing-200',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=542',
			},
			{
				id: 'pricing-300',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=543',
			},
			{
				id: 'pricing-400',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=544',
			},
		],
	},
	{
		name: 'Magento',
		icon: 'magento',
		pricings: [
			{
				id: 'pricing-200',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=545',
			},
			{
				id: 'pricing-300',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=546',
			},
			{
				id: 'pricing-400',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=547',
			},
		],
	},
	{
		name: 'PrestaShop',
		icon: 'prestashop',
		pricings: [
			{
				id: 'pricing-200',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=548',
			},
			{
				id: 'pricing-300',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=549',
			},
			{
				id: 'pricing-400',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=550',
			},
		],
	},
	{
		name: 'MediaWiki',
		icon: 'mediawiki',
		pricings: [
			{
				id: 'pricing-200',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=594',
			},
			{
				id: 'pricing-300',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=593',
			},
			{
				id: 'pricing-400',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=592',
			},
		],
	},
];

export default data;
