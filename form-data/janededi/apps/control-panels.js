const data = [
	{
		name: 'cPanel/WHM',
		icon: 'cpanelwhm',
		license: true,
		pricings: [
			{
				id: 'pricing-320-cpanel',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=536',
			},
			{
				id: 'pricing-640-cpanel',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=537',
			},
			{
				id: 'pricing-1280-cpanel',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=538',
			},
		],
	},
	{
		name: 'Plesk Admin SE',
		icon: 'pleskwebadminse',
		license: true,
		pricings: [
			{
				id: 'pricing-200',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=595',
			},
			{
				id: 'pricing-300',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=596',
			},
			{
				id: 'pricing-400',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=597',
			},
		],
	},
	{
		name: 'Plesk Web Pro',
		icon: 'pleskwebpro',
		license: true,
		pricings: [
			{
				id: 'pricing-320-pwebpro',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=598',
			},
			{
				id: 'pricing-640-pwebpro',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=599',
			},
			{
				id: 'pricing-1280-pwebpro',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=600',
			},
		],
	},
	{
		name: 'Plesk Web Host',
		icon: 'pleskwebhost',
		license: true,
		pricings: [
			{
				id: 'pricing-320-pwebhost',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=601',
			},
			{
				id: 'pricing-640-pwebhost',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=602',
			},
			{
				id: 'pricing-1280-pwebhost',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=603',
			},
		],
	},
	{
		name: 'Webmin',
		icon: 'webmin',
		pricings: [
			{
				id: 'pricing-200',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=604',
			},
			{
				id: 'pricing-300',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=605',
			},
			{
				id: 'pricing-400',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=606',
			},
		],
	},
	{
		name: 'CentOS Web Panel',
		icon: 'centoswpl',
		pricings: [
			{
				id: 'pricing-200',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=1053',
			},
			{
				id: 'pricing-300',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=1054',
			},
			{
				id: 'pricing-400',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=1055',
			},
		],
	},
];

export default data;
