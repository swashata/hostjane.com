const data = [
	{
		name: 'Ubuntu',
		subtitle: '18.04 x64 LTS',
		icon: 'ubuntu',
		license: false,
		pricings: [
			{
				id: 'pricing-linux-320',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=962',
			},
			{
				id: 'pricing-linux-640',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=959',
			},
			{
				id: 'pricing-linux-1280',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=965',
			},
		],
	},
	{
		name: 'CentOS',
		subtitle: '8 x64',
		icon: 'centos',
		license: false,
		pricings: [
			{
				id: 'pricing-linux-320',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=968',
			},
			{
				id: 'pricing-linux-640',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=971',
			},
			{
				id: 'pricing-linux-1280',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=974',
			},
		],
	},
	{
		name: 'FreeBSD',
		subtitle: '12 x64',
		icon: 'freebsd',
		license: false,
		pricings: [
			{
				id: 'pricing-linux-320',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=977',
			},
			{
				id: 'pricing-linux-640',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=980',
			},
			{
				id: 'pricing-linux-1280',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=983',
			},
		],
	},
	{
		name: 'Fedora',
		subtitle: '32 x64',
		icon: 'fedora',
		license: false,
		pricings: [
			{
				id: 'pricing-linux-320',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=987',
			},
			{
				id: 'pricing-linux-640',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=990',
			},
			{
				id: 'pricing-linux-1280',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=993',
			},
		],
	},
	{
		name: 'Debian',
		subtitle: '10 x64',
		icon: 'debian',
		license: false,
		pricings: [
			{
				id: 'pricing-linux-320',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=997',
			},
			{
				id: 'pricing-linux-640',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=1000',
			},
			{
				id: 'pricing-linux-1280',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=1003',
			},
		],
	},
	{
		name: 'CoreOS',
		subtitle: 'Stable x64',
		icon: 'coreos',
		license: false,
		pricings: [
			{
				id: 'pricing-linux-320',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=1006',
			},
			{
				id: 'pricing-linux-640',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=1009',
			},
			{
				id: 'pricing-linux-1280',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneDedi/?group_id=26&pricing_id=1012',
			},
		],
	},
];

export default data;
