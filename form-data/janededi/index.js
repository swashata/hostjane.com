import cpitems from './apps/control-panels';
import cmsitems from './apps/cmses';
import csitems from './apps/cloud-storages';
import linuxonlyitems from './apps/linux-only';

const data = {
	title: 'Configure Your Server',
	seoDescription:
		'HostJane offers powerful SSD dedicated servers for mission-critical websites and applications. Free backups. 24/7 support.',
	helpText:
		'High-performance, enterprise dedicated servers perfect for resource-intensive applications. Automated backups included. Tier III datacenter guaranteed. ',
	applications: [
		{
			name: 'Control Panels',
			items: cpitems,
		},
		{
			name: 'Content Management Systems',
			items: cmsitems,
		},
		{
			name: 'Cloud Storage',
			items: csitems,
		},
		{
			name: 'Linux Only',
			items: linuxonlyitems,
		},
	],
	stepOneTitle: 'Choose Your Application',
	stepTwoTitle: 'Choose a Size - {{App}}',
	stepThreeTitle: 'User Guide',
	checkboxText:
	'I have read and understood the <a href="https://support.hostjane.com/server-setup" target="_blank">User Guide</a> for this product. A temporary browser warning may show until SSL is configured for the domain.',
	footerSummary: 'Order {{App}} {{Size}} Dedicated',
	banner:
	'<span class="news-label">Product Docs:</span> See <a href="https://support.hostjane.com/servers/dedicated" target="_blank">Dedicated Server Features</a> and <a href="https://support.hostjane.com/server-setup" target="_blank">User Guides</a>',
	alertIcon: 'dedicated',
	alertTitle: 'Single-tenant, 16 Intel Core SSD Dedicated Servers',
	alertMessage: null,
};

export default data;
