const data = [
	{
		name: 'Nextcloud',
		icon: 'nextcloud',
		pricings: [
			{
				id: 'pricing-25',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=610',
			},
			{
				id: 'pricing-40',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=609',
			},
			{
				id: 'pricing-60',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=608',
			},
			{
				id: 'pricing-100',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=607',
			},
		],
	},
	{
		name: 'Owncloud',
		icon: 'owncloud',
		pricings: [
			{
				id: 'pricing-25',
				link:
					'#https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=614',
			},
			{
				id: 'pricing-40',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=613',
			},
			{
				id: 'pricing-60',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=612',
			},
			{
				id: 'pricing-100',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=611',
			},
		],
	},
];

export default data;
