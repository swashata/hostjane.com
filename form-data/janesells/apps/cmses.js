const data = [
	{
		name: 'WordPress',
		icon: 'wordpress',
		pricings: [
			{
				id: 'pricing-25',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=528',
			},
			{
				id: 'pricing-40',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=529',
			},
			{
				id: 'pricing-60',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=530',
			},
			{
				id: 'pricing-100',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=531',
			},
		],
	},
	{
		name: 'Drupal',
		icon: 'drupal',
		pricings: [
			{
				id: 'pricing-25',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=509',
			},
			{
				id: 'pricing-40',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=510',
			},
			{
				id: 'pricing-60',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=511',
			},
			{
				id: 'pricing-100',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=512',
			},
		],
	},
	{
		name: 'Joomla',
		icon: 'joomla',
		pricings: [
			{
				id: 'pricing-25',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=513',
			},
			{
				id: 'pricing-40',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=514',
			},
			{
				id: 'pricing-60',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=515',
			},
			{
				id: 'pricing-100',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=516',
			},
		],
	},
	{
		name: 'Magento',
		icon: 'magento',
		pricings: [
			{
				id: 'pricing-25',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=517',
			},
			{
				id: 'pricing-40',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=518',
			},
			{
				id: 'pricing-60',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=519',
			},
			{
				id: 'pricing-100',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=520',
			},
		],
	},
	{
		name: 'PrestaShop',
		icon: 'prestashop',
		pricings: [
			{
				id: 'pricing-25',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=521',
			},
			{
				id: 'pricing-40',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=522',
			},
			{
				id: 'pricing-60',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=523',
			},
			{
				id: 'pricing-100',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=524',
			},
		],
	},
	{
		name: 'MediaWiki',
		icon: 'mediawiki',
		pricings: [
			{
				id: 'pricing-25',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=615',
			},
			{
				id: 'pricing-40',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=616',
			},
			{
				id: 'pricing-60',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=617',
			},
			{
				id: 'pricing-100',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=618',
			},
		],
	},
];

export default data;
