const data = [
	{
		name: 'cPanel/WHM',
		icon: 'cpanelwhm',
		license: true,
		pricings: [
			{
				id: 'pricing-40-wlicense',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=525',
			},
			{
				id: 'pricing-60-wlicense',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=526',
			},
			{
				id: 'pricing-100-wlicense',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=527',
			},
		],
	},
	{
		name: 'Plesk Admin SE',
		icon: 'pleskwebadminse',
		license: true,
		pricings: [
			{
				id: 'pricing-25',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=619',
			},
			{
				id: 'pricing-40',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=620',
			},
			{
				id: 'pricing-60',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=621',
			},
			{
				id: 'pricing-100',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=622',
			},
		],
	},
	{
		name: 'Plesk Web Pro',
		icon: 'pleskwebpro',
		license: true,
		pricings: [
			{
				id: 'pricing-25-pwebpro',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=623',
			},
			{
				id: 'pricing-40-pwebpro',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=624',
			},
			{
				id: 'pricing-60-pwebpro',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=625',
			},
			{
				id: 'pricing-100-pwebpro',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=626',
			},
		],
	},
	{
		name: 'Plesk Web Host',
		icon: 'pleskwebhost',
		license: true,
		pricings: [
			{
				id: 'pricing-25-pwebhost',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=627',
			},
			{
				id: 'pricing-40-pwebhost',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=628',
			},
			{
				id: 'pricing-60-pwebhost',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=629',
			},
			{
				id: 'pricing-100-pwebhost',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=630',
			},
		],
	},
	{
		name: 'Webmin',
		icon: 'webmin',
		pricings: [
			{
				id: 'pricing-25',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=631',
			},
			{
				id: 'pricing-40',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=632',
			},
			{
				id: 'pricing-60',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=633',
			},
			{
				id: 'pricing-100',
				link: 
				    'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=634',
			},
		],
	},
	{
		name: 'CentOS Web Panel',
		icon: 'centoswpl',
		pricings: [
			{
				id: 'pricing-25',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=1041',
			},
			{
				id: 'pricing-40',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=1044',
			},
			{
				id: 'pricing-60',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=1047',
			},
			{
				id: 'pricing-100',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=1050',
			},
		],
	},
];

export default data;
