const data = [
	{
		name: 'Ubuntu',
		subtitle: '18.04 x64 LTS',
		icon: 'ubuntu',
		license: false,
		pricings: [
			{
				id: 'pricing-linux-25',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=887',
			},
			{
				id: 'pricing-linux-40',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=893',
			},
			{
				id: 'pricing-linux-60',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=890',
			},
			{
				id: 'pricing-linux-100',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=896',
			},
		],
	},
	{
		name: 'CentOS',
		subtitle: '8 x64',
		icon: 'centos',
		license: false,
		pricings: [
			{
				id: 'pricing-linux-25',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=899',
			},
			{
				id: 'pricing-linux-40',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=902',
			},
			{
				id: 'pricing-linux-60',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=905',
			},
			{
				id: 'pricing-linux-100',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=908',
			},
		],
	},
	{
		name: 'FreeBSD',
		subtitle: '12 x64',
		icon: 'freebsd',
		license: false,
		pricings: [
			{
				id: 'pricing-linux-25',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=911',
			},
			{
				id: 'pricing-linux-40',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=914',
			},
			{
				id: 'pricing-linux-60',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=917',
			},
			{
				id: 'pricing-linux-100',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=920',
			},
		],
	},
	{
		name: 'Fedora',
		subtitle: '32 x64',
		icon: 'fedora',
		license: false,
		pricings: [
			{
				id: 'pricing-linux-25',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=923',
			},
			{
				id: 'pricing-linux-40',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=926',
			},
			{
				id: 'pricing-linux-60',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=929',
			},
			{
				id: 'pricing-linux-100',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=932',
			},
		],
	},
	{
		name: 'Debian',
		subtitle: '10 x64',
		icon: 'debian',
		license: false,
		pricings: [
			{
				id: 'pricing-linux-25',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=935',
			},
			{
				id: 'pricing-linux-40',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=938',
			},
			{
				id: 'pricing-linux-60',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=941',
			},
			{
				id: 'pricing-linux-100',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=944',
			},
		],
	},
	{
		name: 'CoreOS',
		subtitle: 'Stable x64',
		icon: 'coreos',
		license: false,
		pricings: [
			{
				id: 'pricing-linux-25',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=947',
			},
			{
				id: 'pricing-linux-40',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=950',
			},
			{
				id: 'pricing-linux-60',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=953',
			},
			{
				id: 'pricing-linux-100',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneSells/?group_id=27&pricing_id=956',
			},
		],
	},
];

export default data;
