import cpitems from './apps/control-panels';
import cmsitems from './apps/cmses';
import csitems from './apps/cloud-storages';
import linuxonlyitems from './apps/linux-only';

const data = {
	title: 'Configure Your White-Label VPS',
	seoDescription:
		'Power your reseller business with fast, white-labeled, powerful managed VPS hosting with 24/7 support and backups included.',
	helpText:
		'Managed SSD VPS with no branding for resellers. Offer 17 global locations. Automated daily backups with 24/7 support.',
	applications: [
		{
			name: 'Control Panels',
			items: cpitems,
		},
		{
			name: 'Content Management Systems',
			items: cmsitems,
		},
		{
			name: 'Cloud Storage',
			items: csitems,
		},
		{
			name: 'Linux Only',
			items: linuxonlyitems,
		},
	],
	stepOneTitle: 'Choose Your Application',
	stepTwoTitle: 'Choose a Size - {{App}}',
	stepThreeTitle: 'User Guide',
	checkboxText:
	'I have read and understood the <a href="https://support.hostjane.com/server-setup" target="_blank">User Guide</a> for this product. A temporary browser warning may show until SSL is configured for the domain.',
	footerSummary: 'Order {{App}} {{Size}} Reseller',
	banner:
	    '<span class="news-label">Product Docs:</span> See <a href="https://support.hostjane.com/servers/vps" target="_blank">VPS Features</a> and <a href="https://support.hostjane.com/server-setup" target="_blank">User Guides</a>',
	alertIcon: 'reseller',
	alertTitle: 'White-Labeled, Managed VPS for Resellers',
	alertMessage: null,
};

export default data;
