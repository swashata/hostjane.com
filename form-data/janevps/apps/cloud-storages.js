const data = [
	{
		name: 'Nextcloud',
		icon: 'nextcloud',
		pricings: [
			{
				id: 'pricing-25',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=505',
			},
			{
				id: 'pricing-40',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=506',
			},
			{
				id: 'pricing-60',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=507',
			},
			{
				id: 'pricing-100',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=508',
			},
		],
	},
	{
		name: 'Owncloud',
		icon: 'owncloud',
		pricings: [
			{
				id: 'pricing-25',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=500',
			},
			{
				id: 'pricing-40',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=501',
			},
			{
				id: 'pricing-60',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=502',
			},
			{
				id: 'pricing-100',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=504',
			},
		],
	},
];

export default data;
