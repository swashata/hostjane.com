const data = [
	{
		name: 'WordPress',
		icon: 'wordpress',
		pricings: [
			{
				id: 'pricing-25',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=460',
			},
			{
				id: 'pricing-40',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=461',
			},
			{
				id: 'pricing-60',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=463',
			},
			{
				id: 'pricing-100',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=465',
			},
		],
	},
	{
		name: 'Drupal',
		icon: 'drupal',
		pricings: [
			{
				id: 'pricing-25',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=452',
			},
			{
				id: 'pricing-40',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=453',
			},
			{
				id: 'pricing-60',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=454',
			},
			{
				id: 'pricing-100',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=455',
			},
		],
	},
	{
		name: 'Joomla',
		icon: 'joomla',
		pricings: [
			{
				id: 'pricing-25',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=466',
			},
			{
				id: 'pricing-40',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=467',
			},
			{
				id: 'pricing-60',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=468',
			},
			{
				id: 'pricing-100',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=469',
			},
		],
	},
	{
		name: 'Magento',
		icon: 'magento',
		pricings: [
			{
				id: 'pricing-25',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=470',
			},
			{
				id: 'pricing-40',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=471',
			},
			{
				id: 'pricing-60',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=472',
			},
			{
				id: 'pricing-100',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=473',
			},
		],
	},
	{
		name: 'PrestaShop',
		icon: 'prestashop',
		pricings: [
			{
				id: 'pricing-25',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=474',
			},
			{
				id: 'pricing-40',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=475',
			},
			{
				id: 'pricing-60',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=476',
			},
			{
				id: 'pricing-100',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=477',
			},
		],
	},
	{
		name: 'MediaWiki',
		icon: 'mediawiki',
		pricings: [
			{
				id: 'pricing-25',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=478',
			},
			{
				id: 'pricing-40',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=479',
			},
			{
				id: 'pricing-60',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=480',
			},
			{
				id: 'pricing-100',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=481',
			},
		],
	},
];

export default data;
