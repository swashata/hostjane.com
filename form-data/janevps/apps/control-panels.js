const data = [
	{
		name: 'cPanel/WHM',
		icon: 'cpanelwhm',
		license: true,
		pricings: [
			{
				id: 'pricing-40-wlicense',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=457',
			},
			{
				id: 'pricing-60-wlicense',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=458',
			},
			{
				id: 'pricing-100-wlicense',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=459',
			},
		],
	},
	{
		name: 'Plesk Admin SE',
		icon: 'pleskwebadminse',
		license: true,
		pricings: [
			{
				id: 'pricing-25',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=482',
			},
			{
				id: 'pricing-40',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=483',
			},
			{
				id: 'pricing-60',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=484',
			},
			{
				id: 'pricing-100',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=485',
			},
		],
	},
	{
		name: 'Plesk Web Pro',
		icon: 'pleskwebpro',
		license: true,
		pricings: [
			{
				id: 'pricing-25-pwebpro',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=486',
			},
			{
				id: 'pricing-40-pwebpro',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=487',
			},
			{
				id: 'pricing-60-pwebpro',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=488',
			},
			{
				id: 'pricing-100-pwebpro',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=489',
			},
		],
	},
	{
		name: 'Plesk Web Host',
		icon: 'pleskwebhost',
		license: true,
		pricings: [
			{
				id: 'pricing-25-pwebhost',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=491',
			},
			{
				id: 'pricing-40-pwebhost',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=492',
			},
			{
				id: 'pricing-60-pwebhost',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=493',
			},
			{
				id: 'pricing-100-pwebhost',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=494',
			},
		],
	},
	{
		name: 'Webmin',
		icon: 'webmin',
		pricings: [
			{
				id: 'pricing-25',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=495',
			},
			{
				id: 'pricing-40',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=496',
			},
			{
				id: 'pricing-60',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=498',
			},
			{
				id: 'pricing-100',
				link:
					'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=499',
			},
		],
	},
	{
		name: 'CentOS Web Panel',
		icon: 'centoswpl',
		pricings: [
			{
				id: 'pricing-25',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=1029',
			},
			{
				id: 'pricing-40',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=1032',
			},
			{
				id: 'pricing-60',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=1035',
			},
			{
				id: 'pricing-100',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=1038',
			},
		],
	},
];

export default data;
