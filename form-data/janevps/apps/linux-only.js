const data = [
	{
		name: 'Ubuntu',
		subtitle: '18.04 x64 LTS',
		icon: 'ubuntu',
		license: false,
		pricings: [
			{
				id: 'pricing-linux-25',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=815',
			},
			{
				id: 'pricing-linux-40',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=821',
			},
			{
				id: 'pricing-linux-60',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=818',
			},
			{
				id: 'pricing-linux-100',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=826',
			},
		],
	},
	{
		name: 'CentOS',
		subtitle: '8 x64',
		icon: 'centos',
		license: false,
		pricings: [
			{
				id: 'pricing-linux-25',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=827',
			},
			{
				id: 'pricing-linux-40',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=830',
			},
			{
				id: 'pricing-linux-60',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=833',
			},
			{
				id: 'pricing-linux-100',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=836',
			},
		],
	},
	{
		name: 'FreeBSD',
		subtitle: '12 x64',
		icon: 'freebsd',
		license: false,
		pricings: [
			{
				id: 'pricing-linux-25',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=839',
			},
			{
				id: 'pricing-linux-40',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=842',
			},
			{
				id: 'pricing-linux-60',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=845',
			},
			{
				id: 'pricing-linux-100',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=848',
			},
		],
	},
	{
		name: 'Fedora',
		subtitle: '32 x64',
		icon: 'fedora',
		license: false,
		pricings: [
			{
				id: 'pricing-linux-25',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=851',
			},
			{
				id: 'pricing-linux-40',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=854',
			},
			{
				id: 'pricing-linux-60',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=857',
			},
			{
				id: 'pricing-linux-100',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=860',
			},
		],
	},
	{
		name: 'Debian',
		subtitle: '10 x64',
		icon: 'debian',
		license: false,
		pricings: [
			{
				id: 'pricing-linux-25',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=863',
			},
			{
				id: 'pricing-linux-40',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=866',
			},
			{
				id: 'pricing-linux-60',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=869',
			},
			{
				id: 'pricing-linux-100',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=872',
			},
		],
	},
	{
		name: 'CoreOS',
		subtitle: 'Stable x64',
		icon: 'coreos',
		license: false,
		pricings: [
			{
				id: 'pricing-linux-25',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=875',
			},
			{
				id: 'pricing-linux-40',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=878',
			},
			{
				id: 'pricing-linux-60',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=881',
			},
			{
				id: 'pricing-linux-100',
				link: 'https://www.hostjane.com/webhost/order/config/index/JaneVPS/?group_id=25&pricing_id=884',
			},
		],
	},
];

export default data;
