import cpitems from './apps/control-panels';
import cmsitems from './apps/cmses';
import csitems from './apps/cloud-storages';
import linuxonlyitems from './apps/linux-only';

const data = {
	title: 'Configure Your VPS',
	seoDescription:
		'High-performance SSD VPS hosting for business. Lightning-fast and managed for you. Free daily backups and 24/7 support with all plans.',
	helpText:
		'Virtual Private Servers (VPS) with reliable Intel Cores give you more control and flexibility around your website. Scale your business faster with resource-intensive applications not suitable for shared environments.',
	applications: [
		{
			name: 'Control Panels',
			items: cpitems,
		},
		{
			name: 'Content Management Systems',
			items: cmsitems,
		},
		{
			name: 'Cloud Storage',
			items: csitems,
		},
		{
			name: 'Linux Only',
			items: linuxonlyitems,
		},
	],
	stepOneTitle: 'Choose Your Application',
	stepTwoTitle: 'Choose a Size - {{App}}',
	stepThreeTitle: 'User Guide',
	checkboxText:
			'I have read and understood the <a href="https://support.hostjane.com/server-setup" target="_blank">User Guide</a> for this product. A temporary browser warning may show until SSL is configured for the domain.',
	footerSummary: 'Order {{App}} {{Size}} VPS',
	banner:
		'<span class="news-label">Product Docs:</span> See <a href="https://support.hostjane.com/servers/vps" target="_blank">VPS Features</a> and <a href="https://support.hostjane.com/server-setup" target="_blank">User Guides</a>',
	alertIcon: 'vps',
	alertTitle: 'Managed VPS from $19.99/mo',
	alertMessage: null,
};

export default data;
