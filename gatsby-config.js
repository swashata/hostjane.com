const generateFavicons = sizes => {
	return sizes.map(size => {
		return {
			src: `favicons/icon-${size}x${size}.png`,
			sizes: `${size}x${size}`,
			type: 'image/png',
		};
	});
};

module.exports = {
	siteMetadata: {
		// EDIT THESE
		title: `HostJane Hosting`,
		author: `HostJane`,
		description: `Managed SSD VPS hosting, cloud VMs & dedicated servers with 24/7/365 support.`,
		siteUrl: `https://www.hostjane.com`,
		ogSiteName: 'www.hostjane.com',
		social: {
			twitter: `janeispowerful`,
		},
		navMainLink: '/',
		navMainLinkLabel: 'HostJane',
		navMainAltLink: '/hosting/',
		navMainAltLinkLabel: 'Hosting',
		// Main sidebar navigation used on all pages except /hosting
		mainSidebarNav: [
			{
				link: 'https://hostjane.com/marketplace/categories',
				internal: false,
				icon: 'categoriesIcon',
				label: 'Explore',
			},
			{
				link: 'https://hostjane.com/marketplace/messages',
				internal: false,
				icon: 'messagesIcon',
				label: 'Inbox',
			},
			{
				link: 'https://hostjane.com/marketplace/dashboard',
				internal: false,
				icon: 'accountIcon',
				label: 'Account',
			},
			{
				link: 'https://hostjane.com/marketplace/skills',
				internal: false,
				icon: 'skillsSidebarIcon',
				label: 'Skills',
			},
			{
				link: '/hosting/',
				internal: true,
				icon: 'webHostingIcon',
				label: 'Hosting',
			},
		],
		// title and desc used on index page
		// label used on SiteNavigation
		nav: [
			{
				path: '/hosting/vps-hosting-wizard/',
				icon: 'vps',
				title: 'VPS Hosting',
				// label: 'JaneVPS',
				label: 'VPS',
				altLabel: 'VPS',
				desc: 'Managed SSD VPS Hosting',
			},
			{
				path: '/hosting/cloud-hosting-wizard/',
				icon: 'cloud',
				title: 'Cloud Hosting',
				// label: 'JaneCloud',
				label: 'Cloud',
				altLabel: 'Cloud',
				desc: 'Managed Linux Cloud Servers',
			},
			{
				path: '/hosting/dedicated-servers-hosting-wizard/',
				icon: 'dedicated',
				title: 'Dedicated Servers',
				// label: 'JaneDedi',
				label: 'Dedicated',
				altLabel: 'Dedicated',
				desc: 'Managed SSD Dedicated Hosting',
			},
			{
				path: '/hosting/reseller-hosting-wizard/',
				icon: 'reseller',
				title: 'Reseller Hosting',
				// label: 'JaneSells',
				label: 'Reseller',
				altLabel: 'Reseller',
				desc: 'Managed SSD VPS Reseller Hosting',
			},
		],
	},
	// THAT'S IT, STOP EDITING
	plugins: [
		`gatsby-plugin-sharp`,
		`gatsby-plugin-sitemap`,
		{
			resolve: `gatsby-plugin-catch-links`,
			options: {
				excludePattern: /(marketplace|webhost|config2|assets)/,
			},
		},
		{
			resolve: `gatsby-plugin-mdx`,
			options: {
				gatsbyRemarkPlugins: [
					{
						resolve: `gatsby-remark-images`,
						options: {
							maxWidth: 1200,
						},
					},
				],
			},
		},
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				path: `${__dirname}/content/legal/`,
				name: `legals`,
				ignore: ['**/parts/**/*.*'],
			},
		},
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				path: `${__dirname}/content/assets`,
				name: `assets`,
			},
		},
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				path: `${__dirname}/site-data/testimonials`,
				name: `testimonials`,
			},
		},
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				path: `${__dirname}/site-data/home/web-mobile-sw-slider`,
				name: `web-mobile-sw-slider`,
			},
		},
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				path: `${__dirname}/site-data/home/design-art-video-slider`,
				name: `design-art-video-slider`,
			},
		},
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				path: `${__dirname}/site-data/modals/`,
				name: `hostjane-modals`,
			},
		},
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				path: `${__dirname}/site-data/about-jane`,
				name: `hostjane-about-jane`,
			},
		},
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				path: `${__dirname}/site-data/404`,
				name: `hostjane-404`,
			},
		},
		{
			resolve: `gatsby-source-filesystem`,
			options: {
				path: `${__dirname}/site-data/hosting`,
				name: `hostjane-hosting`,
			},
		},
		{
			resolve: `gatsby-transformer-remark`,
			options: {
				plugins: [
					{
						resolve: `gatsby-remark-images`,
						options: {
							maxWidth: 590,
						},
					},
					{
						resolve: `gatsby-remark-responsive-iframe`,
						options: {
							wrapperStyle: `margin-bottom: 1.0725rem`,
						},
					},
					`gatsby-remark-prismjs`,
					`gatsby-remark-copy-linked-files`,
					`gatsby-remark-smartypants`,
				],
			},
		},
		`gatsby-transformer-sharp`,

		{
			resolve: `gatsby-plugin-google-analytics`,
			options: {
				trackingId: `UA-129396908-1`,
			},
		},
		{
			resolve: `gatsby-plugin-manifest`,
			options: {
				name: `HostJane Web Hosting | WordPress Hosting, VPS, Cloud, Dedicated`,
				short_name: `HostJane`,
				start_url: `/`,
				background_color: `#19216C`,
				theme_color: `#19216C`,
				display: `standalone`,
				icon: `./content/assets/logo.png`,
				// will add this in future if needed
				// icons: generateFavicons([48, 72, 96, 144, 192, 256, 384, 512]),
			},
		},
		`gatsby-plugin-offline`,
		`gatsby-plugin-react-helmet`,
		{
			resolve: `gatsby-plugin-styled-components`,
			options: {
				// Add any options here
			},
		},
		{
			resolve: `gatsby-plugin-svgr`,
			options: {
				prettier: true,
				svgo: true,
			},
		},
		{
			resolve: 'gatsby-plugin-nprogress',
			options: {
				color: '#4055A8',
				showSpinner: true,
				delay: 0.4,
			},
		},
		{
			resolve: 'gatsby-plugin-htaccess',
			options: {
				RewriteBase: true,
				www: true,
				https: false,
				host: 'www.hostjane.com',
			},
		},
	],
};
