/* eslint-disable no-console */
const path = require(`path`);
const { createFilePath } = require(`gatsby-source-filesystem`);

exports.createPages = ({ graphql, actions }) => {
	const { createPage } = actions;
	const legalPageTemplate = path.resolve('./src/templates/legal.js');
	return new Promise((resolve, reject) => {
		resolve(
			graphql(`
				{
					legalMdxes: allMdx(
						sort: { fields: frontmatter___order, order: ASC }
						filter: {
							fileAbsolutePath: { glob: "**/content/legal/**" }
						}
					) {
						edges {
							node {
								id
								fields {
									slug
								}
							}
						}
					}
				}
			`).then(result => {
				// this is some boilerlate to handle errors
				if (result.errors) {
					console.log(result.errors);
					reject(result.errors);
				}

				// We'll call `createPage` for each result
				result.data.legalMdxes.edges.forEach(({ node }) => {
					createPage({
						// This is the slug we created before
						// (or `node.frontmatter.slug`)
						path: node.fields.slug,
						// This component will wrap our MDX content
						component: legalPageTemplate,
						// We can use the values in this context in
						// our page layout component
						context: { id: node.id },
					});
				});
			})
		);
	});
};

exports.onCreateNode = ({ node, actions, getNode }) => {
	const { createNodeField } = actions;

	// We add a slug field to legal pages only
	if (
		node.internal.type === 'Mdx' &&
		node.fileAbsolutePath.includes('/content/legal/')
	) {
		const value = createFilePath({ node, getNode });

		createNodeField({
			// Name of the field you are adding
			name: 'slug',
			// Individual MDX node
			node,
			// Generated value based on filepath with "blog" prefix. We
			// don't need a separating "/" before the value because
			// createFilePath returns a path with the leading "/".
			value: `/legal${value}`,
		});
	}
};
