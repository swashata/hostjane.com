/* eslint-disable import/no-extraneous-dependencies */
const config = require('@wpackio/eslint-config/prettier.config');

module.exports = {
	...config,
	proseWrap: 'always',
};
