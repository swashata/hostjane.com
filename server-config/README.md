-   http will be redirected to https
-   non www will be redirected to www
-   the main site will be served over
    `/var/www/html/hostjane.com/public_html/gatsby`
-   We have the following aliases set

```
Alias "/assets" "/var/www/html/hostjane.com/public_html/assets"
Alias "/config2" "/var/www/html/hostjane.com/public_html/config2"
Alias "/css" "/var/www/html/hostjane.com/public_html/css"
Alias "/js" "/var/www/html/hostjane.com/public_html/js"
Alias "/img" "/var/www/html/hostjane.com/public_html/img"
Alias "/marketplace" "/var/www/html/hostjane.com/public_html/marketplace"
Alias "/webfonts" "/var/www/html/hostjane.com/public_html/webfonts"
Alias "/wp-content" "/var/www/html/hostjane.com/public_html/wp-content"
Alias "/webhost" "/var/www/html/hostjane.com/public_html/webhost"
```

hostjane.com/marketplace and any relevant URL will be served from
/var/www/html/hostjane.com/public_html/marketplace

-   Proper cache header is set for gatsby site.

---

The files in `backups` are from old apache config.
