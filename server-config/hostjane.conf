# Directory should be readable by all and .htaccess should be supported
<Directory /var/www/html/hostjane.com/public_html>
	# Everyone is able to access this directory
	Require all granted
	# Enable symbolic links
	Options FollowSymLinks
	# Enable .htaccess to override
	AllowOverride All
</Directory>

# For the non https, we simply redirect
<VirtualHost *:80>
	ServerName hostjane.com
	ServerAlias www.hostjane.com

	# Redirect to https://www.hostjane.com
	Redirect permanent / https://www.hostjane.com/

	# Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
	# error, crit, alert, emerg.
	# It is also possible to configure the loglevel for particular
	# modules, e.g.
	#LogLevel info ssl:warn

	ErrorLog "| /usr/bin/rotatelogs /var/www/html/hostjane.com/logs/http-errorlog.%Y-%m-%d-%H_%M_%S 2M"
	CustomLog "| /usr/bin/rotatelogs /var/www/html/hostjane.com/logs/http-accesslog.%Y-%m-%d-%H_%M_%S 2M" combined

	#ErrorLog ${APACHE_LOG_DIR}/error.log
	#CustomLog ${APACHE_LOG_DIR}/access.log combined

	# For most configuration files from conf-available/, which are
	# enabled or disabled at a global level, it is possible to
	# include a line for only one particular virtual host. For example the
	# following line enables the CGI configuration for this host only
	# after it has been globally disabled with "a2disconf".
	#Include conf-available/serve-cgi-bin.conf
</VirtualHost>

# Redirect https non-www to www
<VirtualHost *:443>
	ServerName hostjane.com
	# Enable SSL
	SSLEngine on
	SSLCertificateFile /etc/ssl/certs/hostjane_com.crt
	SSLCertificateKeyFile /etc/ssl/private/hostjane_com.key
	# Redirect to www
	Redirect permanent / https://www.hostjane.com/

	ErrorLog "| /usr/bin/rotatelogs /var/www/html/hostjane.com/logs/https-errorlog.%Y-%m-%d-%H_%M_%S 2M"
	CustomLog "| /usr/bin/rotatelogs /var/www/html/hostjane.com/logs/https-accesslog.%Y-%m-%d-%H_%M_%S 2M" combined
</VirtualHost>

# The real virtual host config to handle https://www.hostjane.com
<VirtualHost *:443>
	ServerName www.hostjane.com
	ServerAdmin webmaster@localhost
	DocumentRoot /var/www/html/hostjane.com/public_html/gatsby

	# Enable SSL
	SSLEngine on
	SSLCertificateFile /etc/ssl/certs/hostjane_com.crt
	SSLCertificateKeyFile /etc/ssl/private/hostjane_com.key

	# Log errors
	ErrorLog "| /usr/bin/rotatelogs /var/www/html/hostjane.com/logs/errorlog.%Y-%m-%d-%H_%M_%S 2M"
	CustomLog "| /usr/bin/rotatelogs /var/www/html/hostjane.com/logs/accesslog.%Y-%m-%d-%H_%M_%S 2M" combined

	# Directory indexes
	DirectoryIndex index.html index.php

	# Error documents
	ErrorDocument 404 /404/index.html
	ErrorDocument 403 /404/index.html

	# Aliases for some other directories
	Alias "/assets" "/var/www/html/hostjane.com/public_html/assets"
	Alias "/config2" "/var/www/html/hostjane.com/public_html/config2"
	Alias "/css" "/var/www/html/hostjane.com/public_html/css"
	Alias "/js" "/var/www/html/hostjane.com/public_html/js"
	Alias "/img" "/var/www/html/hostjane.com/public_html/img"
	Alias "/marketplace" "/var/www/html/hostjane.com/public_html/marketplace"
	Alias "/webfonts" "/var/www/html/hostjane.com/public_html/webfonts"
	Alias "/wp-content" "/var/www/html/hostjane.com/public_html/wp-content"
	Alias "/webhost" "/var/www/html/hostjane.com/public_html/webhost"

</VirtualHost>
