function getFiveColumns(parent) {
	const columns = [];
	Array.from(parent.querySelectorAll('.footer-column-class')).forEach(
		column => {
			const singleColumn = {
				title: column.querySelector('h3.footer-column-h3').textContent,
				items: [],
			};

			Array.from(column.querySelectorAll('.footer-li-a')).forEach(
				anchor => {
					singleColumn.items.push({
						title: anchor.textContent,
						internal: false,
						link: anchor.getAttribute('href'),
					});
				}
			);

			columns.push(singleColumn);
		}
	);
	return columns;
}
