import { ReactComponent as SellerIcon } from './seller.svg';
import { ReactComponent as DiscussionIcon } from './discussion.svg';
import { ReactComponent as BlogIcon } from './blog.svg';
import { ReactComponent as SupportIcon } from './support.svg';
import { ReactComponent as TwitterIcon } from './twitter.svg';
import { ReactComponent as YoutubeIcon } from './youtube.svg';
import { ReactComponent as LinkedinIcon } from './linkedin.svg';
import { ReactComponent as PinterestIcon } from './pinterest.svg';
import { ReactComponent as DiscordIcon } from './discord.svg';
import { ReactComponent as RedditIcon } from './reddit.svg';

export default {
	fiverrPart: {
		socials: {
			twitter: {
				link: 'https://twitter.com/janeispowerful',
				title: 'Twitter',
				icon: TwitterIcon,
			},
			reddit: {
				link: '#',
				title: 'Reddit',
				icon: RedditIcon,
			},
			youtube: {
				link: 'https://youtube.com/channel/UCcUdymV3_eSuUrxJSAEonkA',
				title: 'YouTube',
				icon: YoutubeIcon,
			},
			linkedIn: {
				link: 'https://linkedin.com/company/hostjane',
				title: 'LinkedIn',
				icon: LinkedinIcon,
			},
			pinterest: {
				link: 'https://www.pinterest.com.au/hostjane/',
				title: 'Pinterest',
				icon: PinterestIcon,
			},
			discord: {
				link: 'https://discord.gg/5rS6Tvd',
				title: 'Discord',
				icon: DiscordIcon,
			},
		},
		description:
			'We help businesses do smarter work, highlighted by our principles of payment protection, lower costs, simplicity, quality and variety.',
	},
	footLinks: {
		copyright: {
			link: '/',
			internal: true,
			title: `© ${new Date().getFullYear()} HostJane, Inc`,
		},
		jip: {
			title: 'Jane is powerful',
			link: '/about-jane/',
			internal: true,
		},
		others: [
			{
				link: '/legal/tos/',
				internal: true,
				title: 'General TOS',
			},
			{
				link: '/legal/marketplace-terms/',
				internal: true,
				title: 'Marketplace Terms',
			},
			{
				link: '/legal/hosting-terms/',
				internal: true,
				title: 'Hosting Terms',
			},
			{
				link: '/legal/privacy/',
				internal: true,
				title: 'Privacy Policy',
			},
			{
				link: '/legal/gdpr-privacy-notice/',
				internal: true,
				title: 'GDPR',
			},
			{
				link: '/legal/cookie-policy/',
				internal: true,
				title: 'Cookies',
			},
			{
				link: '/legal/use-policy/',
				internal: true,
				title: 'AUP',
			},
			{
				link: '/legal/sla/',
				internal: true,
				title: 'SLA',
			},
			{
				link: '/sitemap/',
				internal: false,
				title: 'Sitemap',
			},
		],
	},
	columns: [
		{
			title: 'Marketplace',
			items: [
				{
					title: 'Web / Mobile / Tech',
					internal: false,
					link: '/marketplace/categories/web-mobile-tech-freelancers',
				},
				{
					title: 'Design / Art & More',
					internal: false,
					link: '/marketplace/categories/design-art-video-voice',
				},
				{
					title: 'Online Tutors',
					internal: false,
					link: '/marketplace/categories/online-tutors',
				},
				{
					title: 'Writing / Translation',
					internal: false,
					link:
						'/marketplace/categories/writing-translation-freelancers',
				},
				{
					title: 'Business / Admin',
					internal: false,
					link: '/marketplace/categories/business-admin-support',
				},
				{
					title: 'All Categories',
					internal: false,
					link: '/marketplace/categories/',
				},
				{
					title: 'Browse Skills',
					internal: false,
					link: '/marketplace/skills/',
				},
			],
		},
		{
			title: 'VPS Shop',
			items: [
				{
					title: 'Explore All',
					internal: true,
					link: '/hosting/',
				},
				{
					title: 'VPS Hosting',
					internal: true,
					link: '/hosting/vps-hosting-wizard/',
				},
				{
					title: 'Cloud Servers',
					internal: true,
					link: '/hosting/cloud-hosting-wizard/',
				},
				{
					title: 'Dedicated Servers',
					internal: true,
					link: '/hosting/dedicated-servers-hosting-wizard/',
				},
				{
					title: 'Reseller Hosting',
					internal: true,
					link: '/hosting/reseller-hosting-wizard/',
				},
				{
					title: 'Hosting FAQ',
					internal: false,
					link: 'https://support.hostjane.com/servers',
				},
				{
					title: 'VPS Guides',
					internal: false,
					link: 'https://support.hostjane.com/vps-setup',
				},
				{
					title: 'Linux Tutorials',
					internal: false,
					link: 'https://support.hostjane.com/cloud-setup',
				},
				{
					title: 'Hosting Login',
					internal: false,
					link: 'https://www.hostjane.com/webhost/client/login',
				},
			],
		},
		{
			title: 'Help',
			items: [
				{
					title: 'Tutorials',
					internal: false,
					link: '#',
				},
				{
					title: 'Q&A',
					internal: false,
					link: '#',
				},
				{
					title: 'Discord Chat',
					internal: false,
					link: 'https://discord.gg/5rS6Tvd',
				},
				{
					title: 'Contact Support',
					internal: false,
					link:
						'https://hostjane.atlassian.net/servicedesk/customer/portal/1',
				},
				{
					title: 'Submit a Dispute',
					internal: true,
					link: '/disputes/',
				},
				{
					title: 'Sitemap',
					internal: true,
					link: '/sitemap/',
				},
			],
		},
		{
			title: 'Sell Skills',
			items: [
				{
					title: 'Sell / Teach Online',
					internal: false,
					link: '#',
				},
				{
					title: 'Orders',
					internal: false,
					link: '#',
				},
				{
					title: 'Payouts',
					internal: false,
					link: '#',
				},
				{
					title: 'Listing Center',
					internal: false,
					link: '#',
				},
				{
					title: 'Create Listing',
					internal: false,
					link: '#',
				},
				{
					title: 'Studio Settings',
					internal: false,
					link: '#',
				},
				{
					title: 'Account Settings',
					internal: false,
					link: '#',
				},
				{
					title: 'Security',
					internal: false,
					link: '#',
				},
			],
		},
		{
			title: 'Company',
			mobileTitle: 'Company & More',
			items: [
				{
					title: 'A Connected World',
					internal: false,
					link: '#',
				},
				{
					title: 'Feedback',
					internal: false,
					link: '#',
				},
				{
					title: 'Newsroom',
					internal: false,
					link: 'http://support.hostjane.com/newsroom',
				},
				{
					title: 'Advertising',
					internal: false,
					link: '#',
				},
			],
		},
	],
	connect: {
		title: 'Connect',
		withIcons: [
			{
				icon: SellerIcon,
				title: 'Sell / Teach',
				link: '/sell/',
				internal: true,
			},
			{
				icon: DiscussionIcon,
				title: 'Community',
				link: '/community',
				internal: false,
			},
			{
				icon: BlogIcon,
				title: 'Submit a Dispute',
				link: '/disputes/',
				internal: true,
			},
			{
				icon: SupportIcon,
				title: 'Contact Support',
				link:
					'https://hostjane.atlassian.net/servicedesk/customer/portal/1',
				internal: false,
			},
		],
		smallOne: [
			{
				title: 'Customer Login',
				link: '/login/',
				internal: true,
			},
			{
				title: 'Use Policy',
				link: '/legal/use-policy/',
				internal: true,
			},
			{
				title: 'Cookies Policy',
				link: '/legal/cookie-policy/',
				internal: true,
			},
		],
		smallTwo: [
			{
				title: 'Payout Settings',
				link: '#',
				internal: false,
			},
			{
				title: 'My Balance',
				link: '#',
				internal: false,
			},
		],
	},
};
