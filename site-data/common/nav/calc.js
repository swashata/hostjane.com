// Calculator function for Mobile Menu stuff
// Select the primary UL from DOM on Chrome Dev Tool
// Pass it as parameter to this function
function recursivelyGetMobileNav(parent) {
	console.log(parent);
	return Array.from(parent.querySelectorAll('li'))
		.map(list => {
			// If this is a link, then just follow through
			const link = list.querySelector('a');
			if (link) {
				const title = link.textContent.trim();
				let strong = false;
				// If this a View All thing, then add strong
				if (title.toLowerCase().startsWith('view all')) {
					strong = true;
				}
				return {
					title,
					strong,
					link: link.getAttribute('href'),
					internal: false,
				};
			}

			const flagBody = list.querySelector('.flag-body');
			if (flagBody) {
				const subParent = document.querySelector(
					`#mobile-sub-catnav-content-${list.dataset.uid} ul`
				);
				if (subParent) {
					return {
						title: flagBody.textContent.trim(),
						navs: recursivelyGetMobileNav(subParent),
					};
				}
				return false;
			}

			return false;
		})
		.filter(Boolean);
}

// Call by passing the parent LI for the first "BUY HOSTING" menu
function getDesktopNavHosting(parent) {
	console.log(parent);
	const parentAnchor = parent.querySelector('a');
	const navs = [];
	Array.from(parent.querySelectorAll('aside')).forEach(aside => {
		navs.push({
			items: Array.from(aside.querySelectorAll('a')).map(anchor => {
				const titleH4 = anchor.querySelector('h4');
				const subtitleSpan = anchor.querySelector('.text-muted');
				return {
					title: titleH4
						? titleH4.textContent.trim()
						: anchor.textContent.trim(),
					subtitle: subtitleSpan
						? subtitleSpan.textContent.trim()
						: '',
					href: anchor.getAttribute('href'),
					type: anchor.classList.contains('btn') ? 'button' : 'full',
				};
			}),
		});
	});
	return {
		title: parentAnchor.textContent.trim(),
		link: parentAnchor.getAttribute('href'),
		navs,
	};
}

// Call by passing the parent LI for everything after "BUY HOSTING" menu, but not the last HELP menu.
function getDesktopOtherNavHosting(parent) {
	console.log(parent);
	const parentAnchor = parent.querySelector('a');
	const navs = [];
	Array.from(parent.querySelectorAll('ul')).forEach(ul => {
		navs.push({
			title: ul.querySelector('p.text-muted').textContent.trim(),
			items: Array.from(ul.querySelectorAll('a')).map(anchor => {
				const titleP = anchor.querySelector('.mb-1');
				const subtitleSpan = anchor.querySelector('.text-muted');
				return {
					title: titleP
						? titleP.textContent.trim()
						: anchor.textContent.trim(),
					subtitle: subtitleSpan
						? subtitleSpan.textContent.trim()
						: '',
					href: anchor.getAttribute('href'),
					type: anchor.classList.contains('btn') ? 'button' : 'full',
				};
			}),
		});
	});
	return {
		title: parentAnchor.textContent.trim(),
		link: parentAnchor.getAttribute('href'),
		navs,
	};
}

// Call by passing the parent LI for "HELP".
function getDesktopHelpNavHosting(parent) {
	console.log(parent);
	const parentAnchor = parent.querySelector('a');
	const navs = [];
	Array.from(parent.querySelectorAll('.categories-nav-help')).forEach(ul => {
		navs.push({
			title: '',
			items: Array.from(ul.querySelectorAll('a')).map(anchor => {
				const titleP = anchor.querySelector('.mb-1');
				const subtitleSpan = anchor.querySelector('.text-muted');
				return {
					title: titleP
						? titleP.textContent.trim()
						: anchor.textContent.trim(),
					subtitle: subtitleSpan
						? subtitleSpan.textContent.trim()
						: '',
					href: anchor.getAttribute('href'),
					type: anchor.classList.contains('btn') ? 'button' : 'big',
				};
			}),
		});
	});
	return {
		title: parentAnchor.textContent.trim(),
		link: parentAnchor.getAttribute('href'),
		navs,
	};
}
