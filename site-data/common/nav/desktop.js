import React from 'react';
import { ReactComponent as LockIcon } from '../../../src/svgs/icons/lock.svg';
import { ReactComponent as FreelancersIcon } from '../../home/hero/freelancers.svg';
import { ReactComponent as WebHostingIcon } from '../../home/hero/web-hosting.svg';
import { ReactComponent as HostJaneIcon } from '../../../src/svgs/nav/HostJane.svg';
import { ReactComponent as ContactSupportIcon } from '../../../src/svgs/icons/contact-support.svg';
import { ReactComponent as SellerGuideLinesIcon } from '../../../src/svgs/icons/seller-guidelines.svg';
import { ReactComponent as DiscordIcon } from '../../../src/svgs/icons/discord.svg';
import { ReactComponent as PopularAnswersIcon } from '../../../src/svgs/icons/popular-answers.svg';

export default {
	topNav: {
		sellTeach: {
			title: 'Sell / Teach',
			heading: 'Start selling or teaching',
			subheading: 'Turn your skills into a new income stream',
			link: '/sell',
			links: [
				[
					{
						title: 'Get started!',
						link: 'https://www.hostjane.com/marketplace',
						icon: HostJaneIcon,
						className: 'hostjane-main',
					},
					{
						title: 'Contact us',
						link: '#',
						icon: ContactSupportIcon,
					},
				],
				[
					{
						title: 'Seller guidelines',
						link: '/sell',
						icon: SellerGuideLinesIcon,
					},
					{
						title: 'Seller chat',
						link: '#',
						icon: DiscordIcon,
					},
				],
			],
			popularAnswers: {
				title: 'Popular answers',
				link: 'https://answers.hostjane.com',
				icon: PopularAnswersIcon,
				links: [
					{
						title: 'Finding your first customer',
						link: '#',
					},
					{
						title: 'Tax information',
						link: '#',
					},
					{
						title: 'How to register for payouts',
						link: '#',
					},
				],
			},
		},
		navs: [
			{
				title: 'Community',
				link: '/community',
			},
			{
				title: 'Join',
				link: 'https://www.hostjane.com/marketplace/register',
			},
			{
				title: (
					<span>
						Sign In <em>to</em>
					</span>
				),
				popupTitle: 'Sign In',
				highlighted: true,
				icon: LockIcon,
				children: [
					{
						title: 'Marketplace',
						href: 'https://www.hostjane.com/marketplace/login',
						icon: FreelancersIcon,
					},
					{
						title: 'Hosting',
						href: '/webhost/client/login',
						icon: WebHostingIcon,
					},
				],
			},
		],
		skills: {
			title: 'Skills',
			link: 'https://www.hostjane.com/marketplace/skills',
		},
	},
	// search categories
	searchCats: [
		{
			title: 'Search All',
			value: 'all',
		},
		{
			title: 'Web / Mobile / Tech',
			value: 'web-mobile-tech',
		},
		{
			title: 'Design / Art / Video / Voice',
			value: 'design-art-video-voice',
		},
		{
			title: 'Online Tutors',
			value: 'online-tutors',
		},
		{
			title: 'Writing / Translation',
			value: 'writing-translation',
		},
		{
			title: 'Business / Admin',
			value: 'business-admin',
		},
	],
	// First Buy Hosting, highlighted
	highlighted: {
		title: 'VPS Shop',
		link: '/hosting/',
		navs: [
			{
				items: [
					{
						title: 'Explore All',
						subtitle: 'From $7.95 / mo',
						href: '/hosting/',
						type: 'big',
						internal: true,
					},
					{
						title: 'Managed VPS CMS',
						subtitle: 'From $19.99 / mo',
						href: '/hosting/vps-hosting-wizard/?appType=1&app=0',
						type: 'big',
						internal: true,
					},
					{
						title: 'Managed VPS cPanel',
						subtitle: 'From $39.99 / mo',
						href: '/hosting/vps-hosting-wizard/?appType=0&app=0',
						type: 'big',
						internal: true,
					},
				],
			},
			{
				items: [
					{
						title: 'Linux VPS',
						subtitle: 'From $7.95 / mo',
						href: '/hosting/vps-hosting-wizard/?appType=3&app=0',
						type: 'big',
						internal: true,
					},
					{
						title: 'Cloud Compute',
						subtitle: 'From $0.0139 / hr',
						href: '/hosting/cloud-hosting-wizard/',
						type: 'big',
						internal: true,
					},
					{
						title: 'Dedicated Servers',
						subtitle: 'From $149 / mo',
						href: '/hosting/reseller-hosting-wizard',
						type: 'big',
						internal: true,
					},
				],
			},
			{
				items: [
					{
						title: 'Reseller Hosting',
						subtitle: 'White-label SSD VPS',
						href: '/webhost/client/login/',
						type: 'big',
						internal: true,
					},
					{
						title: 'Contact Support',
						subtitle: '24/7 Dedicated Experts',
						href:
							'https://hostjane.atlassian.net/servicedesk/customer/portal/1',
						type: 'big',
					},
					{
						title: 'Hosting login',
						subtitle: '',
						href: '/webhost/client/login/',
						type: 'button',
						internal: true,
					},
				],
			},
			{
				items: [
					{
						title: 'Server Guides',
						subtitle: 'Easy set-up & linux tutorials',
						href: 'https://support.hostjane.com/',
						type: 'image',
						imageUrl:
							'https://www.hostjane.com/assets/theme-plugin-reviews.svg',
					},
				],
			},
		],
	},
	// Then others
	others: [
		{
			title: 'Web / Mobile / Tech',
			link:
				'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers',
			navs: [
				{
					title: 'Highest demand',
					items: [
						{
							title: 'Web Development',
							subtitle: 'Build, code & maintain websites',
							href:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/web-development',
							type: 'full',
						},
						{
							title: 'Programming / Software',
							subtitle: 'Plan, write & design source code',
							href:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software',
							type: 'full',
						},
						{
							title: 'Apps / Mobile',
							subtitle: 'Design secure applications',
							href:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/apps-mobile',
							type: 'full',
						},
						{
							title: 'Database Design / Administration',
							subtitle: 'Create, maintain & secure data',
							href:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/database-design-administration-1',
							type: 'full',
						},
						{
							title: 'WordPress',
							subtitle: 'Develop WP themes & plugins',
							href:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/wordpress-1',
							type: 'full',
						},
						{
							title: 'Networking / System Admin',
							subtitle: 'Install & maintain systems',
							href:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/networking-system-admin-1',
							type: 'full',
						},
						{
							title: 'QA / Testing',
							subtitle: 'Troubleshoot & test systems',
							href:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/qa-testing-1',
							type: 'full',
						},
					],
				},
				{
					title: 'Featured services',
					items: [
						{
							title: 'Information Security',
							subtitle: 'Protect sensitive data',
							href:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/information-security-1',
							type: 'full',
						},
						{
							title: 'Ideas / Help / Consultation',
							subtitle: 'Consult with IT experts',
							href:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/ideas-help-consultation-1',
							type: 'full',
						},
						{
							title: 'Technical Support',
							subtitle: 'Remote help & troubleshooting',
							href:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/technical-support-1',
							type: 'full',
						},
						{
							title: 'Bug Fixing Services',
							subtitle: 'Debug & resolve issues',
							href:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/bug-fixing-services-1',
							type: 'full',
						},
						{
							title: 'ERP / CRM / SCM',
							subtitle: 'Streamline your business',
							href:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/erp-crm-scm-1',
							type: 'full',
						},
						{
							title: 'Management / Training',
							subtitle: 'Allocate resources properly',
							href:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/management-training-1',
							type: 'full',
						},
						{
							title: 'SAP',
							subtitle: 'Find SAP experts',
							href:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/sap-1',
							type: 'full',
						},
					],
				},
				{
					title: 'Key services',
					items: [
						{
							title: 'Telephony / Telecommunications',
							subtitle: 'Voice & sound technicians',
							href:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/telephony-telecommunications-1',
							type: 'full',
						},
						{
							title: 'Math / Science / Algorithms',
							subtitle: 'Scientists for hire',
							href:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/math-science-algorithms-1',
							type: 'full',
						},
						{
							title: 'Game Development',
							subtitle: 'Build, test, design games',
							href:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/game-development-1',
							type: 'full',
						},
					],
				},
			],
		},
		{
			title: 'Design / Art / Video / Voice',
			link:
				'https://www.hostjane.com/marketplace/categories/design-art-video-voice',
			navs: [
				{
					title: 'Highest demand',
					items: [
						{
							title: 'Web Design / Apps',
							subtitle: 'Create the perfect website',
							href:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/web-design-app-design',
							type: 'full',
						},
						{
							title: 'Graphic Design / Logos',
							subtitle: 'Expert designers for less',
							href:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/graphic-design-logos',
							type: 'full',
						},
						{
							title: 'Freelance Artists / Other Art',
							subtitle: 'Hire creative talent',
							href:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/freelance-artist-other-art',
							type: 'full',
						},
						{
							title: 'Video / Animation',
							subtitle: 'Make amazing productions',
							href:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/video-animation',
							type: 'full',
						},
						{
							title: 'Illustration',
							subtitle: 'Skilled artists on-demand',
							href:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration',
							type: 'full',
						},
						{
							title: 'Voice Over / Acting',
							subtitle: 'Find the perfect voice',
							href:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/voice-over-acting',
							type: 'full',
						},
						{
							title: 'Studio Musicians / Session Singers',
							subtitle: 'Ready-to-record talent',
							href:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/studio-musicians-session-singers',
							type: 'full',
						},
					],
				},
				{
					title: 'Featured services',
					items: [
						{
							title: 'Business / Advertising Design',
							subtitle: 'Empower your brand',
							href:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/business-advertising-design',
							type: 'full',
						},
						{
							title: 'Book / Magazine Design',
							subtitle: 'Create eye-catching designs',
							href:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/book-magazine-design',
							type: 'full',
						},
						{
							title: 'Audio / Sound / Music',
							subtitle: 'From mixing to audio editing',
							href:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/audio-sound-music',
							type: 'full',
						},
						{
							title: 'T-Shirts / Merchandise Design',
							subtitle: 'Create your clothing brand',
							href:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/t-shirts-merchandise-design',
							type: 'full',
						},
						{
							title: 'Packaging / Label Design',
							subtitle: 'Hire packaging designers',
							href:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/packaging-label-design',
							type: 'full',
						},
						{
							title: 'CAD / Technical Drawings',
							subtitle: 'Drafting & modeling experts',
							href:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings',
							type: 'full',
						},
						{
							title: 'Image Restoration / Editing',
							subtitle: 'Hire photoshop gurus',
							href:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/image-restoration-editing',
							type: 'full',
						},
					],
				},
				{
					title: 'Key services',
					items: [
						{
							title: 'Photography',
							subtitle: 'Freelance photographers',
							href:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/photography',
							type: 'full',
						},
						{
							title: 'File Conversions',
							subtitle: 'Manage all file types',
							href:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/file-converting',
							type: 'full',
						},
						{
							title: 'Printing / Production',
							subtitle: 'Prepress to desktop publishing',
							href:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/concepts-direction',
							type: 'full',
						},
						{
							title: 'Painting',
							subtitle: 'Watercolor to digital painting',
							href:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/painting',
							type: 'full',
						},
						{
							title: 'Cartoons / Comic Art',
							subtitle: 'Talented cartoonists for hire',
							href:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cartoons-comic-art',
							type: 'full',
						},
						{
							title: 'Fashion',
							subtitle: 'From textiles to jewelry design',
							href:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/fashion',
							type: 'full',
						},
					],
				},
			],
		},
		{
			title: 'Online Tutors',
			link:
				'https://www.hostjane.com/marketplace/categories/online-tutors',
			navs: [
				{
					title: 'Highest demand',
					items: [
						{
							title: 'Language Tutors',
							subtitle: 'Learn a new language!',
							href:
								'https://www.hostjane.com/marketplace/categories/online-tutors/language-tutors-1',
							type: 'full',
						},
						{
							title: 'Online Interpreting Services',
							subtitle: 'Hire remote interpreters',
							href:
								'https://www.hostjane.com/marketplace/categories/online-tutors/online-interpreting-services',
							type: 'full',
						},
						{
							title: 'Music Lessons',
							subtitle: 'Learn a new instrument',
							href:
								'https://www.hostjane.com/marketplace/categories/online-tutors/music-lessons',
							type: 'full',
						},
						{
							title: 'Investing & Trading Lessons',
							subtitle: 'Learn how to trade',
							href:
								'https://www.hostjane.com/marketplace/categories/online-tutors/investing-trading-lessons',
							type: 'full',
						},
						{
							title: 'Finance / Accounting Lessons',
							subtitle: 'Learn accounting & tax prep',
							href:
								'https://www.hostjane.com/marketplace/categories/online-tutors/finance-accounting-lessons',
							type: 'full',
						},
						{
							title: 'Business Training',
							subtitle: 'Teach your staff skills',
							href:
								'https://www.hostjane.com/marketplace/categories/online-tutors/business-training',
							type: 'full',
						},
						{
							title: 'IT / Computing',
							subtitle: 'Learn how to code',
							href:
								'https://www.hostjane.com/marketplace/categories/online-tutors/technology-computer-science',
							type: 'full',
						},
					],
				},
				{
					title: 'Featured services',
					items: [
						{
							title: 'Test Prep Tutors',
							subtitle: 'Improve your grades',
							href:
								'https://www.hostjane.com/marketplace/categories/online-tutors/test-prep-tutors',
							type: 'full',
						},
						{
							title: 'Math Lessons',
							subtitle: 'Hire math tutors',
							href:
								'https://www.hostjane.com/marketplace/categories/online-tutors/math-lessons',
							type: 'full',
						},
						{
							title: 'Science Lessons',
							subtitle: 'Master science concepts',
							href:
								'https://www.hostjane.com/marketplace/categories/online-tutors/science-lessons',
							type: 'full',
						},
						{
							title: 'English Lessons',
							subtitle: 'Improve your English',
							href:
								'https://www.hostjane.com/marketplace/categories/online-tutors/english-lessons',
							type: 'full',
						},
						{
							title: 'Creative / Design Lessons',
							subtitle: 'Hire art teachers',
							href:
								'https://www.hostjane.com/marketplace/categories/online-tutors/creative-lessons',
							type: 'full',
						},
						{
							title: 'Teacher Training',
							subtitle: 'Become a better teacher',
							href:
								'https://www.hostjane.com/marketplace/categories/online-tutors/teacher-training',
							type: 'full',
						},
						{
							title: 'History Lessons',
							subtitle: 'Learn how to research',
							href:
								'https://www.hostjane.com/marketplace/categories/online-tutors/history-lessons',
							type: 'full',
						},
					],
				},
				{
					title: 'Key services',
					items: [
						{
							title: 'Health & Fitness Teachers',
							subtitle: 'Learn with qualified trainers',
							href:
								'https://www.hostjane.com/marketplace/categories/online-tutors/health-fitness-teachers',
							type: 'full',
						},
						{
							title: 'Office Productivity',
							subtitle: 'Learn Microsoft & Apple products',
							href:
								'https://www.hostjane.com/marketplace/categories/online-tutors/office-productivity',
							type: 'full',
						},
						{
							title: 'Personal Development',
							subtitle: 'The best version of yourself',
							href:
								'https://www.hostjane.com/marketplace/categories/online-tutors/personal-development',
							type: 'full',
						},
						{
							title: 'Lifestyle Lessons',
							subtitle: 'From sailing to dog training',
							href:
								'https://www.hostjane.com/marketplace/categories/online-tutors/lifestyle-lessons',
							type: 'full',
						},
					],
				},
			],
		},
		{
			title: 'Writing / Translation',
			link:
				'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers',
			navs: [
				{
					title: 'Highest demand',
					items: [
						{
							title: 'Translation',
							subtitle: 'Expert human translators',
							href:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/translation-freelancers',
							type: 'full',
						},
						{
							title: 'Creative Writing',
							subtitle: 'Hire talented wordsmiths',
							href:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/creative-writing',
							type: 'full',
						},
						{
							title: 'Book Writing',
							subtitle: 'Make your book shine',
							href:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/book-writing',
							type: 'full',
						},
						{
							title: 'General Writing',
							subtitle: 'Experienced writers for hire',
							href:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/general-writing',
							type: 'full',
						},
						{
							title: 'Copywriting',
							subtitle: 'Devise copy that sells',
							href:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/copywriting',
							type: 'full',
						},
						{
							title: 'Editing / Proofreading',
							subtitle: 'Hire the right editor',
							href:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/editing-proofreading',
							type: 'full',
						},
						{
							title: 'Web Content',
							subtitle: 'Hire specialist online writers',
							href:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/web-content',
							type: 'full',
						},
					],
				},
				{
					title: 'Featured services',
					items: [
						{
							title: 'Article / News Writing',
							subtitle: 'Hire on-demand journalists',
							href:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/article-news-writing',
							type: 'full',
						},
						{
							title: 'Subtitling',
							subtitle: 'Hire skilled subtitlers',
							href:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/subtitling',
							type: 'full',
						},
						{
							title: 'Research',
							subtitle: 'Hire professional researchers',
							href:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/research',
							type: 'full',
						},
						{
							title: 'Writing for Industries',
							subtitle: 'Niche writers on-demand',
							href:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/writing-for-industries',
							type: 'full',
						},
						{
							title: 'Scripts / Speeches / Storyboards',
							subtitle: 'Hire drama & genre writers',
							href:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/scripts-speeches-storyboards',
							type: 'full',
						},
						{
							title: 'Jobs / Resumes',
							subtitle: 'Improve your CV',
							href:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/jobs-resumes',
							type: 'full',
						},
						{
							title: 'Review Writing',
							subtitle: 'Write critiques & commentary',
							href:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/review-writing',
							type: 'full',
						},
					],
				},
				{
					title: 'Key services',
					items: [
						{
							title: 'Grants / Proposals',
							subtitle: 'Strengthen your proposals',
							href:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/grants-proposals',
							type: 'full',
						},
						{
							title: 'Songs / Poems',
							subtitle: 'Creative poets on-demand',
							href:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/songs-poems',
							type: 'full',
						},
					],
				},
			],
		},
		{
			title: 'Business',
			link:
				'https://www.hostjane.com/marketplace/categories/business-admin-support',
			navs: [
				{
					title: 'Highest demand',
					items: [
						{
							title: 'Digital Marketing',
							subtitle: 'Reach more eyeballs',
							href:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/digital-marketing-freelancers',
							type: 'full',
						},
						{
							title: 'Accounting / Finance / Tax',
							subtitle: 'Make tax season easy',
							href:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa',
							type: 'full',
						},
						{
							title: 'Business Help / Consulting',
							subtitle: 'Get a fresh pair of eyes',
							href:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/business-help-consultation',
							type: 'full',
						},
						{
							title: 'Legal Assistance',
							subtitle: 'Hire freelance lawyers',
							href:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance',
							type: 'full',
						},
						{
							title: 'Personal / Virtual Assistants',
							subtitle: 'Hire a cloud workforce',
							href:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/personal-virtual-assistants',
							type: 'full',
						},
						{
							title: 'Transcription Services',
							subtitle: 'Turn speech into printed words',
							href:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/transcription-freelancers',
							type: 'full',
						},
						{
							title: 'Sales / General Marketing',
							subtitle: 'Hone your brand message',
							href:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/sales-general-marketing',
							type: 'full',
						},
					],
				},
				{
					title: 'Featured services',
					items: [
						{
							title: 'Press Release Writing',
							subtitle: 'Hire marketing gurus',
							href:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/press-release-writing',
							type: 'full',
						},
						{
							title: 'Call Centers / Telemarketing',
							subtitle: 'Power your business goals',
							href:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/call-centers-telemarketing',
							type: 'full',
						},
						{
							title: 'Data Entry',
							subtitle: 'Accurate update & input data',
							href:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/data-entry',
							type: 'full',
						},
						{
							title: 'Microsoft Office Software',
							subtitle: 'Hire MS experts',
							href:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/microsoft-office-software',
							type: 'full',
						},
						{
							title: 'Customer Service',
							subtitle: 'Hire phone to email support',
							href:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/customer-service',
							type: 'full',
						},
						{
							title: 'Word Processing / Typing',
							subtitle: 'Get help with business docs',
							href:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/word-processing-typing',
							type: 'full',
						},
						{
							title: 'Spreadsheets / Data Manipulation',
							subtitle: 'Hire excel professionals',
							href:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/spreadsheets-data-manipulation',
							type: 'full',
						},
					],
				},
				{
					title: 'Key services',
					items: [
						{
							title: 'Mailings / Lists',
							subtitle: 'Ethical email marketing',
							href:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/mailings-lists',
							type: 'full',
						},
						{
							title: 'Human Resources (HR)',
							subtitle: 'Help recruiting & managing staff',
							href:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/human-resources',
							type: 'full',
						},
					],
				},
			],
		},
		{
			title: 'Support',
			link: '#',
			navs: [
				{
					title: '',
					items: [
						{
							title: 'Ask Jane',
							subtitle: 'Search our help center',
							href: 'https://support.hostjane.com',
							type: 'full',
						},
						{
							title: 'Community',
							subtitle: 'Chat, gain & share knowledge',
							href: '/community',
							type: 'full',
						},
						{
							title: 'Seller Guide',
							subtitle: 'FAQs & resources',
							href: 'https://support.hostjane.com/seller-guide',
							type: 'full',
						},
						{
							title: 'VPS Features',
							subtitle: 'Server info & specs',
							href: 'https://support.hostjane.com/servers/vps',
							type: 'full',
						},
						{
							title: 'VPS Tutorials',
							subtitle: 'Help setting up',
							href:
								'https://support.hostjane.com/servers/vps-setup',
							type: 'full',
						},
						{
							title: 'Cloud Help',
							subtitle: 'Review linux tutorials',
							href: 'https://support.hostjane.com/cloud-setup',
							type: 'full',
						},
						{
							title: 'Contact Support',
							subtitle: '',
							href:
								'https://hostjane.atlassian.net/servicedesk/customer/portal/1',
							type: 'button',
						},
					],
				},
			],
		},
	],
};
