import React from 'react';
import { ReactComponent as SkillsIcon } from '../../../src/svgs/homenav/skills-sidebar.svg';
import { ReactComponent as CategoriesIcon } from '../../../src/svgs/homenav/categories.svg';
import { ReactComponent as SellIcon } from './sell.svg';
import { ReactComponent as LockIcon } from '../../../src/svgs/icons/lock.svg';
import { ReactComponent as FreelancersIcon } from '../../home/hero/freelancers.svg';
import { ReactComponent as WebHostingIcon } from '../../home/hero/web-hosting.svg';

export default {
	title: 'Browse Categories',
	footerButton: {
		title: 'All Categories',
		link: 'https://www.hostjane.com/marketplace/categories',
		internal: false,
	},
	primaryLinks: [
		{
			title: 'Join',
			highlighted: false,
			href: 'https://www.hostjane.com/marketplace/register',
		},
		{
			title: (
				<span>
					Sign In <em>to</em>
				</span>
			),
			popupTitle: 'Sign In',
			highlighted: true,
			icon: LockIcon,
			children: [
				{
					title: 'Marketplace',
					href: 'https://www.hostjane.com/marketplace/login',
					icon: FreelancersIcon,
				},
				{
					title: 'Hosting',
					href: '/webhost/client/login',
					icon: WebHostingIcon,
				},
			],
		},
	],
	navs: [
		{
			title: 'VPS Shop',
			navs: [
				{
					title: 'Explore All',
					strong: false,
					link: '/hosting/',
					internal: true,
				},
				{
					title: 'WordPress VPS',
					strong: false,
					link: '/hosting/vps-hosting-wizard/?appType=1&app=0',
					internal: true,
				},
				{
					title: 'cPanel VPS',
					strong: false,
					link: 'hosting/vps-hosting-wizard/?appType=0&app=0',
					internal: true,
				},
				{
					title: 'Linux-Only VPS',
					strong: false,
					link: '/hosting/vps-hosting-wizard/?appType=3&app=0',
					internal: true,
				},
				{
					title: 'Cloud Servers',
					strong: false,
					link: '/hosting/cloud-hosting-wizard/',
					internal: false,
				},
				{
					title: 'Dedicated Servers',
					strong: false,
					link: '/hosting/dedicated-servers-hosting-wizard/',
					internal: false,
				},
				{
					title: 'Customer Login',
					strong: false,
					link: 'https://www.hostjane.com/webhost/client/login/',
					internal: false,
				},
			],
		},
		{
			title: 'Web / Mobile / Tech',
			navs: [
				{
					title: 'View All',
					strong: true,
					link:
						'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers',
					internal: false,
				},
				{
					title: 'Web Development',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/web-development',
							internal: false,
						},
						{
							title: 'Backend Development',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/web-development/backend-development',
							internal: false,
						},
						{
							title: 'Frontend Development',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/web-development/frontend-development',
							internal: false,
						},
						{
							title: 'Full Stack Developers',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/web-development/full-stack-developers',
							internal: false,
						},
						{
							title: 'PHP Framework',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/web-development/php-framework',
							internal: false,
						},
						{
							title: 'HTML / CSS',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/web-development/html-css',
							internal: false,
						},
						{
							title: 'Content Management System (CMS)',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/web-development/content-management-system-cms',
							internal: false,
						},
						{
							title: 'UX / UI / Responsive',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/web-development/ux-ui-responsive',
							internal: false,
						},
						{
							title: 'ECommerce',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/web-development/ecommerce',
							internal: false,
						},
						{
							title: 'Web Servers',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/web-development/web-servers',
							internal: false,
						},
						{
							title: 'Refactoring / Rewriting Code',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/web-development/refactoring-rewriting-code',
							internal: false,
						},
						{
							title: 'Templates / Wireframes / Mockups',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/web-development/templates-wireframes-mockups',
							internal: false,
						},
						{
							title: 'Social Media / Networks',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/web-development/social-media-networks',
							internal: false,
						},
						{
							title: 'Text Search Engines',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/web-development/text-search-engines',
							internal: false,
						},
						{
							title: 'Domains / Hosting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/web-development/domains-hosting',
							internal: false,
						},
						{
							title: 'Maintenance / Security',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/web-development/maintenance-security',
							internal: false,
						},
						{
							title: 'Graphics / Interactive',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/web-development/graphics-interactive',
							internal: false,
						},
						{
							title: 'Email / Chat',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/web-development/email-chat',
							internal: false,
						},
					],
				},
				{
					title: 'Programming / Software',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software',
							internal: false,
						},
						{
							title: 'JavaScript',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/javascript',
							internal: false,
						},
						{
							title: 'Java',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/java',
							internal: false,
						},
						{
							title: 'Microsoft',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/microsoft',
							internal: false,
						},
						{
							title: 'C#',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/c',
							internal: false,
						},
						{
							title: 'Unix / Linux',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/unix-linux',
							internal: false,
						},
						{
							title: 'SQL',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/sql',
							internal: false,
						},
						{
							title: 'Apple',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/apple',
							internal: false,
						},
						{
							title: 'Data Extraction / ETL',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/data-extraction-etl',
							internal: false,
						},
						{
							title: 'C++',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/c',
							internal: false,
						},
						{
							title: 'Ruby / Ruby on Rails',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/ruby-ruby-on-rails',
							internal: false,
						},
						{
							title: '.NET development',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/net-development',
							internal: false,
						},
						{
							title: 'Laravel',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/laravel',
							internal: false,
						},
						{
							title: 'API',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/api',
							internal: false,
						},
						{
							title: 'XML',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/xml',
							internal: false,
						},
						{
							title: 'Web Scraping',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/web-scraping',
							internal: false,
						},
						{
							title: 'Objective-C',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/objective-c',
							internal: false,
						},
						{
							title: 'Open Source',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/open-source',
							internal: false,
						},
						{
							title: 'Python',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/python',
							internal: false,
						},
						{
							title: 'Object-oriented',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/object-oriented',
							internal: false,
						},
						{
							title: 'GIS / Geolocation / Maps',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/gis-geolocation-maps',
							internal: false,
						},
						{
							title: 'Graphics / Virtual Reality / AI',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/graphics-virtual-reality-ai',
							internal: false,
						},
						{
							title: 'JSON',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/json',
							internal: false,
						},
						{
							title: 'Version Control',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/version-control',
							internal: false,
						},
						{
							title: 'Embedded Systems',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/embedded-systems',
							internal: false,
						},
						{
							title: 'Cloud Computing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/cloud-computing',
							internal: false,
						},
						{
							title: 'XHTML',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/xhtml',
							internal: false,
						},
						{
							title: 'Perl',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/perl',
							internal: false,
						},
						{
							title: 'Billing / Credit Cards / Finance',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/billing-credit-cards-finance',
							internal: false,
						},
						{
							title: 'General / Other Programming',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/programming-software/general-other-programming',
							internal: false,
						},
					],
				},
				{
					title: 'Apps / Mobile',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/apps-mobile',
							internal: false,
						},
						{
							title: 'Custom Apps',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/apps-mobile/custom-apps',
							internal: false,
						},
						{
							title: 'Convert Site to App',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/apps-mobile/convert-site-to-app',
							internal: false,
						},
						{
							title: 'iOS app development',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/apps-mobile/ios-app-development',
							internal: false,
						},
						{
							title: 'Android app development',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/apps-mobile/android-app-development',
							internal: false,
						},
						{
							title: 'Apps / Mobile Programming Languages',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/apps-mobile/apps-mobile-programming-languages',
							internal: false,
						},
						{
							title: 'Responsive / Hybrid',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/apps-mobile/responsive-hybrid',
							internal: false,
						},
						{
							title: 'Games',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/apps-mobile/games',
							internal: false,
						},
						{
							title: 'App / Mobile Marketing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/apps-mobile/app-mobile-marketing',
							internal: false,
						},
					],
				},
				{
					title: 'Database Design / Administration',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/database-design-administration-1',
							internal: false,
						},
						{
							title: 'MySQL',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/database-design-administration-1/mysql',
							internal: false,
						},
						{
							title: 'PostgreSQL',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/database-design-administration-1/postgresql',
							internal: false,
						},
						{
							title: 'SQL',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/database-design-administration-1/sql',
							internal: false,
						},
						{
							title: 'Oracle',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/database-design-administration-1/oracle',
							internal: false,
						},
						{
							title: 'Database Development',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/database-design-administration-1/database-development',
							internal: false,
						},
						{
							title: 'Database Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/database-design-administration-1/database-design',
							internal: false,
						},
						{
							title: 'Database Administration',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/database-design-administration-1/database-administration',
							internal: false,
						},
						{
							title: 'Query Optimization',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/database-design-administration-1/query-optimization',
							internal: false,
						},
						{
							title: 'Microsoft SQL Server',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/database-design-administration-1/microsoft-sql-server',
							internal: false,
						},
						{
							title: 'MongoDB',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/database-design-administration-1/mongodb',
							internal: false,
						},
						{
							title: 'IBM Db2',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/database-design-administration-1/ibm-db2',
							internal: false,
						},
						{
							title: 'Transact SQL',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/database-design-administration-1/transact-sql',
							internal: false,
						},
						{
							title: 'PL/SQL',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/database-design-administration-1/plsql',
							internal: false,
						},
						{
							title: 'Microsoft Access',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/database-design-administration-1/microsoft-access',
							internal: false,
						},
						{
							title: 'SAP Crystal Reports',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/database-design-administration-1/crystal-reports',
							internal: false,
						},
						{
							title: 'Data Warehouse',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/database-design-administration-1/data-warehouse',
							internal: false,
						},
						{
							title: 'Stored Procedures',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/database-design-administration-1/stored-procedures',
							internal: false,
						},
						{
							title: 'SQLite',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/database-design-administration-1/sqlite',
							internal: false,
						},
						{
							title: 'Sybase',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/database-design-administration-1/sybase',
							internal: false,
						},
						{
							title: 'FoxPro',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/database-design-administration-1/foxpro',
							internal: false,
						},
					],
				},
				{
					title: 'WordPress',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/wordpress-1',
							internal: false,
						},
						{
							title: 'Full Website Creation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/wordpress-1/full-website-creation',
							internal: false,
						},
						{
							title: 'Landing Page',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/wordpress-1/landing-page',
							internal: false,
						},
						{
							title: 'Theme Development',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/wordpress-1/theme-development',
							internal: false,
						},
						{
							title: 'Plugin Development',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/wordpress-1/plugin-development',
							internal: false,
						},
						{
							title: 'Backup / Cloning / Migration',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/wordpress-1/backup-cloning-migration',
							internal: false,
						},
						{
							title: 'Customization',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/wordpress-1/customization',
							internal: false,
						},
						{
							title: 'Bug Fixes',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/wordpress-1/bug-fixes',
							internal: false,
						},
					],
				},
				{
					title: 'Networking / System Admin',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/networking-system-admin-1',
							internal: false,
						},
						{
							title: 'Linux',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/networking-system-admin-1/linux',
							internal: false,
						},
						{
							title: 'Cloud Computing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/networking-system-admin-1/cloud-computing',
							internal: false,
						},
						{
							title: 'AWS',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/networking-system-admin-1/aws',
							internal: false,
						},
						{
							title: 'Microsoft',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/networking-system-admin-1/microsoft',
							internal: false,
						},
						{
							title: 'Cisco',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/networking-system-admin-1/cisco',
							internal: false,
						},
						{
							title: 'Virtualization',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/networking-system-admin-1/virtualization',
							internal: false,
						},
						{
							title: 'Switches / Routers',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/networking-system-admin-1/switches-routers',
							internal: false,
						},
						{
							title: 'Email / Chat / Video',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/networking-system-admin-1/email-chat-video',
							internal: false,
						},
						{
							title: 'Hardware',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/networking-system-admin-1/hardware',
							internal: false,
						},
						{
							title: 'Blockchain Technology',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/networking-system-admin-1/blockchain-technology',
							internal: false,
						},
						{
							title: 'Dell Boomi',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/networking-system-admin-1/dell-boomi',
							internal: false,
						},
						{
							title: 'General / Other Networking',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/networking-system-admin-1/general-other-networking',
							internal: false,
						},
						{
							title: 'Other Servers',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/networking-system-admin-1/other-servers',
							internal: false,
						},
					],
				},
				{
					title: 'QA / Testing',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/qa-testing-1',
							internal: false,
						},
						{
							title: 'Troubleshooting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/qa-testing-1/troubleshooting',
							internal: false,
						},
						{
							title: 'Quality Assurance (QA) and Testing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/qa-testing-1/quality-assurance-qa-and-testing',
							internal: false,
						},
						{
							title: 'Selenium',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/qa-testing-1/selenium',
							internal: false,
						},
						{
							title: 'Manual Testing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/qa-testing-1/manual-testing',
							internal: false,
						},
						{
							title: 'Bug Fixes',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/qa-testing-1/bug-fixes',
							internal: false,
						},
						{
							title: 'Regression Testing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/qa-testing-1/regression-testing',
							internal: false,
						},
						{
							title: 'Functional Testing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/qa-testing-1/functional-testing',
							internal: false,
						},
						{
							title: 'Usability Testing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/qa-testing-1/usability-testing',
							internal: false,
						},
						{
							title: 'Localization Testing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/qa-testing-1/localization-testing',
							internal: false,
						},
						{
							title: 'Performance Testing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/qa-testing-1/performance-testing',
							internal: false,
						},
						{
							title: 'Unit Testing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/qa-testing-1/unit-testing',
							internal: false,
						},
						{
							title: 'W3C Validation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/qa-testing-1/w3c-validation',
							internal: false,
						},
						{
							title: 'Test Case Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/qa-testing-1/test-case-design',
							internal: false,
						},
						{
							title: 'Test Driven Development',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/qa-testing-1/test-driven-development',
							internal: false,
						},
						{
							title: 'Validation Engineering',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/qa-testing-1/validation-engineering',
							internal: false,
						},
						{
							title: 'Performance Tuning',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/qa-testing-1/performance-tuning',
							internal: false,
						},
						{
							title: 'JUnit',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/qa-testing-1/junit',
							internal: false,
						},
						{
							title: 'Load Testing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/qa-testing-1/load-testing',
							internal: false,
						},
						{
							title: 'Automation Software Testing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/qa-testing-1/automation-software-testing',
							internal: false,
						},
						{
							title: 'SoapUI',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/qa-testing-1/soapui',
							internal: false,
						},
					],
				},
				{
					title: 'Information Security',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/information-security-1',
							internal: false,
						},
						{
							title: 'Firewalls',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/information-security-1/firewalls',
							internal: false,
						},
						{
							title: 'Virus Removal',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/information-security-1/virus-removal',
							internal: false,
						},
						{
							title: 'SSL',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/information-security-1/ssl',
							internal: false,
						},
						{
							title: 'Mobile Security',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/information-security-1/mobile-security',
							internal: false,
						},
						{
							title: 'Online Payments',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/information-security-1/online-payments',
							internal: false,
						},
						{
							title: 'Checkpoint',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/information-security-1/checkpoint',
							internal: false,
						},
						{
							title: 'Data Security',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/information-security-1/data-security',
							internal: false,
						},
						{
							title: 'Malware',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/information-security-1/malware',
							internal: false,
						},
						{
							title: 'iptables',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/information-security-1/iptables',
							internal: false,
						},
						{
							title: 'Encryption Software',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/information-security-1/encryption-software',
							internal: false,
						},
						{
							title: 'Cisco PIX',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/information-security-1/cisco-pix',
							internal: false,
						},
						{
							title: 'Nagios Core',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/information-security-1/nagios-core',
							internal: false,
						},
						{
							title: 'Cryptocurrency',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/information-security-1/cryptocurrency',
							internal: false,
						},
						{
							title: 'Penetration Testing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/information-security-1/penetration-testing',
							internal: false,
						},
						{
							title: 'Cisco ASA',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/information-security-1/cisco-asa',
							internal: false,
						},
						{
							title: 'Security Testing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/information-security-1/security-testing',
							internal: false,
						},
						{
							title: 'Ethical Hacking',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/information-security-1/ethical-hacking',
							internal: false,
						},
						{
							title: 'Sonicwall',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/information-security-1/sonicwall',
							internal: false,
						},
						{
							title: 'Sonicwall',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/information-security-1/sonicwall',
							internal: false,
						},
						{
							title: 'Antivirus / Antispam',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/information-security-1/antivirus-antispam',
							internal: false,
						},
					],
				},
				{
					title: 'Ideas / Help / Consultation',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/ideas-help-consultation-1',
							internal: false,
						},
						{
							title: 'Oracle Designer',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/ideas-help-consultation-1/oracle-designer',
							internal: false,
						},
						{
							title: 'Concept Development',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/ideas-help-consultation-1/concept-development',
							internal: false,
						},
						{
							title: 'Information Architecture',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/ideas-help-consultation-1/information-architecture',
							internal: false,
						},
						{
							title: 'Microsoft Visio',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/ideas-help-consultation-1/microsoft-visio',
							internal: false,
						},
						{
							title: 'Design Documents',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/ideas-help-consultation-1/design-documents',
							internal: false,
						},
						{
							title: 'Software Documentation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/ideas-help-consultation-1/software-documentation',
							internal: false,
						},
						{
							title: 'Functional Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/ideas-help-consultation-1/functional-design',
							internal: false,
						},
						{
							title: 'Road Mapping',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/ideas-help-consultation-1/road-mapping',
							internal: false,
						},
					],
				},
				{
					title: 'Technical Support',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/technical-support-1',
							internal: false,
						},
						{
							title: 'Computer Repair',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/technical-support-1/computer-repair',
							internal: false,
						},
						{
							title: 'Data loss / recovery',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/technical-support-1/data-loss-recovery',
							internal: false,
						},
						{
							title: 'Computer Technician',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/technical-support-1/computer-technician',
							internal: false,
						},
						{
							title: 'Remote Desktop Support',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/technical-support-1/desktop-support',
							internal: false,
						},
						{
							title: 'Email Support',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/technical-support-1/email-support',
							internal: false,
						},
						{
							title: 'Missing Documentation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/technical-support-1/missing-documentation',
							internal: false,
						},
						{
							title: 'Web Support',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/technical-support-1/web-support',
							internal: false,
						},
						{
							title: 'Chat Support',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/technical-support-1/chat-support',
							internal: false,
						},
						{
							title: 'Help Desk',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/technical-support-1/help-desk',
							internal: false,
						},
						{
							title: 'IT Service Management',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/technical-support-1/it-service-management',
							internal: false,
						},
					],
				},
				{
					title: 'Bug Fixing Services',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/bug-fixing-services-1',
							internal: false,
						},
						{
							title: 'Memory Leaks',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/bug-fixing-services-1/memory-leaks',
							internal: false,
						},
						{
							title: 'Scalability issues',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/bug-fixing-services-1/scalability-issues',
							internal: false,
						},
						{
							title: 'Logical / Functional Bugs:',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/bug-fixing-services-1/logical-functional-bugs',
							internal: false,
						},
						{
							title: 'Database Bugs',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/bug-fixing-services-1/database-bugs',
							internal: false,
						},
						{
							title: 'GUI Related Bugs',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/bug-fixing-services-1/gui-related-bugs',
							internal: false,
						},
						{
							title: 'System Related bugs',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/bug-fixing-services-1/system-related-bugs',
							internal: false,
						},
						{
							title: 'PHP errors / warnings',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/bug-fixing-services-1/php-errors-warnings',
							internal: false,
						},
						{
							title: 'Cross Browser Compatibility',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/bug-fixing-services-1/cross-browser-compatibility',
							internal: false,
						},
						{
							title: 'Version Regression',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/bug-fixing-services-1/version-regression',
							internal: false,
						},
						{
							title: 'Run Time Errors',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/bug-fixing-services-1/run-time-errors',
							internal: false,
						},
						{
							title: 'Missing commands',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/bug-fixing-services-1/missing-commands',
							internal: false,
						},
						{
							title: 'Mobile Software Bugs',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/bug-fixing-services-1/mobile-software-bugs',
							internal: false,
						},
						{
							title: 'Application Crash',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/bug-fixing-services-1/application-crash',
							internal: false,
						},
						{
							title: 'API Bugs',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/bug-fixing-services-1/api-bugs',
							internal: false,
						},
						{
							title: 'Compilation errors',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/bug-fixing-services-1/compilation-errors',
							internal: false,
						},
						{
							title: 'Control flow bugs',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/bug-fixing-services-1/control-flow-bugs',
							internal: false,
						},
						{
							title: 'Other Bugs / Errors',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/bug-fixing-services-1/other-bugs-errors',
							internal: false,
						},
					],
				},
				{
					title: 'ERP / CRM / SCM',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/erp-crm-scm-1',
							internal: false,
						},
						{
							title: 'Salesforce',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/erp-crm-scm-1/salesforce',
							internal: false,
						},
						{
							title: 'Microsoft Dynamics',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/erp-crm-scm-1/microsoft-dynamics',
							internal: false,
						},
						{
							title: 'Oracle Siebel CRM',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/erp-crm-scm-1/oracle-siebel-crm',
							internal: false,
						},
						{
							title: 'Oracle JD Edwards',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/erp-crm-scm-1/oracle-jd-edwards',
							internal: false,
						},
						{
							title: 'Oracle Hyperion',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/erp-crm-scm-1/oracle-hyperion',
							internal: false,
						},
						{
							title: 'SAP ABAP',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/erp-crm-scm-1/sap-abap',
							internal: false,
						},
						{
							title: 'Zoho CRM',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/erp-crm-scm-1/zoho-crm',
							internal: false,
						},
						{
							title: 'Zoho',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/erp-crm-scm-1/zoho',
							internal: false,
						},
						{
							title: 'Zoho Creator',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/erp-crm-scm-1/zoho-creator',
							internal: false,
						},
						{
							title: 'Zendesk',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/erp-crm-scm-1/zendesk',
							internal: false,
						},
						{
							title: 'Visualforce',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/erp-crm-scm-1/visualforce',
							internal: false,
						},
						{
							title: 'Odoo',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/erp-crm-scm-1/odoo',
							internal: false,
						},
						{
							title: 'Infusionsoft / Keap',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/erp-crm-scm-1/infusionsoft-keap',
							internal: false,
						},
						{
							title: 'Bitrix24',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/erp-crm-scm-1/bitrix24',
							internal: false,
						},
						{
							title: 'SugarCRM',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/erp-crm-scm-1/sugarcrm',
							internal: false,
						},
					],
				},
				{
					title: 'Management / Training',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/management-training-1',
							internal: false,
						},
						{
							title: 'Management / Leadership',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/management-training-1/management-leadership',
							internal: false,
						},
						{
							title: 'Product Development',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/management-training-1/product-development',
							internal: false,
						},
						{
							title: 'Project Management',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/management-training-1/project-management',
							internal: false,
						},
						{
							title: 'IT Service Management (ITSM)',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/management-training-1/it-service-management-itsm',
							internal: false,
						},
						{
							title: 'Agile Development',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/management-training-1/agile-development',
							internal: false,
						},
						{
							title: 'IT Training',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/management-training-1/it-training',
							internal: false,
						},
						{
							title: 'Apache Maven',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/management-training-1/apache-maven',
							internal: false,
						},
						{
							title: 'Software Configuration Management',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/management-training-1/software-configuration-management',
							internal: false,
						},
						{
							title: 'Business Process Management',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/management-training-1/business-process-management',
							internal: false,
						},
						{
							title: 'Software Lifecycle Management',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/management-training-1/software-lifecycle-management',
							internal: false,
						},
						{
							title: 'Test Driven Development',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/management-training-1/test-driven-development',
							internal: false,
						},
					],
				},
				{
					title: 'SAP',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/sap-1',
							internal: false,
						},
						{
							title: 'SAP HANA',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/sap-1/sap-hana',
							internal: false,
						},
						{
							title: 'SAP Basis',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/sap-1/sap-basis',
							internal: false,
						},
						{
							title: 'SAP SD',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/sap-1/sap-sd',
							internal: false,
						},
						{
							title: 'SAP BW',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/sap-1/sap-bw',
							internal: false,
						},
						{
							title: 'SAP Business Intelligence (BI)',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/sap-1/sap-bi',
							internal: false,
						},
						{
							title: 'SAP CRM',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/sap-1/sap-crm',
							internal: false,
						},
						{
							title: 'SAP MM',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/sap-1/sap-mm',
							internal: false,
						},
						{
							title: 'SAP NetWeaver',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/sap-1/sap-netweaver',
							internal: false,
						},
						{
							title: 'SAP ERP',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/sap-1/sap-erp',
							internal: false,
						},
						{
							title: 'SAP Business One',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/sap-1/sap-business-one',
							internal: false,
						},
						{
							title: 'SAP Fiori',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/sap-1/sap-fiori',
							internal: false,
						},
						{
							title: 'SAP FICO',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/sap-1/sap-fico',
							internal: false,
						},
						{
							title: 'SAP HR',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/sap-1/sap-hr',
							internal: false,
						},
						{
							title: 'SAP Scripting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/sap-1/sap-scripting',
							internal: false,
						},
						{
							title: 'SAP Hybris',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/sap-1/sap-hybris',
							internal: false,
						},
					],
				},
				{
					title: 'Telephony / Telecommunications',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/telephony-telecommunications-1',
							internal: false,
						},
						{
							title: 'VoIP',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/telephony-telecommunications-1/voip',
							internal: false,
						},
						{
							title: 'Telephony',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/telephony-telecommunications-1/telephony',
							internal: false,
						},
						{
							title: 'Telecommunications Systems',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/telephony-telecommunications-1/telecommunications-systems',
							internal: false,
						},
						{
							title: 'Cisco VoIP',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/telephony-telecommunications-1/cisco-voip',
							internal: false,
						},
						{
							title: 'Cisco Unified Call Manager',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/telephony-telecommunications-1/cisco-unified-call-manager',
							internal: false,
						},
						{
							title: 'SMS gateway',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/telephony-telecommunications-1/sms-gateway',
							internal: false,
						},
						{
							title: 'Code-Division Multiple Access',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/telephony-telecommunications-1/code-division-multiple-access',
							internal: false,
						},
						{
							title: 'Interactive Voice Response',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/telephony-telecommunications-1/interactive-voice-response',
							internal: false,
						},
						{
							title: 'OpenSIPS',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/telephony-telecommunications-1/opensips',
							internal: false,
						},
						{
							title: 'VoiceXML',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/telephony-telecommunications-1/voicexml',
							internal: false,
						},
						{
							title: 'Structured Cabling',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/telephony-telecommunications-1/structured-cabling',
							internal: false,
						},
						{
							title: 'VICIdial',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/telephony-telecommunications-1/vicidial',
							internal: false,
						},
						{
							title: 'Qualcomm',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/telephony-telecommunications-1/qualcomm',
							internal: false,
						},
						{
							title: 'Asterisk',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/telephony-telecommunications-1/asterisk',
							internal: false,
						},
						{
							title: 'Avaya',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/telephony-telecommunications-1/avaya',
							internal: false,
						},
						{
							title: 'A2Billing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/telephony-telecommunications-1/a2billing',
							internal: false,
						},
						{
							title: 'FreeSWITCH',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/telephony-telecommunications-1/freeswitch',
							internal: false,
						},
						{
							title: 'Elastix',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/telephony-telecommunications-1/elastix',
							internal: false,
						},
					],
				},
				{
					title: 'Math / Science / Algorithms',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/math-science-algorithms-1',
							internal: false,
						},
						{
							title: 'SCADA',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/math-science-algorithms-1/scada',
							internal: false,
						},
						{
							title: 'Digital Signal Processing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/math-science-algorithms-1/digital-signal-processing',
							internal: false,
						},
						{
							title: 'Mathcad',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/math-science-algorithms-1/mathcad',
							internal: false,
						},
						{
							title: 'Machine Learning',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/math-science-algorithms-1/machine-learning',
							internal: false,
						},
						{
							title: 'Simulation Modeling',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/math-science-algorithms-1/simulation-modeling',
							internal: false,
						},
						{
							title: 'Business Intelligence / Analysis',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/math-science-algorithms-1/business-intelligence-analysis',
							internal: false,
						},
						{
							title: 'Finite Element Analysis',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/math-science-algorithms-1/finite-element-analysis',
							internal: false,
						},
						{
							title: 'Math / Statistics',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/math-science-algorithms-1/math-statistics',
							internal: false,
						},
						{
							title: 'MATLAB',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/math-science-algorithms-1/matlab',
							internal: false,
						},
						{
							title: 'Statistical Analysis',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/math-science-algorithms-1/statistical-analysis',
							internal: false,
						},
						{
							title: 'Data Visualization',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/math-science-algorithms-1/data-visualization',
							internal: false,
						},
						{
							title: 'Regression Testing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/math-science-algorithms-1/regression-testing',
							internal: false,
						},
						{
							title: 'Web Analytics',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/math-science-algorithms-1/web-analytics',
							internal: false,
						},
						{
							title: 'General Analytics',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/math-science-algorithms-1/analytics',
							internal: false,
						},
						{
							title: 'Google Analytics',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/math-science-algorithms-1/google-analytics',
							internal: false,
						},
						{
							title: 'Power BI',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/math-science-algorithms-1/power-bi',
							internal: false,
						},
						{
							title: 'Fortran',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/math-science-algorithms-1/fortran',
							internal: false,
						},
						{
							title: 'Tableau',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/math-science-algorithms-1/tableau',
							internal: false,
						},
						{
							title: 'R Programming',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/math-science-algorithms-1/r-programming',
							internal: false,
						},
						{
							title: 'Internet of Things (IOT)',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/math-science-algorithms-1/internet-of-things-iot',
							internal: false,
						},
						{
							title: 'SPSS',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/math-science-algorithms-1/spss',
							internal: false,
						},
						{
							title: 'SAS',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/math-science-algorithms-1/sas',
							internal: false,
						},
					],
				},
				{
					title: 'Game Development',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/game-development-1',
							internal: false,
						},
						{
							title: 'Gaming',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/game-development-1/gaming',
							internal: false,
						},
						{
							title: 'Game Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/game-development-1/game-design',
							internal: false,
						},
						{
							title: '2D Games',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/game-development-1/2d-games',
							internal: false,
						},
						{
							title: '3D Games',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/game-development-1/3d-games',
							internal: false,
						},
						{
							title: 'Game Testing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/game-development-1/game-testing',
							internal: false,
						},
						{
							title: 'Cocos2D',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/game-development-1/cocos2d',
							internal: false,
						},
						{
							title: 'Mobile Game Development',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/game-development-1/mobile-game-development',
							internal: false,
						},
						{
							title: 'Level Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/game-development-1/level-design',
							internal: false,
						},
						{
							title: 'Microsoft DirectX',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/game-development-1/microsoft-directx',
							internal: false,
						},
						{
							title: 'Android Game Development',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/game-development-1/android-game-development',
							internal: false,
						},
						{
							title: 'Augmented Reality (AR)',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/game-development-1/augmented-reality-ar',
							internal: false,
						},
						{
							title: 'Xbox Game Development',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/game-development-1/xbox-game-development',
							internal: false,
						},
						{
							title: 'Social Games',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/game-development-1/social-games',
							internal: false,
						},
						{
							title: 'Box2D',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/game-development-1/box2d',
							internal: false,
						},
						{
							title: 'Multiplayer',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/game-development-1/multiplayer',
							internal: false,
						},
						{
							title: 'Video Games',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/game-development-1/video-games',
							internal: false,
						},
						{
							title: 'Unreal Engine',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/game-development-1/unreal-engine',
							internal: false,
						},
						{
							title: 'Corona SDK',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/game-development-1/corona-sdk',
							internal: false,
						},
						{
							title: 'Microsoft Kinect',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/game-development-1/microsoft-kinect',
							internal: false,
						},
						{
							title: 'Virtual Reality (VR)',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/web-mobile-tech-freelancers/game-development-1/virtual-reality-vr',
							internal: false,
						},
					],
				},
			],
		},
		{
			title: 'Design / Art / Video / Voice',
			navs: [
				{
					title: 'View All',
					strong: true,
					link:
						'https://www.hostjane.com/marketplace/categories/design-art-video-voice',
					internal: false,
				},
				{
					title: 'Web Design / Apps',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/web-design-app-design',
							internal: false,
						},
						{
							title: 'Web Page Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/web-design-app-design/web-page-design',
							internal: false,
						},
						{
							title: 'Website Redesign',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/web-design-app-design/website-redesign',
							internal: false,
						},
						{
							title: 'WordPress Theme Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/web-design-app-design/wordpress-theme-design',
							internal: false,
						},
						{
							title: 'iOS App Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/web-design-app-design/ios-app-design',
							internal: false,
						},
						{
							title: 'Android App Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/web-design-app-design/android-app-design',
							internal: false,
						},
						{
							title: 'Custom App Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/web-design-app-design/custom-app-design',
							internal: false,
						},
						{
							title: 'Landing Page Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/web-design-app-design/landing-page-design',
							internal: false,
						},
						{
							title: 'Blog Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/web-design-app-design/blog-design',
							internal: false,
						},
						{
							title: 'Social Media Page Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/web-design-app-design/social-media-page-design',
							internal: false,
						},
						{
							title: 'Icons / Buttons Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/web-design-app-design/icons-buttons-design',
							internal: false,
						},
						{
							title: 'Website Icon Set Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/web-design-app-design/website-icon-set-design',
							internal: false,
						},
						{
							title: 'App Icon Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/web-design-app-design/app-icon-design',
							internal: false,
						},
						{
							title: 'Form Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/web-design-app-design/form-design',
							internal: false,
						},
						{
							title: 'Twitter Header Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/web-design-app-design/twitter-header-design',
							internal: false,
						},
						{
							title: 'YouTube Channel Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/web-design-app-design/youtube-channel-design',
							internal: false,
						},
						{
							title: 'Facebook Cover Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/web-design-app-design/facebook-cover-design',
							internal: false,
						},
						{
							title: 'Banner Ad Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/web-design-app-design/banner-ad-design',
							internal: false,
						},
						{
							title: 'Other Website / App design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/web-design-app-design/other-website-app-design',
							internal: false,
						},
					],
				},
				{
					title: 'Graphic Design / Logos',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/graphic-design-logos',
							internal: false,
						},
						{
							title: 'Logo Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/graphic-design-logos/logo-design',
							internal: false,
						},
						{
							title: 'Business Identity / Branding',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/graphic-design-logos/logos-corporate-id-branding',
							internal: false,
						},
						{
							title: 'Adobe Software',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/graphic-design-logos/adobe-software',
							internal: false,
						},
						{
							title: 'Poster Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/graphic-design-logos/poster-design',
							internal: false,
						},
						{
							title: 'Layout Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/graphic-design-logos/layout-design',
							internal: false,
						},
						{
							title: '2D Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/graphic-design-logos/2d-design',
							internal: false,
						},
						{
							title: '3D Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/graphic-design-logos/3d-design',
							internal: false,
						},
						{
							title: '3D Modeling',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/graphic-design-logos/3d-modeling',
							internal: false,
						},
						{
							title: 'Industrial',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/graphic-design-logos/industrial',
							internal: false,
						},
						{
							title: 'Backgrounds',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/graphic-design-logos/backgrounds',
							internal: false,
						},
						{
							title: 'Templates / Wireframes / Mockups',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/graphic-design-logos/templates-wireframes-mockups',
							internal: false,
						},
						{
							title: 'Characters',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/graphic-design-logos/characters',
							internal: false,
						},
						{
							title: 'Banner Ads',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/graphic-design-logos/banner-ads',
							internal: false,
						},
						{
							title: 'Brochure Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/graphic-design-logos/brochure-design',
							internal: false,
						},
						{
							title: 'Font Designing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/graphic-design-logos/font-designing',
							internal: false,
						},
						{
							title: 'Colors / Typography / Textures',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/graphic-design-logos/colors-typography-textures',
							internal: false,
						},
						{
							title: 'Presentations',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/graphic-design-logos/presentations',
							internal: false,
						},
						{
							title: 'Digital',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/graphic-design-logos/digital',
							internal: false,
						},
						{
							title: 'Web / Email / Apps',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/graphic-design-logos/web-email-apps',
							internal: false,
						},
						{
							title: 'Cover Art / Page Art / Layouts',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/graphic-design-logos/cover-art-page-art-layouts',
							internal: false,
						},
						{
							title: 'Advertisements',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/graphic-design-logos/advertisements',
							internal: false,
						},
						{
							title: 'Other Graphic Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/graphic-design-logos/other-graphic-design',
							internal: false,
						},
					],
				},
				{
					title: 'Freelance Artists / Other Art',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/freelance-artist-other-art',
							internal: false,
						},
						{
							title: 'Freelance Artists',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/freelance-artist-other-art/freelance-artists',
							internal: false,
						},
						{
							title: 'Creative Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/freelance-artist-other-art/creative-design',
							internal: false,
						},
						{
							title: 'Traditional Media',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/freelance-artist-other-art/traditional-media',
							internal: false,
						},
						{
							title: 'Minimalist Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/freelance-artist-other-art/minimalist-design',
							internal: false,
						},
						{
							title: 'Fine Art',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/freelance-artist-other-art/fine-art',
							internal: false,
						},
						{
							title: 'Abstract Art',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/freelance-artist-other-art/abstract-art',
							internal: false,
						},
						{
							title: 'Portraits',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/freelance-artist-other-art/portraits',
							internal: false,
						},
						{
							title: 'Sequential Art',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/freelance-artist-other-art/sequential-art',
							internal: false,
						},
						{
							title: 'Paint Tool SAI',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/freelance-artist-other-art/paint-tool-sai',
							internal: false,
						},
						{
							title: 'Mascot Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/freelance-artist-other-art/mascot-design',
							internal: false,
						},
						{
							title: 'Contemporary Art',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/freelance-artist-other-art/contemporary-art',
							internal: false,
						},
					],
				},
				{
					title: 'Video / Animation',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/video-animation',
							internal: false,
						},
						{
							title: 'Adobe After Effects',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/video-animation/after-effects-customization',
							internal: false,
						},
						{
							title: 'Video Editing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/video-animation/video-editing',
							internal: false,
						},
						{
							title: '2D Animation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/video-animation/2d-animation',
							internal: false,
						},
						{
							title: '3D Animation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/video-animation/3d-animation',
							internal: false,
						},
						{
							title: 'Cinema 4D',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/video-animation/cinema-4d',
							internal: false,
						},
						{
							title: 'Storyboarding',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/video-animation/storyboarding',
							internal: false,
						},
						{
							title: 'Whiteboard Animation / Explainer Videos',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/video-animation/whiteboard-animation-explainer-videos',
							internal: false,
						},
						{
							title: 'Anime',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/video-animation/anime',
							internal: false,
						},
						{
							title: 'Adobe Flash',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/video-animation/adobe-flash',
							internal: false,
						},
						{
							title: 'Animated Video',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/video-animation/animated-video',
							internal: false,
						},
						{
							title: 'Logo Animation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/video-animation/logo-animation',
							internal: false,
						},
						{
							title: 'Motion Graphics',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/video-animation/motion-graphics',
							internal: false,
						},
						{
							title: 'Stop motion',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/video-animation/stop-motion',
							internal: false,
						},
						{
							title: 'Flash Presentations',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/video-animation/flash-presentations',
							internal: false,
						},
						{
							title: 'Claymation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/video-animation/claymation',
							internal: false,
						},
						{
							title: 'Visual Effects (VFX)',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/video-animation/visual-effects-vfx',
							internal: false,
						},
						{
							title: 'Special Effects (FX)',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/video-animation/special-effects-fx',
							internal: false,
						},
						{
							title: 'GIF Animation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/video-animation/gif-animation',
							internal: false,
						},
						{
							title: '3D Character Animation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/video-animation/3d-character-animation',
							internal: false,
						},
						{
							title: 'Character Animation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/video-animation/character-animation',
							internal: false,
						},
						{
							title: 'Creature Animation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/video-animation/creature-animation',
							internal: false,
						},
						{
							title: 'Camera Animation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/video-animation/camera-animation',
							internal: false,
						},
					],
				},
				{
					title: 'Illustration',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration',
							internal: false,
						},
						{
							title: 'Drawing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/drawing',
							internal: false,
						},
						{
							title: 'Sketching',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/sketching',
							internal: false,
						},
						{
							title: 'Vector Art',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/vector-art',
							internal: false,
						},
						{
							title: '3D Art / Illustrations',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/3d-art-illustrations',
							internal: false,
						},
						{
							title: 'Pattern Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/pattern-design',
							internal: false,
						},
						{
							title: 'Wedding Invitation Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/wedding-invitation-design',
							internal: false,
						},
						{
							title: 'Invitation Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/invitation-design',
							internal: false,
						},
						{
							title: 'Tattoo Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/tattoo-design',
							internal: false,
						},
						{
							title: 'Book Illustration',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/book-illustration',
							internal: false,
						},
						{
							title: 'Specialized Mediums',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/specialized-mediums',
							internal: false,
						},
						{
							title: 'Realistic Illustration',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/realistic-illustration',
							internal: false,
						},
						{
							title: 'Cover Art',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/cover-art',
							internal: false,
						},
						{
							title: '',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/childrens-book-illustration',
							internal: false,
						},
						{
							title: 'Medical Illustration',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/medical-illustration',
							internal: false,
						},
						{
							title: 'Pen & Ink',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/pen-ink',
							internal: false,
						},
						{
							title: 'Character Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/character-design',
							internal: false,
						},
						{
							title: 'Objects / Industries / Styles',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/objects-industries-styles',
							internal: false,
						},
						{
							title: 'Architectural Illustration',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/architectural-illustration',
							internal: false,
						},
						{
							title: 'Product Illustration',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/product-illustration',
							internal: false,
						},
						{
							title: 'Portrait Illustration',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/portrait-illustration',
							internal: false,
						},
						{
							title: 'Pencil Drawings',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/pencil-drawings',
							internal: false,
						},
						{
							title: 'Line Art',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/line-art',
							internal: false,
						},
						{
							title: 'Animals & Nature',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/animals-nature',
							internal: false,
						},
						{
							title: 'Sports',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/sports',
							internal: false,
						},
						{
							title: 'Clipping Path',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/clipping-path',
							internal: false,
						},
						{
							title: 'Digital Illustration',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/digital-illustration',
							internal: false,
						},
						{
							title: 'Ink Illustration',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/illustration/ink-illustration',
							internal: false,
						},
					],
				},
				{
					title: 'Voice Over / Acting',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/voice-over-acting',
							internal: false,
						},
						{
							title: 'Music Business Consulting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/voice-over-acting/music-business-consulting',
							internal: false,
						},
						{
							title: 'Video Narration',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/voice-over-acting/video-narration',
							internal: false,
						},
						{
							title: 'Radio',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/voice-over-acting/radio',
							internal: false,
						},
						{
							title: 'Phone System / IVR',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/voice-over-acting/phone-system-ivr',
							internal: false,
						},
						{
							title: 'Television',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/voice-over-acting/television',
							internal: false,
						},
						{
							title: 'Audiobook',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/voice-over-acting/audiobook',
							internal: false,
						},
						{
							title: 'Educational',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/voice-over-acting/educational',
							internal: false,
						},
						{
							title: 'Movie Trailers',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/voice-over-acting/movie-trailers',
							internal: false,
						},
						{
							title: 'Podcasting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/voice-over-acting/podcasting',
							internal: false,
						},
					],
				},
				{
					title: 'Studio Musicians / Session Singers',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/studio-musicians-session-singers',
							internal: false,
						},
						{
							title: 'Singer-Songwriters',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/studio-musicians-session-singers/singer-songwriters',
							internal: false,
						},
						{
							title: 'Pop',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/studio-musicians-session-singers/pop',
							internal: false,
						},
						{
							title: 'Hip Hop',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/studio-musicians-session-singers/hip-hop',
							internal: false,
						},
						{
							title: 'R&B',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/studio-musicians-session-singers/rb',
							internal: false,
						},
						{
							title: 'Rock',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/studio-musicians-session-singers/rock',
							internal: false,
						},
						{
							title: 'Opera / Classical',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/studio-musicians-session-singers/opera-classical',
							internal: false,
						},
						{
							title: 'Disc Jockeys',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/studio-musicians-session-singers/disc-jockeys',
							internal: false,
						},
						{
							title: 'Folk',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/studio-musicians-session-singers/folk',
							internal: false,
						},
						{
							title: 'Blues',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/studio-musicians-session-singers/blues',
							internal: false,
						},
						{
							title: 'Jazz',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/studio-musicians-session-singers/jazz',
							internal: false,
						},
						{
							title: 'Punk',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/studio-musicians-session-singers/punk',
							internal: false,
						},
						{
							title: 'Country',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/studio-musicians-session-singers/country',
							internal: false,
						},
						{
							title: 'Heavy',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/studio-musicians-session-singers/heavy',
							internal: false,
						},
						{
							title: 'Reggae',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/studio-musicians-session-singers/reggae',
							internal: false,
						},
						{
							title: 'Electronic',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/studio-musicians-session-singers/electronic',
							internal: false,
						},
					],
				},
				{
					title: 'Business / Advertising Design',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/business-advertising-design',
							internal: false,
						},
						{
							title: 'Business Card Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/business-advertising-design/business-card-design',
							internal: false,
						},
						{
							title: 'Postcard / Flyer Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/business-advertising-design/postcard-flyer-design',
							internal: false,
						},
						{
							title: 'Direct Mail Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/business-advertising-design/direct-mail-design',
							internal: false,
						},
						{
							title: 'Poster Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/business-advertising-design/poster-design',
							internal: false,
						},
						{
							title: 'Email Template Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/business-advertising-design/email-template-design',
							internal: false,
						},
						{
							title: 'Email Newsletter Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/business-advertising-design/email-newsletter-design',
							internal: false,
						},
						{
							title: 'Menu Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/business-advertising-design/menu-design',
							internal: false,
						},
						{
							title: 'Album Cover Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/business-advertising-design/album-cover-design',
							internal: false,
						},
						{
							title: 'Website Header Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/business-advertising-design/website-header-design',
							internal: false,
						},
						{
							title: 'Resume Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/business-advertising-design/resume-design',
							internal: false,
						},
						{
							title: 'Brochure Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/business-advertising-design/brochure-design',
							internal: false,
						},
						{
							title: 'Booklet Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/business-advertising-design/booklet-design',
							internal: false,
						},
						{
							title: 'Word Template Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/business-advertising-design/word-template-design',
							internal: false,
						},
						{
							title: 'PowerPoint Template Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/business-advertising-design/powerpoint-template-design',
							internal: false,
						},
						{
							title: 'Graphs / Infographics / Reports',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/business-advertising-design/graphs-infographics-reports',
							internal: false,
						},
						{
							title: 'Pamphlet Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/business-advertising-design/pamphlet-design',
							internal: false,
						},
						{
							title: 'Car / Truck / Van Wrap Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/business-advertising-design/car-truck-van-wrap-design',
							internal: false,
						},
						{
							title: 'Leaflet Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/business-advertising-design/leaflet-design',
							internal: false,
						},
						{
							title: 'Billboard Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/business-advertising-design/billboard-design',
							internal: false,
						},
						{
							title: 'Banner Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/business-advertising-design/banner-design',
							internal: false,
						},
						{
							title: 'Trade Show Banner Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/business-advertising-design/trade-show-banner-design',
							internal: false,
						},
						{
							title: 'Trade Show Booth Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/business-advertising-design/trade-show-booth-design',
							internal: false,
						},
						{
							title: 'Other Business / Advertising Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/business-advertising-design/other-business-advertising-design',
							internal: false,
						},
					],
				},
				{
					title: 'Book / Magazine Design',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/book-magazine-design',
							internal: false,
						},
						{
							title: 'Book Cover Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/book-magazine-design/book-cover-design',
							internal: false,
						},
						{
							title: 'Interior Book Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/book-magazine-design/interior-book-design',
							internal: false,
						},
						{
							title: 'eBook Cover Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/book-magazine-design/ebook-cover-design',
							internal: false,
						},
						{
							title: 'Magazine Cover Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/book-magazine-design/magazine-cover-design',
							internal: false,
						},
						{
							title: 'Book Layout Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/book-magazine-design/book-layout-design',
							internal: false,
						},
						{
							title: 'Other Book / Magazine Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/book-magazine-design/other-book-magazine-design',
							internal: false,
						},
					],
				},
				{
					title: 'Audio / Sound / Music',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/audio-sound-music',
							internal: false,
						},
						{
							title: 'Music Production',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/audio-sound-music/music-production',
							internal: false,
						},
						{
							title: 'Music Videos',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/audio-sound-music/music-videos',
							internal: false,
						},
						{
							title: 'Music Composition',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/audio-sound-music/music-composition',
							internal: false,
						},
						{
							title: 'Vocal Tuning',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/audio-sound-music/vocal-tuning',
							internal: false,
						},
						{
							title: 'Podcast & Spoken-Word Editing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/audio-sound-music/podcast-spoken-word-editing',
							internal: false,
						},
						{
							title: 'Audio Editing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/audio-sound-music/audio-editing',
							internal: false,
						},
						{
							title: 'Audio Post Production',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/audio-sound-music/audio-post-production',
							internal: false,
						},
						{
							title: 'Adobe Audition',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/audio-sound-music/adobe-audition',
							internal: false,
						},
						{
							title: 'Radio',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/audio-sound-music/radio',
							internal: false,
						},
						{
							title: 'Sound Effects',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/audio-sound-music/vocal-tuning',
							internal: false,
						},
						{
							title: 'Sound Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/audio-sound-music/sound-design',
							internal: false,
						},
						{
							title: 'Sound Engineering',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/audio-sound-music/sound-engineering',
							internal: false,
						},
						{
							title: 'Sound Production',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/audio-sound-music/sound-production',
							internal: false,
						},
						{
							title: 'Audio Mastering',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/audio-sound-music/audio-mastering',
							internal: false,
						},
						{
							title: 'Audio Mixing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/audio-sound-music/audio-mixing',
							internal: false,
						},
						{
							title: 'Jingles & Drops',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/audio-sound-music/jingles-drops',
							internal: false,
						},
						{
							title: 'Audio Recording',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/audio-sound-music/audio-recording',
							internal: false,
						},
						{
							title: 'CD Production',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/audio-sound-music/cd-production',
							internal: false,
						},
						{
							title: 'Sony Acid Pro',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/audio-sound-music/sony-acid-pro',
							internal: false,
						},
						{
							title: 'Sound Forge',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/audio-sound-music/sound-forge',
							internal: false,
						},
						{
							title: 'Broadcasting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/audio-sound-music/broadcasting',
							internal: false,
						},
						{
							title: 'Music Business Consulting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/audio-sound-music/music-business-consulting',
							internal: false,
						},
					],
				},
				{
					title: 'T-Shirts / Merchandise Design',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/t-shirts-merchandise-design',
							internal: false,
						},
						{
							title: 'Custom T-Shirt Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/t-shirts-merchandise-design/custom-t-shirt-design',
							internal: false,
						},
						{
							title: 'Clothing / Apparel Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/t-shirts-merchandise-design/clothing-apparel-design',
							internal: false,
						},
						{
							title: 'Jersey Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/t-shirts-merchandise-design/jersey-design',
							internal: false,
						},
						{
							title: 'Sweatshirt / Hoodie Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/t-shirts-merchandise-design/sweatshirt-hoodie-design',
							internal: false,
						},
						{
							title: 'Merchandise Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/t-shirts-merchandise-design/merchandise-design',
							internal: false,
						},
						{
							title: 'Bag / Tote Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/t-shirts-merchandise-design/bag-tote-design',
							internal: false,
						},
						{
							title: 'Hat / Cap Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/t-shirts-merchandise-design/hat-cap-design',
							internal: false,
						},
						{
							title: 'Cup / Mug Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/t-shirts-merchandise-design/cup-mug-design',
							internal: false,
						},
						{
							title: 'Sticker Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/t-shirts-merchandise-design/sticker-design',
							internal: false,
						},
						{
							title: 'Other Clothing / Merchandise Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/t-shirts-merchandise-design/other-clothing-merchandise-design',
							internal: false,
						},
					],
				},
				{
					title: 'Packaging / Label Design',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/packaging-label-design',
							internal: false,
						},
						{
							title: 'Product Packaging Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/packaging-label-design/product-packaging-design',
							internal: false,
						},
						{
							title: 'Beer Label Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/packaging-label-design/beer-label-design',
							internal: false,
						},
						{
							title: 'Wine Label Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/packaging-label-design/wine-label-design',
							internal: false,
						},
						{
							title: 'Cosmetics Packaging Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/packaging-label-design/cosmetics-packaging-design',
							internal: false,
						},
						{
							title: 'Food Label Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/packaging-label-design/food-label-design',
							internal: false,
						},
						{
							title: 'Product Label Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/packaging-label-design/product-label-design',
							internal: false,
						},
						{
							title: 'Other Packaging / Label Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/packaging-label-design/other-packaging-label-design',
							internal: false,
						},
						{
							title: 'Retail Packaging Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/packaging-label-design/retail-packaging-design',
							internal: false,
						},
						{
							title: 'Food Packaging Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/packaging-label-design/food-packaging-design',
							internal: false,
						},
						{
							title: 'Beverage Label Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/packaging-label-design/beverage-label-design',
							internal: false,
						},
					],
				},
				{
					title: 'CAD / Technical Drawings',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings',
							internal: false,
						},
						{
							title: 'CAD / Drafting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/cad-drafting',
							internal: false,
						},
						{
							title: '2D Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/2d-design',
							internal: false,
						},
						{
							title: '3D Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/3d-design',
							internal: false,
						},
						{
							title: '3D Rendering',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/3d-rendering',
							internal: false,
						},
						{
							title: '3D Modeling',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/3d-modeling',
							internal: false,
						},
						{
							title: 'Interior Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/interior-design',
							internal: false,
						},
						{
							title: 'Product Development',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/product-development',
							internal: false,
						},
						{
							title: 'Prototyping',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/prototyping',
							internal: false,
						},
						{
							title: 'Schematic Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/schematic-design',
							internal: false,
						},
						{
							title: 'Autodesk 3ds Max',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/autodesk-3ds-max',
							internal: false,
						},
						{
							title: 'Autodesk Revit',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/autodesk-revit',
							internal: false,
						},
						{
							title: 'Construction Drawings',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/construction-drawings',
							internal: false,
						},
						{
							title: 'CATIA',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/catia',
							internal: false,
						},
						{
							title: 'Mold Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/mold-design',
							internal: false,
						},
						{
							title: 'Augmented Reality (AR)',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/augmented-reality-ar',
							internal: false,
						},
						{
							title: 'Google SketchUp',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/google-sketchup',
							internal: false,
						},
						{
							title: 'Autodesk Inventor',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/autodesk-inventor',
							internal: false,
						},
						{
							title: 'Machine Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/machine-design',
							internal: false,
						},
						{
							title: 'ArchiCAD',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/archicad',
							internal: false,
						},
						{
							title: 'Sheet Metal Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/sheet-metal-design',
							internal: false,
						},
						{
							title: 'SolidWorks',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/solidworks',
							internal: false,
						},
						{
							title: 'Virtual Staging',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/virtual-staging',
							internal: false,
						},
						{
							title: 'Sustainable Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/sustainable-design',
							internal: false,
						},
						{
							title: 'Space Planning',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/space-planning',
							internal: false,
						},
						{
							title: 'Landscape Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/landscape-design',
							internal: false,
						},
						{
							title: 'Furniture Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/furniture-design',
							internal: false,
						},
						{
							title: 'Lighting Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/lighting-design',
							internal: false,
						},
						{
							title: 'Set Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cad-technical-drawings/set-design',
							internal: false,
						},
					],
				},
				{
					title: 'Image Restoration / Editing',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/image-restoration-editing',
							internal: false,
						},
						{
							title: 'Photo Retouching',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/image-restoration-editing/photo-retouching',
							internal: false,
						},
						{
							title: 'Skin Retouching Services',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/image-restoration-editing/skin-retouching-services',
							internal: false,
						},
						{
							title: 'Adobe Lightroom',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/image-restoration-editing/adobe-lightroom',
							internal: false,
						},
						{
							title: 'Beauty Retouching',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/image-restoration-editing/beauty-retouching',
							internal: false,
						},
						{
							title: 'Image Resizing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/image-restoration-editing/image-resizing',
							internal: false,
						},
						{
							title: 'Image Masking',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/image-restoration-editing/image-masking',
							internal: false,
						},
						{
							title: 'Photo Illustration',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/image-restoration-editing/photo-illustration',
							internal: false,
						},
						{
							title: 'Photoscape',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/image-restoration-editing/photoscape',
							internal: false,
						},
						{
							title: 'Photo / Image Cropping',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/image-restoration-editing/photo-image-cropping',
							internal: false,
						},
						{
							title: 'Photo / Image Editing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/image-restoration-editing/photo-image-editing',
							internal: false,
						},
						{
							title: 'Chroma Key',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/image-restoration-editing/chroma-key',
							internal: false,
						},
						{
							title: 'Paint.NET',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/image-restoration-editing/paintnet',
							internal: false,
						},
						{
							title: 'Masking',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/image-restoration-editing/masking',
							internal: false,
						},
						{
							title: 'Noise Reduction',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/image-restoration-editing/noise-reduction',
							internal: false,
						},
						{
							title: 'Color Balancing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/image-restoration-editing/color-balancing',
							internal: false,
						},
						{
							title: 'Deep Etching',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/image-restoration-editing/deep-etching',
							internal: false,
						},
						{
							title: 'Object Removal',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/image-restoration-editing/object-removal',
							internal: false,
						},
						{
							title: 'Hair Masking',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/image-restoration-editing/hair-masking',
							internal: false,
						},
						{
							title: 'Ghost Mannequin',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/image-restoration-editing/ghost-mannequin',
							internal: false,
						},
						{
							title: 'Watermarking',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/image-restoration-editing/watermarking',
							internal: false,
						},
					],
				},
				{
					title: 'Photography',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/photography',
							internal: false,
						},
						{
							title: 'Adobe Lightroom',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/photography/adobe-lightroom',
							internal: false,
						},
						{
							title: 'Compositing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/photography/compositing',
							internal: false,
						},
						{
							title: 'Product Photography',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/photography/product-photography',
							internal: false,
						},
						{
							title: 'Commercial Photography',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/photography/commercial-photography',
							internal: false,
						},
						{
							title: 'Outdoor Photography',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/photography/outdoor-photography',
							internal: false,
						},
						{
							title: 'Advertising Photography',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/photography/advertising-photography',
							internal: false,
						},
						{
							title: 'Photorealism',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/photography/photorealism',
							internal: false,
						},
					],
				},
				{
					title: 'File Conversions',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/file-converting',
							internal: false,
						},
						{
							title: 'Convert to Kindle',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/file-converting/convert-to-kindle',
							internal: false,
						},
						{
							title: 'Convert to ePub',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/file-converting/convert-to-epub',
							internal: false,
						},
						{
							title: '',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/file-converting/convert-to-ebook',
							internal: false,
						},
						{
							title: 'PSD to HTML',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/file-converting/psd-to-html',
							internal: false,
						},
						{
							title: 'Convert to HTML',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/file-converting/convert-to-html',
							internal: false,
						},
						{
							title: 'Sketch to HTML',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/file-converting/sketch-to-html',
							internal: false,
						},
						{
							title: 'PSD to WordPress',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/file-converting/psd-to-wordpress',
							internal: false,
						},
						{
							title: 'PSD to Email Templates',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/file-converting/psd-to-email-templates',
							internal: false,
						},
						{
							title: 'Convert to PDF',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/file-converting/convert-to-pdf',
							internal: false,
						},
						{
							title: 'Convert to RTF',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/file-converting/convert-to-rtf',
							internal: false,
						},
						{
							title: 'Convert to DOC / DOCX',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/file-converting/convert-to-editable-file',
							internal: false,
						},
						{
							title: 'Lightning Source / Ingram Support',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/file-converting/lightning-source-ingram-support',
							internal: false,
						},
						{
							title: 'Image File Conversions',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/file-converting/image-file-conversions',
							internal: false,
						},
						{
							title: 'Video File Conversions',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/file-converting/video-conversions',
							internal: false,
						},
						{
							title: 'Audio File Conversions',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/file-converting/audio-file-conversions',
							internal: false,
						},
						{
							title: 'Other File Conversions',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/file-converting/other-file-conversions',
							internal: false,
						},
					],
				},
				{
					title: 'Concepts / Direction',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/concepts-direction',
							internal: false,
						},
						{
							title: 'Art Direction',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/concepts-direction/art-direction',
							internal: false,
						},
						{
							title: 'Concept Art',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/concepts-direction/concept-art',
							internal: false,
						},
						{
							title: 'Concept Development',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/concepts-direction/concept-development',
							internal: false,
						},
						{
							title: 'Design Consulting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/concepts-direction/design-consulting',
							internal: false,
						},
						{
							title: 'Ideation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/concepts-direction/ideation',
							internal: false,
						},
					],
				},
				{
					title: 'Painting',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/painting',
							internal: false,
						},
						{
							title: 'Digital Painting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/painting/digital-painting',
							internal: false,
						},
						{
							title: 'Oil Painting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/painting/oil-painting',
							internal: false,
						},
						{
							title: 'Pointillism',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/painting/pointillism',
							internal: false,
						},
						{
							title: 'Abstract Painting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/painting/abstract-painting',
							internal: false,
						},
						{
							title: 'Murals',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/painting/murals',
							internal: false,
						},
						{
							title: 'Portrait Painting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/painting/portrait-painting',
							internal: false,
						},
						{
							title: 'Acrylic Painting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/painting/acrylic-painting',
							internal: false,
						},
						{
							title: 'Watercolor Art',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/painting/watercolor-art',
							internal: false,
						},
						{
							title: 'Landscape Painting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/painting/landscape-painting',
							internal: false,
						},
						{
							title: 'Digital Printing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/painting/digital-printing',
							internal: false,
						},
						{
							title: 'Matte Painting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/painting/matte-painting',
							internal: false,
						},
						{
							title: 'Still Life Painting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/painting/still-life-painting',
							internal: false,
						},
						{
							title: 'Airbrushing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/painting/airbrushing',
							internal: false,
						},
						{
							title: 'Pastels',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/painting/pastels',
							internal: false,
						},
						{
							title: 'Realism Painting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/painting/realism-painting',
							internal: false,
						},
					],
				},
				{
					title: 'Cartoons / Comic Art',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cartoons-comic-art',
							internal: false,
						},
						{
							title: 'Cartooning',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cartoons-comic-art/cartooning',
							internal: false,
						},
						{
							title: 'Caricatures',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cartoons-comic-art/caricatures',
							internal: false,
						},
						{
							title: 'Graphic Novel Artist',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cartoons-comic-art/graphic-novel-artist',
							internal: false,
						},
						{
							title: 'Manga',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cartoons-comic-art/manga',
							internal: false,
						},
						{
							title: 'Political Cartoon',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cartoons-comic-art/political-cartoon',
							internal: false,
						},
						{
							title: 'Comic Art',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/cartoons-comic-art/comic-art',
							internal: false,
						},
					],
				},
				{
					title: 'Fashion',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/fashion',
							internal: false,
						},
						{
							title: 'Embroidery Digitization',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/fashion/embroidery-digitization',
							internal: false,
						},
						{
							title: 'Fashion Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/fashion/fashion-design',
							internal: false,
						},
						{
							title: 'Pattern Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/fashion/pattern-design',
							internal: false,
						},
						{
							title: 'Fashion Illustration',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/fashion/fashion-illustration',
							internal: false,
						},
						{
							title: 'Jewelry Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/fashion/jewelry-design',
							internal: false,
						},
						{
							title: 'Footwear Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/fashion/footwear-design',
							internal: false,
						},
						{
							title: 'Textile Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/fashion/textile-design',
							internal: false,
						},
						{
							title: 'Costume Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/fashion/costume-design',
							internal: false,
						},
						{
							title: 'Bag Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/fashion/bag-design',
							internal: false,
						},
					],
				},
				{
					title: 'Printing / Production',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/printing-production',
							internal: false,
						},
						{
							title: 'Printing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/printing-production/printing',
							internal: false,
						},
						{
							title: 'Online Publishing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/printing-production/online-publishing',
							internal: false,
						},
						{
							title: 'Print Production',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/printing-production/print-production',
							internal: false,
						},
						{
							title: 'Post-Production',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/printing-production/post-production',
							internal: false,
						},
						{
							title: 'Prepress',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/printing-production/prepress',
							internal: false,
						},
						{
							title: 'PostScript / Desktop Publishing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/printing-production/postscript',
							internal: false,
						},
						{
							title: 'Microsoft Publisher',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/printing-production/microsoft-publisher',
							internal: false,
						},
						{
							title: 'Publication Production',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/printing-production/publication-production',
							internal: false,
						},
						{
							title: '3D Printing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/printing-production/3d-printing',
							internal: false,
						},
						{
							title: 'Canvas Prints',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/printing-production/canvas-prints',
							internal: false,
						},
						{
							title: 'Color Printing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/printing-production/color-printing',
							internal: false,
						},
						{
							title: 'Color Management',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/printing-production/color-management',
							internal: false,
						},
						{
							title: 'Commercial Printing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/printing-production/commercial-printing',
							internal: false,
						},
						{
							title: 'Large Format Printing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/design-art-video-voice/printing-production/large-format-printing',
							internal: false,
						},
					],
				},
			],
		},
		{
			title: 'Online Tutors',
			navs: [
				{
					title: 'View All',
					strong: true,
					link:
						'https://www.hostjane.com/marketplace/categories/online-tutors',
					internal: false,
				},
				{
					title: 'Language Tutors',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/language-tutors-1',
							internal: false,
						},
						{
							title: 'French Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/language-tutors-1/french-lessons',
							internal: false,
						},
						{
							title: 'English Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/language-tutors-1/english-lessons',
							internal: false,
						},
						{
							title: 'Spanish Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/language-tutors-1/spanish-lessons',
							internal: false,
						},
						{
							title: 'Hindi Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/language-tutors-1/hindi-lessons',
							internal: false,
						},
						{
							title: 'Portuguese Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/language-tutors-1/portuguese-lessons',
							internal: false,
						},
						{
							title: 'Polish Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/language-tutors-1/polish-lessons',
							internal: false,
						},
						{
							title: 'ASL Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/language-tutors-1/asl-lessons',
							internal: false,
						},
						{
							title: 'Chinese Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/language-tutors-1/chinese-lessons',
							internal: false,
						},
						{
							title: 'Japanese Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/language-tutors-1/japanese-lessons',
							internal: false,
						},
						{
							title: 'Italian Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/language-tutors-1/italian-lessons',
							internal: false,
						},
						{
							title: 'German Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/language-tutors-1/german-lessons',
							internal: false,
						},
						{
							title: 'Russian Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/language-tutors-1/russian-lessons',
							internal: false,
						},
						{
							title: 'Korean Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/language-tutors-1/korean-lessons',
							internal: false,
						},
						{
							title: 'Dutch Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/language-tutors-1/dutch-lessons',
							internal: false,
						},
						{
							title: 'Serbian Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/language-tutors-1/serbian-lessons',
							internal: false,
						},
						{
							title: 'Hebrew Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/language-tutors-1/hebrew-lessons',
							internal: false,
						},
						{
							title: 'Arabic Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/language-tutors-1/arabic-lessons',
							internal: false,
						},
						{
							title: 'Turkish Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/language-tutors-1/turkish-lessons',
							internal: false,
						},
						{
							title: 'Malay Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/language-tutors-1/malay-lessons',
							internal: false,
						},
						{
							title: 'Bengali Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/language-tutors-1/bengali-lessons',
							internal: false,
						},
					],
				},
				{
					title: 'Online Interpreting Services',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/online-interpreting-services',
							internal: false,
						},
						{
							title: 'Simultaneous Interpreting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/online-interpreting-services/simultaneous-interpreting',
							internal: false,
						},
						{
							title: 'Over-the-Phone Interpretation (OPI)',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/online-interpreting-services/over-the-phone-interpretation-opi',
							internal: false,
						},
						{
							title: 'Consecutive Interpreting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/online-interpreting-services/consecutive-interpreting',
							internal: false,
						},
						{
							title: 'On-Demand Phone Interpreting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/online-interpreting-services/on-demand-phone-interpreting',
							internal: false,
						},
						{
							title: 'Whisper Interpreting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/online-interpreting-services/whisper-interpreting',
							internal: false,
						},
						{
							title: 'Escort/Travel Interpreting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/online-interpreting-services/escorttravel-interpreting',
							internal: false,
						},
					],
				},
				{
					title: 'Music Lessons',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/music-lessons',
							internal: false,
						},
						{
							title: 'Music Software Training',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/music-lessons/music-software-training',
							internal: false,
						},
						{
							title: 'Harmonica Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/music-lessons/harmonica-lessons',
							internal: false,
						},
						{
							title: 'Music Production Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/music-lessons/music-production-lessons',
							internal: false,
						},
						{
							title: 'Keyboard Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/music-lessons/keyboard-lessons',
							internal: false,
						},
						{
							title: 'Piano Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/music-lessons/piano-lessons',
							internal: false,
						},
						{
							title: 'Electric Guitar Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/music-lessons/electric-guitar-lessons',
							internal: false,
						},
						{
							title: 'Classical Guitar Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/music-lessons/guitar-lessons',
							internal: false,
						},
						{
							title: 'Bass Guitar Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/music-lessons/bass-guitar-lessons',
							internal: false,
						},
						{
							title: 'Harp Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/music-lessons/harp-lessons',
							internal: false,
						},
						{
							title: 'Recorder Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/music-lessons/recorder-lessons',
							internal: false,
						},
						{
							title: 'Violin Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/music-lessons/violin-lessons',
							internal: false,
						},
						{
							title: 'Cello Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/music-lessons/cello-lessons',
							internal: false,
						},
						{
							title: 'Singing Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/music-lessons/singing-lessons',
							internal: false,
						},
						{
							title: 'Flute Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/music-lessons/flute-lessons',
							internal: false,
						},
						{
							title: 'Clarinet Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/music-lessons/clarinet-lessons',
							internal: false,
						},
						{
							title: 'Saxophone Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/music-lessons/saxophone-lessons',
							internal: false,
						},
						{
							title: 'Drum Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/music-lessons/drum-lessons',
							internal: false,
						},
						{
							title: 'Trumpet Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/music-lessons/trumpet-lessons',
							internal: false,
						},
						{
							title: 'Ukulele Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/music-lessons/ukulele-lessons',
							internal: false,
						},
						{
							title: 'Other Musical Instrument Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/music-lessons/other-musical-instrument-lessons',
							internal: false,
						},
					],
				},
				{
					title: 'Investing & Trading Lessons',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/investing-trading-lessons',
							internal: false,
						},
						{
							title: 'Stock Trading Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/investing-trading-lessons/stock-trading-lessons',
							internal: false,
						},
						{
							title: 'Investing Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/investing-trading-lessons/investing-lessons',
							internal: false,
						},
						{
							title: 'Forex Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/investing-trading-lessons/forex-lessons',
							internal: false,
						},
						{
							title: 'Financial Analysis Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/investing-trading-lessons/financial-analysis-lessons',
							internal: false,
						},
						{
							title: 'Technical Analysis Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/investing-trading-lessons/technical-analysis-lessons',
							internal: false,
						},
						{
							title: 'Options Trading Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/investing-trading-lessons/options-trading-lessons',
							internal: false,
						},
						{
							title: 'Financial Trading Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/investing-trading-lessons/financial-trading-lessons',
							internal: false,
						},
						{
							title: 'Day Trading Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/investing-trading-lessons/day-trading-lessons',
							internal: false,
						},
						{
							title: 'Algorithmic Trading Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/investing-trading-lessons/algorithmic-trading-lessons',
							internal: false,
						},
					],
				},
				{
					title: 'Finance / Accounting Lessons',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/finance-accounting-lessons',
							internal: false,
						},
						{
							title: 'Accounting / Bookkeeping Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/finance-accounting-lessons/accounting-bookkeeping-lessons',
							internal: false,
						},
						{
							title: 'Cryptocurrency / Blockchain Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/finance-accounting-lessons/cryptocurrency-blockchain-lessons',
							internal: false,
						},
						{
							title: 'Finance Cert & Exam Prep',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/finance-accounting-lessons/finance-cert-exam-prep',
							internal: false,
						},
						{
							title: 'Money Management Training',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/finance-accounting-lessons/money-management-training',
							internal: false,
						},
						{
							title: 'Economics Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/finance-accounting-lessons/economics-lessons',
							internal: false,
						},
						{
							title: 'Financial Modeling / Analysis Training',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/finance-accounting-lessons/financial-modeling-analysis-training',
							internal: false,
						},
						{
							title: 'Compliance Training',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/finance-accounting-lessons/compliance-training',
							internal: false,
						},
					],
				},
				{
					title: 'Business Training',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/business-training',
							internal: false,
						},
						{
							title: 'Professional Development Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/business-training/professional-development-lessons',
							internal: false,
						},
						{
							title: 'Business Software Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/business-training/business-software-lessons',
							internal: false,
						},
						{
							title: 'Accounting / Finance Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/business-training/accounting-finance-lessons',
							internal: false,
						},
						{
							title: 'Marketing Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/business-training/marketing-lessons',
							internal: false,
						},
						{
							title: 'Project Management Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/business-training/project-management-lessons',
							internal: false,
						},
						{
							title: 'Sales Strategy Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/business-training/sales-strategy-lessons',
							internal: false,
						},
						{
							title: 'Customer Service Resources',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/business-training/customer-service-resources',
							internal: false,
						},
						{
							title: 'Human Resources Support',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/business-training/human-resources-support',
							internal: false,
						},
						{
							title: 'Small Business Support',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/business-training/small-business-support',
							internal: false,
						},
						{
							title: 'Business Law Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/business-training/business-law-lessons',
							internal: false,
						},
						{
							title: 'Real Estate Training',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/business-training/real-estate-training',
							internal: false,
						},
						{
							title: 'Management Training',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/business-training/management-training',
							internal: false,
						},
						{
							title: 'Entrepreneurship Support',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/business-training/entrepreneurship-support',
							internal: false,
						},
					],
				},
				{
					title: 'IT / Computing',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/technology-computer-science',
							internal: false,
						},
						{
							title: 'Web Design Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/technology-computer-science/web-design-lessons',
							internal: false,
						},
						{
							title: 'Data Science Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/technology-computer-science/data-science-lessons',
							internal: false,
						},
						{
							title: 'IT Certification Training',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/technology-computer-science/it-certification-training',
							internal: false,
						},
						{
							title: 'Web Development Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/technology-computer-science/web-development-lessons',
							internal: false,
						},
						{
							title: 'Network / System Admin Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/technology-computer-science/network-system-admin-lessons',
							internal: false,
						},
						{
							title: 'IT Security Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/technology-computer-science/it-security-lessons',
							internal: false,
						},
						{
							title: 'Database Management Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/technology-computer-science/database-management-lessons',
							internal: false,
						},
						{
							title: 'Mobile Development Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/technology-computer-science/mobile-development-lessons',
							internal: false,
						},
						{
							title: 'DevOps Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/technology-computer-science/devops-lessons',
							internal: false,
						},
						{
							title: 'Cloud Computing Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/technology-computer-science/cloud-computing-lessons',
							internal: false,
						},
						{
							title: 'Operating System Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/technology-computer-science/operating-system-lessons',
							internal: false,
						},
						{
							title: 'Cryptography Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/technology-computer-science/cryptography-lessons',
							internal: false,
						},
					],
				},
				{
					title: 'Math Lessons',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/math-lessons',
							internal: false,
						},
						{
							title: 'Trigonometry Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/math-lessons/trigonometry-lessons',
							internal: false,
						},
						{
							title: 'Geometry Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/math-lessons/geometry-lessons',
							internal: false,
						},
						{
							title: 'Prealgebra Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/math-lessons/prealgebra-lessons',
							internal: false,
						},
						{
							title: 'Elementary Math Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/math-lessons/elementary-math-lessons',
							internal: false,
						},
						{
							title: 'Algebra Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/math-lessons/algebra-lessons',
							internal: false,
						},
						{
							title: 'Precalculus',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/math-lessons/precalculus',
							internal: false,
						},
						{
							title: 'Calculus Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/math-lessons/calculus-lessons',
							internal: false,
						},
						{
							title: 'Statistics and Probability Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/math-lessons/statistics-and-probability-lessons',
							internal: false,
						},
					],
				},
				{
					title: 'Science Lessons',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/science-lessons',
							internal: false,
						},
						{
							title: 'Physics Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/science-lessons/physics-lessons',
							internal: false,
						},
						{
							title: 'Biology Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/science-lessons/biology-lessons',
							internal: false,
						},
						{
							title: 'Chemistry Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/science-lessons/chemistry-lessons',
							internal: false,
						},
						{
							title: 'AP Physics Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/science-lessons/ap-physics-lessons',
							internal: false,
						},
						{
							title: 'Applied Science Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/science-lessons/applied-science-lessons',
							internal: false,
						},
						{
							title: 'Health Science Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/science-lessons/health-science-lessons',
							internal: false,
						},
						{
							title: 'Engineering Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/science-lessons/engineering-lessons',
							internal: false,
						},
						{
							title: 'Space and Astronomy Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/science-lessons/space-and-astronomy-lessons',
							internal: false,
						},
						{
							title: 'Scientific Method Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/science-lessons/scientific-method-lessons',
							internal: false,
						},
					],
				},
				{
					title: 'English Lessons',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/english-lessons',
							internal: false,
						},
						{
							title: 'Grammar Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/english-lessons/grammar-lessons',
							internal: false,
						},
						{
							title: 'Punctuation Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/english-lessons/punctuation-lessons',
							internal: false,
						},
						{
							title: 'Writing Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/english-lessons/writing-lessons',
							internal: false,
						},
						{
							title: 'English Pronunciation Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/english-lessons/english-pronunciation-lessons',
							internal: false,
						},
						{
							title: 'English Vocabulary Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/english-lessons/english-vocabulary-lessons',
							internal: false,
						},
						{
							title: 'ESL/ESOL Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/english-lessons/esl-lessons',
							internal: false,
						},
						{
							title: 'IELTS Support',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/english-lessons/ielts-support',
							internal: false,
						},
						{
							title: 'Business English Courses',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/english-lessons/business-english-courses',
							internal: false,
						},
						{
							title: 'Semantics Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/english-lessons/semantics-lessons',
							internal: false,
						},
						{
							title: 'Etymology Study',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/english-lessons/etymology-lessons',
							internal: false,
						},
					],
				},
				{
					title: 'Creative / Design Lessons',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/creative-lessons',
							internal: false,
						},
						{
							title: 'Animation / Illustration Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/creative-lessons/animation-illustration-lessons',
							internal: false,
						},
						{
							title: 'Graphic Design Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/creative-lessons/graphic-design-lessons',
							internal: false,
						},
						{
							title: 'Photography Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/creative-lessons/photography-lessons',
							internal: false,
						},
						{
							title: 'Video Editing Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/creative-lessons/video-editing-lessons',
							internal: false,
						},
						{
							title: 'Motion Graphics and VFX Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/creative-lessons/motion-graphics-and-vfx-lessons',
							internal: false,
						},
						{
							title: 'Drawing Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/creative-lessons/drawing-lessons',
							internal: false,
						},
						{
							title: 'Typography Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/creative-lessons/typography-lessons',
							internal: false,
						},
						{
							title: 'Design Thinking Training',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/creative-lessons/design-thinking-training',
							internal: false,
						},
						{
							title: 'User Experience Training',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/creative-lessons/user-experience-training',
							internal: false,
						},
					],
				},
				{
					title: 'History Lessons',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/history-lessons',
							internal: false,
						},
						{
							title: 'Government & Politics Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/history-lessons/government-politics-lessons',
							internal: false,
						},
						{
							title: 'US History Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/history-lessons/us-history-lessons',
							internal: false,
						},
						{
							title: 'World History Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/history-lessons/world-history-lessons',
							internal: false,
						},
						{
							title: 'Military History Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/history-lessons/military-history-lessons',
							internal: false,
						},
						{
							title: 'Gender History Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/history-lessons/gender-history-lessons',
							internal: false,
						},
						{
							title: 'History of Religion Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/history-lessons/history-of-religion-lessons',
							internal: false,
						},
						{
							title: 'Economic History Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/history-lessons/economic-history-lessons',
							internal: false,
						},
						{
							title: 'Social History Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/history-lessons/social-history-lessons',
							internal: false,
						},
						{
							title: 'Environmental History Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/history-lessons/environmental-history-lessons',
							internal: false,
						},
						{
							title: 'Diplomatic History Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/history-lessons/diplomatic-history-lessons',
							internal: false,
						},
						{
							title: 'Anthropology / Cultural History Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/history-lessons/cultural-history-lessons',
							internal: false,
						},
					],
				},
				{
					title: 'Test Prep Tutors',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/test-prep-tutors',
							internal: false,
						},
						{
							title: 'SAT Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/test-prep-tutors/sat-lessons',
							internal: false,
						},
						{
							title: 'ACT Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/test-prep-tutors/act-lessons',
							internal: false,
						},
						{
							title: 'GRE Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/test-prep-tutors/gre-lessons',
							internal: false,
						},
						{
							title: 'LSAT Training',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/test-prep-tutors/lsat-training',
							internal: false,
						},
					],
				},
				{
					title: 'Teacher Training',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/teacher-training',
							internal: false,
						},
						{
							title: 'Instructional Design Training',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/teacher-training/instructional-design-training',
							internal: false,
						},
						{
							title: 'Train the Trainer Courses',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/teacher-training/train-the-trainer-courses',
							internal: false,
						},
						{
							title: 'Presentation / Public Speaking Skills',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/teacher-training/presentation-public-speaking-skills',
							internal: false,
						},
						{
							title: 'Early Childhood Education',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/teacher-training/early-childhood-education',
							internal: false,
						},
						{
							title: 'Moodle Training',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/teacher-training/moodle-training',
							internal: false,
						},
					],
				},
				{
					title: 'Health & Fitness Teachers',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/health-fitness-teachers',
							internal: false,
						},
						{
							title: 'Online Fitness Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/health-fitness-teachers/online-fitness-lessons',
							internal: false,
						},
						{
							title: 'General Health Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/health-fitness-teachers/general-health-lessons',
							internal: false,
						},
						{
							title: 'Sports Training',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/health-fitness-teachers/sports-training',
							internal: false,
						},
						{
							title: 'Nutrition Education',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/health-fitness-teachers/nutrition-education',
							internal: false,
						},
						{
							title: 'Yoga Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/health-fitness-teachers/yoga-lessons',
							internal: false,
						},
						{
							title: 'Dieting Support',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/health-fitness-teachers/dieting-support',
							internal: false,
						},
						{
							title: 'Mental Health Support',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/health-fitness-teachers/mental-health-support',
							internal: false,
						},
						{
							title: 'Self Defense Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/health-fitness-teachers/self-defense-lessons',
							internal: false,
						},
						{
							title: 'Safety & First Aid Training',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/health-fitness-teachers/safety-first-aid-training',
							internal: false,
						},
						{
							title: 'Online Dance Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/health-fitness-teachers/online-dance-lessons',
							internal: false,
						},
						{
							title: 'Meditation Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/health-fitness-teachers/meditation-lessons',
							internal: false,
						},
						{
							title: 'Other Health Training',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/health-fitness-teachers/other-health-training',
							internal: false,
						},
					],
				},
				{
					title: 'Office Productivity',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/office-productivity',
							internal: false,
						},
						{
							title: 'Microsoft Product Training',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/office-productivity/microsoft-product-training',
							internal: false,
						},
						{
							title: 'Apple Product Training',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/office-productivity/apple-product-training',
							internal: false,
						},
						{
							title: 'Google Product Training',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/office-productivity/google-product-training',
							internal: false,
						},
						{
							title: 'SAP Training',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/office-productivity/sap-training',
							internal: false,
						},
						{
							title: 'Oracle Training',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/office-productivity/oracle-training',
							internal: false,
						},
						{
							title: 'Other Training',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/office-productivity/other-training',
							internal: false,
						},
					],
				},
				{
					title: 'Personal Development',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/personal-development',
							internal: false,
						},
						{
							title: 'Therapy / Relationship Advice',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/personal-development/therapy-relationship-advice',
							internal: false,
						},
						{
							title: 'Psychology / Addiction Support',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/personal-development/psychology-lessons',
							internal: false,
						},
						{
							title: 'Counseling / Self Esteem Support',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/personal-development/self-esteem-support',
							internal: false,
						},
						{
							title: 'Parenting Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/personal-development/parenting-lessons',
							internal: false,
						},
						{
							title: 'Memory / Study Skills',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/personal-development/memory-study-skills',
							internal: false,
						},
						{
							title: 'Religion & Spirituality',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/personal-development/religion-spirituality',
							internal: false,
						},
						{
							title: 'Public Speaking Training',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/personal-development/public-speaking-training',
							internal: false,
						},
						{
							title: 'Communications Skills',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/personal-development/communications-skills',
							internal: false,
						},
						{
							title: 'Learning Disability Support',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/personal-development/learning-disability-support',
							internal: false,
						},
						{
							title: 'Critical Thinking Training',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/personal-development/critical-thinking-training',
							internal: false,
						},
						{
							title: 'Personal Finance Help',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/personal-development/personal-finance-help',
							internal: false,
						},
						{
							title: 'Personal Transformation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/personal-development/personal-transformation',
							internal: false,
						},
						{
							title: 'Personal Brand Building',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/personal-development/personal-brand-building',
							internal: false,
						},
						{
							title: 'Stress Management Training',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/personal-development/stress-management-training',
							internal: false,
						},
						{
							title: 'Happiness Goals',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/personal-development/happiness-goals',
							internal: false,
						},
						{
							title: 'Anger Management Classes',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/personal-development/anger-management-classes',
							internal: false,
						},
						{
							title: 'Leadership Training',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/personal-development/leadership-training',
							internal: false,
						},
						{
							title: 'Motivational Trainers',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/personal-development/motivational-trainers',
							internal: false,
						},
						{
							title: 'Career Development Support',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/personal-development/career-development-support',
							internal: false,
						},
						{
							title: 'Productivity',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/personal-development/productivity',
							internal: false,
						},
					],
				},
				{
					title: 'Lifestyle Lessons',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/lifestyle-lessons',
							internal: false,
						},
						{
							title: 'Cooking Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/lifestyle-lessons/cooking-lessons',
							internal: false,
						},
						{
							title: 'Bread Baking Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/lifestyle-lessons/bread-baking-lessons',
							internal: false,
						},
						{
							title: 'Drinks / Bartending Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/lifestyle-lessons/drinks-bartending-lessons',
							internal: false,
						},
						{
							title: 'Dog Training Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/lifestyle-lessons/dog-training-lessons',
							internal: false,
						},
						{
							title: 'Feng Shui Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/lifestyle-lessons/feng-shui-lessons',
							internal: false,
						},
						{
							title: 'House Buying Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/lifestyle-lessons/house-buying-lessons',
							internal: false,
						},
						{
							title: 'Chess Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/lifestyle-lessons/chess-lessons',
							internal: false,
						},
						{
							title: 'Gaming / Twitch Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/lifestyle-lessons/poker-lessons',
							internal: false,
						},
						{
							title: 'Beauty / Makeup Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/lifestyle-lessons/makeup-artistry-lessons',
							internal: false,
						},
						{
							title: 'Travel Guides',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/lifestyle-lessons/travel-guides',
							internal: false,
						},
						{
							title: 'Pet Care Lessons',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/lifestyle-lessons/pet-care-lessons',
							internal: false,
						},
						{
							title: 'Reflexology Techniques',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/lifestyle-lessons/reflexology-techniques',
							internal: false,
						},
						{
							title: 'Sport / Outdoors',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/online-tutors/lifestyle-lessons/sport-outdoors',
							internal: false,
						},
					],
				},
			],
		},
		{
			title: 'Writing / Translation',
			navs: [
				{
					title: 'View All',
					strong: true,
					link:
						'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers',
					internal: false,
				},
				{
					title: 'Translation',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/translation-freelancers',
							internal: false,
						},
						{
							title: 'General Translation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/translation-freelancers/general-translation',
							internal: false,
						},
						{
							title: 'Technical Translation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/translation-freelancers/technical-translation',
							internal: false,
						},
						{
							title: 'Legal Translation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/translation-freelancers/legal-translation',
							internal: false,
						},
						{
							title: 'Linguistics',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/translation-freelancers/linguistics',
							internal: false,
						},
						{
							title: 'Other Languages Translation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/translation-freelancers/other-languages-translation',
							internal: false,
						},
						{
							title: 'French Translation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/translation-freelancers/french-translation',
							internal: false,
						},
						{
							title: 'Chinese Translation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/translation-freelancers/chinese-translation',
							internal: false,
						},
						{
							title: 'Portuguese Translation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/translation-freelancers/portuguese-translation',
							internal: false,
						},
						{
							title: 'Spanish Translation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/translation-freelancers/spanish-translation',
							internal: false,
						},
						{
							title: 'Japanese Translation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/translation-freelancers/japanese-translation',
							internal: false,
						},
						{
							title: 'Italian Translation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/translation-freelancers/italian-translation',
							internal: false,
						},
						{
							title: 'German Translation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/translation-freelancers/german-translation',
							internal: false,
						},
						{
							title: 'Dutch Translation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/translation-freelancers/dutch-translation',
							internal: false,
						},
						{
							title: 'Arabic Translation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/translation-freelancers/arabic-translation',
							internal: false,
						},
						{
							title: 'Russian Translation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/translation-freelancers/russian-translation',
							internal: false,
						},
						{
							title: 'Hindi Translation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/translation-freelancers/hindi-translation',
							internal: false,
						},
						{
							title: 'Turkish Translation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/translation-freelancers/turkish-translation',
							internal: false,
						},
						{
							title: 'Urdu Translation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/translation-freelancers/urdu-translation',
							internal: false,
						},
						{
							title: 'Tamil Translation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/translation-freelancers/tamil-translation',
							internal: false,
						},
						{
							title: 'Vietnamese Translation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/translation-freelancers/vietnamese-translation',
							internal: false,
						},
						{
							title: 'Romanian Translation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/translation-freelancers/romanian-translation',
							internal: false,
						},
						{
							title: 'Polish Translation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/translation-freelancers/polish-translation',
							internal: false,
						},
						{
							title: 'Ukrainian Translation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/translation-freelancers/ukrainian-translation',
							internal: false,
						},
					],
				},
				{
					title: 'Creative Writing',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/creative-writing',
							internal: false,
						},
						{
							title: 'Dramatic Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/creative-writing/dramatic-writing',
							internal: false,
						},
						{
							title: 'Character Development',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/creative-writing/character-development',
							internal: false,
						},
						{
							title: 'Plot Development',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/creative-writing/plot-development',
							internal: false,
						},
						{
							title: 'Imaginative Language',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/creative-writing/imaginative-language',
							internal: false,
						},
						{
							title: 'Dialogue',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/creative-writing/dialogue',
							internal: false,
						},
						{
							title: 'Underlying Theme',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/creative-writing/underlying-theme',
							internal: false,
						},
						{
							title: 'Point of View',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/creative-writing/point-of-view',
							internal: false,
						},
						{
							title: 'Vivid Setting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/creative-writing/vivid-setting',
							internal: false,
						},
						{
							title: 'Metaphors / Similes',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/creative-writing/metaphors-similes',
							internal: false,
						},
						{
							title: 'Anecdotes',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/creative-writing/anecdotes',
							internal: false,
						},
						{
							title: 'Figures of Speech',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/creative-writing/figures-of-speech',
							internal: false,
						},
						{
							title: 'Emotional Appeal',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/creative-writing/emotional-appeal',
							internal: false,
						},
						{
							title: 'Heavy Description',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/creative-writing/heavy-description',
							internal: false,
						},
					],
				},
				{
					title: 'Book Writing',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/book-writing',
							internal: false,
						},
						{
							title: 'Fiction Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/book-writing/fiction-writing',
							internal: false,
						},
						{
							title: 'Short Stories',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/book-writing/short-stories',
							internal: false,
						},
						{
							title: 'eBook Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/book-writing/ebook-writing',
							internal: false,
						},
						{
							title: 'Non-fiction Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/book-writing/non-fiction-writing',
							internal: false,
						},
						{
							title: 'Story Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/book-writing/story-writing',
							internal: false,
						},
						{
							title: 'Science Fiction / Fantasy Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/book-writing/science-fiction-writing',
							internal: false,
						},
						{
							title: '',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/book-writing/childrens-young-adult',
							internal: false,
						},
						{
							title: 'Romance Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/book-writing/romance-writing',
							internal: false,
						},
						{
							title: 'Autobiography / Biography',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/book-writing/autobiography-biography',
							internal: false,
						},
						{
							title: 'Publishing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/book-writing/publishing',
							internal: false,
						},
						{
							title: 'Contemporary Literature',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/book-writing/contemporary-literature',
							internal: false,
						},
						{
							title: 'Contemporary Fiction',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/book-writing/contemporary-fiction',
							internal: false,
						},
						{
							title: 'Literary Fiction',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/book-writing/literary-fiction',
							internal: false,
						},
						{
							title: 'Fiction Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/book-writing/fiction-writing',
							internal: false,
						},
						{
							title: 'Booklets',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/book-writing/booklets',
							internal: false,
						},
						{
							title: 'Book Proposals',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/book-writing/book-proposals',
							internal: false,
						},
						{
							title: 'Cookbooks',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/book-writing/cookbooks',
							internal: false,
						},
						{
							title: 'Memoir Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/book-writing/memoir-writing',
							internal: false,
						},
						{
							title: 'General / Other Books',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/book-writing/general-other-books',
							internal: false,
						},
					],
				},
				{
					title: 'General Writing',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/general-writing',
							internal: false,
						},
						{
							title: 'Technical Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/general-writing/technical-writing',
							internal: false,
						},
						{
							title: 'Descriptive Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/general-writing/descriptive-writing',
							internal: false,
						},
						{
							title: 'Letter Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/general-writing/letter-writing',
							internal: false,
						},
						{
							title: 'Persuasive Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/general-writing/persuasive-writing',
							internal: false,
						},
						{
							title: 'Ghost Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/general-writing/ghost-writing',
							internal: false,
						},
						{
							title: 'Self-Publishing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/general-writing/self-publishing',
							internal: false,
						},
						{
							title: 'Blurb Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/general-writing/blurb-writing',
							internal: false,
						},
						{
							title: 'Manuscript / Screen Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/general-writing/manuscript',
							internal: false,
						},
						{
							title: 'Synopsis Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/general-writing/synopsis-writing',
							internal: false,
						},
						{
							title: 'Summarizing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/general-writing/summarizing',
							internal: false,
						},
						{
							title: 'Prose',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/general-writing/prose',
							internal: false,
						},
						{
							title: 'Query Letters',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/general-writing/query-letters',
							internal: false,
						},
						{
							title: 'Satire',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/general-writing/satire',
							internal: false,
						},
						{
							title: 'Redaction',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/general-writing/redaction',
							internal: false,
						},
						{
							title: 'Materials Development',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/general-writing/materials-development',
							internal: false,
						},
					],
				},
				{
					title: 'Copywriting',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/copywriting',
							internal: false,
						},
						{
							title: 'Slogan Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/copywriting/slogan-writing',
							internal: false,
						},
						{
							title: 'Marketing Copywriting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/copywriting/marketing-copywriting',
							internal: false,
						},
						{
							title: 'Product Descriptions',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/copywriting/product-descriptions',
							internal: false,
						},
						{
							title: 'Creative Copywriting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/copywriting/creative-copywriting',
							internal: false,
						},
						{
							title: 'SEO Copywriting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/copywriting/seo-copywriting',
							internal: false,
						},
						{
							title: 'Content Copywriting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/copywriting/content-copywriting',
							internal: false,
						},
						{
							title: 'Technical Copywriting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/copywriting/technical-copywriting',
							internal: false,
						},
					],
				},
				{
					title: 'Editing / Proofreading',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/editing-proofreading',
							internal: false,
						},
						{
							title: 'Editing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/editing-proofreading/editing',
							internal: false,
						},
						{
							title: 'Proofreading',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/editing-proofreading/proofreading',
							internal: false,
						},
						{
							title: 'English Grammar',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/editing-proofreading/english-grammar',
							internal: false,
						},
						{
							title: 'Article Editing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/editing-proofreading/article-editing',
							internal: false,
						},
						{
							title: 'Index Creation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/editing-proofreading/index-creation',
							internal: false,
						},
						{
							title: 'APA Style',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/editing-proofreading/apa-style',
							internal: false,
						},
						{
							title: 'Book Editing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/editing-proofreading/book-editing',
							internal: false,
						},
						{
							title: 'Copy Editing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/editing-proofreading/copy-editing',
							internal: false,
						},
						{
							title: 'Line Editing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/editing-proofreading/line-editing',
							internal: false,
						},
						{
							title: 'Chicago Manual of Style',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/editing-proofreading/chicago-manual-of-style',
							internal: false,
						},
						{
							title: 'Associated Press (AP) Style',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/editing-proofreading/associated-press-ap-style',
							internal: false,
						},
						{
							title: 'Technical Editing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/editing-proofreading/technical-editing',
							internal: false,
						},
						{
							title: 'Academic Editing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/editing-proofreading/academic-editing',
							internal: false,
						},
						{
							title: 'Style Guide Development',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/editing-proofreading/style-guide-development',
							internal: false,
						},
						{
							title: 'Substantive Editing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/editing-proofreading/substantive-editing',
							internal: false,
						},
						{
							title: 'Rewriting and Revisions',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/editing-proofreading/rewriting-and-revisions',
							internal: false,
						},
						{
							title: 'Editorial Consulting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/editing-proofreading/editorial-consulting',
							internal: false,
						},
						{
							title: 'Developmental Editing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/editing-proofreading/developmental-editing',
							internal: false,
						},
						{
							title: 'MLA Style',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/editing-proofreading/mla-style',
							internal: false,
						},
						{
							title: 'Citations',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/editing-proofreading/citations',
							internal: false,
						},
					],
				},
				{
					title: 'Web Content',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/web-content',
							internal: false,
						},
						{
							title: 'Content Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/web-content/content-writing',
							internal: false,
						},
						{
							title: 'Blog Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/web-content/blog-writing',
							internal: false,
						},
						{
							title: 'On Page SEO',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/web-content/on-page-seo',
							internal: false,
						},
						{
							title: 'Content Editing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/web-content/content-editing',
							internal: false,
						},
						{
							title: 'Content Marketing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/web-content/content-marketing',
							internal: false,
						},
						{
							title: 'Other Web Content',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/web-content/other-web-content',
							internal: false,
						},
					],
				},
				{
					title: 'Article / News Writing',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/article-news-writing',
							internal: false,
						},
						{
							title: 'Article Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/article-news-writing/article-writing',
							internal: false,
						},
						{
							title: 'Blog Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/article-news-writing/blog-writing',
							internal: false,
						},
						{
							title: 'Article Editing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/article-news-writing/article-editing',
							internal: false,
						},
						{
							title: 'News Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/article-news-writing/news-writing',
							internal: false,
						},
						{
							title: 'Journalism',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/article-news-writing/journalism',
							internal: false,
						},
						{
							title: 'Journalistic Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/article-news-writing/journalistic-writing',
							internal: false,
						},
						{
							title: 'Feature Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/article-news-writing/feature-writing',
							internal: false,
						},
						{
							title: 'Editorial Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/article-news-writing/editorial-writing',
							internal: false,
						},
						{
							title: 'Magazine Articles',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/article-news-writing/magazine-articles',
							internal: false,
						},
						{
							title: 'Newspaper',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/article-news-writing/newspaper',
							internal: false,
						},
						{
							title: 'Newsletters',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/article-news-writing/newsletters',
							internal: false,
						},
						{
							title: 'Citations',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/article-news-writing/citations',
							internal: false,
						},
						{
							title: 'Lifestyle Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/article-news-writing/lifestyle-writing',
							internal: false,
						},
						{
							title: 'Investigative Reporting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/article-news-writing/investigative-reporting',
							internal: false,
						},
						{
							title: 'Fact Checking',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/article-news-writing/fact-checking',
							internal: false,
						},
						{
							title: 'Video Journalism',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/article-news-writing/video-journalism',
							internal: false,
						},
						{
							title: 'Business Journalism',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/article-news-writing/business-journalism',
							internal: false,
						},
						{
							title: 'Arts Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/article-news-writing/arts-writing',
							internal: false,
						},
						{
							title: 'Corporate Blogging',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/article-news-writing/corporate-blogging',
							internal: false,
						},
						{
							title: 'Content Curation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/article-news-writing/content-curation',
							internal: false,
						},
						{
							title: 'How To Articles',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/article-news-writing/how-to-articles',
							internal: false,
						},
					],
				},
				{
					title: 'Subtitling',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/subtitling',
							internal: false,
						},
						{
							title: 'Intralingual Subtitling',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/subtitling/intralingual-subtitling',
							internal: false,
						},
						{
							title: 'Interlingual Subtitling',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/subtitling/interlingual-subtitling',
							internal: false,
						},
						{
							title: 'Closed Captioning',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/subtitling/closed-captioning',
							internal: false,
						},
					],
				},
				{
					title: 'Research',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/research',
							internal: false,
						},
						{
							title: 'Essay Research',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/research/essay-research',
							internal: false,
						},
						{
							title: 'Art Research',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/research/art-research',
							internal: false,
						},
						{
							title: 'Fact Checking',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/research/fact-checking',
							internal: false,
						},
						{
							title: 'Report Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/research/report-writing',
							internal: false,
						},
					],
				},
				{
					title: 'Writing for Industries',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/writing-for-industries',
							internal: false,
						},
						{
							title: 'Marketing / Sales / PR',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/writing-for-industries/marketing-sales-pr',
							internal: false,
						},
						{
							title: 'Health / Medical / Dental',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/writing-for-industries/health-medical-dental',
							internal: false,
						},
						{
							title: 'Psychology / Behavior',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/writing-for-industries/psychology-behavior',
							internal: false,
						},
						{
							title: 'Travel / Tourism / Entertainment',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/writing-for-industries/travel-tourism-entertainment',
							internal: false,
						},
						{
							title: 'Business / Finance',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/writing-for-industries/business-finance',
							internal: false,
						},
						{
							title: 'Sports / Fitness',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/writing-for-industries/sports-fitness',
							internal: false,
						},
						{
							title: 'Religion / Spirituality',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/writing-for-industries/religion-spirituality',
							internal: false,
						},
						{
							title: 'Government / Politics',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/writing-for-industries/government-politics',
							internal: false,
						},
						{
							title: 'Science / Math',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/writing-for-industries/science-math',
							internal: false,
						},
						{
							title: 'History / Culture',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/writing-for-industries/history-culture',
							internal: false,
						},
						{
							title: 'Food / Restaurants',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/writing-for-industries/food-restaurants',
							internal: false,
						},
						{
							title: 'Real Estate',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/writing-for-industries/real-estate',
							internal: false,
						},
					],
				},
				{
					title: 'Scripts / Speeches / Storyboards',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/scripts-speeches-storyboards',
							internal: false,
						},
						{
							title: 'Storyboarding',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/scripts-speeches-storyboards/storyboarding',
							internal: false,
						},
						{
							title: 'Speech Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/scripts-speeches-storyboards/speech-writing',
							internal: false,
						},
						{
							title: 'Public Speaking',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/scripts-speeches-storyboards/public-speaking',
							internal: false,
						},
						{
							title: 'Documentary',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/scripts-speeches-storyboards/documentary',
							internal: false,
						},
						{
							title: 'Film / TV Scripts',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/scripts-speeches-storyboards/film-tv-scripts',
							internal: false,
						},
						{
							title: 'Playwriting / Dramaturgy',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/scripts-speeches-storyboards/playwriting-dramaturgy',
							internal: false,
						},
					],
				},
				{
					title: 'Jobs / Resumes',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/jobs-resumes',
							internal: false,
						},
						{
							title: 'Resume Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/jobs-resumes/resume-writing',
							internal: false,
						},
						{
							title: 'Cover Letter Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/jobs-resumes/cover-letter-writing',
							internal: false,
						},
						{
							title: 'Personal Statement',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/jobs-resumes/personal-statement',
							internal: false,
						},
						{
							title: 'Job Description Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/jobs-resumes/job-description-writing',
							internal: false,
						},
						{
							title: 'Curriculum Vitae (CV) Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/jobs-resumes/curriculum-vitae-cv-writing',
							internal: false,
						},
						{
							title: 'LinkedIn Profile',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/jobs-resumes/linkedin-profile',
							internal: false,
						},
					],
				},
				{
					title: 'Review Writing',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/review-writing',
							internal: false,
						},
						{
							title: 'Review Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/review-writing/review-writing',
							internal: false,
						},
						{
							title: 'Book Reviews',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/review-writing/book-reviews',
							internal: false,
						},
						{
							title: 'Product Reviews',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/review-writing/product-reviews',
							internal: false,
						},
						{
							title: 'Literary Criticism',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/review-writing/literary-criticism',
							internal: false,
						},
						{
							title: 'Art / Food Reviews',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/review-writing/art-food-reviews',
							internal: false,
						},
						{
							title: 'Music Review',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/review-writing/music-review',
							internal: false,
						},
						{
							title: 'Document Review',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/review-writing/document-review',
							internal: false,
						},
						{
							title: 'Movie Review',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/review-writing/movie-review',
							internal: false,
						},
					],
				},
				{
					title: 'Grants / Proposals',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/grants-proposals',
							internal: false,
						},
						{
							title: 'Proposal Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/grants-proposals/proposal-writing',
							internal: false,
						},
						{
							title: 'Business Proposal Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/grants-proposals/business-proposal-writing',
							internal: false,
						},
						{
							title: 'Grant Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/grants-proposals/grant-writing',
							internal: false,
						},
						{
							title: 'Government Proposals',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/grants-proposals/government-proposals',
							internal: false,
						},
					],
				},
				{
					title: 'Songs / Poems',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/songs-poems',
							internal: false,
						},
						{
							title: 'Poetry',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/songs-poems/poetry',
							internal: false,
						},
						{
							title: 'Lyrics Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/songs-poems/lyrics-writing',
							internal: false,
						},
						{
							title: 'Song Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/writing-translation-freelancers/songs-poems/song-writing',
							internal: false,
						},
					],
				},
			],
		},
		{
			title: 'Business / Admin',
			navs: [
				{
					title: 'View All',
					strong: true,
					link:
						'https://www.hostjane.com/marketplace/categories/business-admin-support',
					internal: false,
				},
				{
					title: 'Digital Marketing',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/digital-marketing-freelancers',
							internal: false,
						},
						{
							title: 'SEO',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/digital-marketing-freelancers/search-engine-optimization-seo',
							internal: false,
						},
						{
							title: 'SMM',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/digital-marketing-freelancers/social-media-marketing-smm',
							internal: false,
						},
						{
							title: 'SEM / PPC',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/digital-marketing-freelancers/search-engine-marketing-sem-ppc',
							internal: false,
						},
						{
							title: 'Mobile Marketing / Advertising',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/digital-marketing-freelancers/mobile-marketing-advertising',
							internal: false,
						},
						{
							title: 'Content / Copywriting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/digital-marketing-freelancers/content-copywriting',
							internal: false,
						},
						{
							title: 'Landing Pages',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/digital-marketing-freelancers/landing-pages',
							internal: false,
						},
						{
							title: 'Email Marketing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/digital-marketing-freelancers/email-text',
							internal: false,
						},
						{
							title: 'Influencer Marketing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/digital-marketing-freelancers/influencer-marketing',
							internal: false,
						},
						{
							title: 'Site Performance',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/digital-marketing-freelancers/site-performance',
							internal: false,
						},
						{
							title: 'SMS / Text Marketing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/digital-marketing-freelancers/sms-text-marketing',
							internal: false,
						},
						{
							title: 'Marketing Strategy',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/digital-marketing-freelancers/marketing-strategy',
							internal: false,
						},
						{
							title: 'Surveys',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/digital-marketing-freelancers/surveys',
							internal: false,
						},
						{
							title: 'Web Traffic',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/digital-marketing-freelancers/web-traffic',
							internal: false,
						},
						{
							title: 'Local Listings',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/digital-marketing-freelancers/local-listings',
							internal: false,
						},
						{
							title: 'Crowdfunding',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/digital-marketing-freelancers/crowdfunding',
							internal: false,
						},
						{
							title: 'Domain Research',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/digital-marketing-freelancers/domain-research',
							internal: false,
						},
						{
							title: 'Web Analytics',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/digital-marketing-freelancers/web-analytics',
							internal: false,
						},
						{
							title: 'Other Web Marketing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/digital-marketing-freelancers/general-other-web-marketing',
							internal: false,
						},
					],
				},
				{
					title: 'Accounting / Finance / Tax',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa',
							internal: false,
						},
						{
							title: 'Accounting / Tax Returns',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa/accounting-taxes',
							internal: false,
						},
						{
							title: 'Bookkeeping',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa/bookkeeping',
							internal: false,
						},
						{
							title: 'Payroll',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa/payroll',
							internal: false,
						},
						{
							title: 'Accounts Payable / Receivable',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa/accounts-payable-receivable',
							internal: false,
						},
						{
							title: 'QuickBooks',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa/quickbooks',
							internal: false,
						},
						{
							title: 'TurboTax',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa/turbotax',
							internal: false,
						},
						{
							title: 'Financial Analysis / Planning',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa/financial-analysis-planning',
							internal: false,
						},
						{
							title: 'Invoicing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa/invoicing',
							internal: false,
						},
						{
							title: 'Profit / Loss',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa/profit-loss',
							internal: false,
						},
						{
							title: 'Cost Accounting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa/cost-accounting',
							internal: false,
						},
						{
							title: 'Stock Trading',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa/stock-trading',
							internal: false,
						},
						{
							title: 'Forex Trading',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa/forex-trading',
							internal: false,
						},
						{
							title: 'Investment Banking',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa/microsoft-money',
							internal: false,
						},
						{
							title: 'Virtual / Cryptocurrency',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa/virtual-cryptocurrency',
							internal: false,
						},
						{
							title: 'Banking / Loans',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa/banking-loans',
							internal: false,
						},
						{
							title: 'Risk / Claims',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa/risk-claims',
							internal: false,
						},
						{
							title: 'Collections',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa/collections',
							internal: false,
						},
						{
							title: 'Financial Audits',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa/financial-audits',
							internal: false,
						},
						{
							title: 'Securities',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa/securities',
							internal: false,
						},
						{
							title: 'FreshBooks',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa/freshbooks',
							internal: false,
						},
						{
							title: 'NetSuite',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa/netsuite',
							internal: false,
						},
						{
							title: 'Sage',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa/sage',
							internal: false,
						},
						{
							title: 'Oracle Financials',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa/oracle-financials',
							internal: false,
						},
						{
							title: 'Xero',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa/xero',
							internal: false,
						},
						{
							title: 'Personal Finances',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/accounting-finance-tax-cpa/personal-finances',
							internal: false,
						},
					],
				},
				{
					title: 'Business Help / Consulting',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/business-help-consultation',
							internal: false,
						},
						{
							title: 'Start-ups / Small Business Help',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/business-help-consultation/start-ups-small-business',
							internal: false,
						},
						{
							title: 'Project Management',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/business-help-consultation/project-management',
							internal: false,
						},
						{
							title: 'Planning / Development',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/business-help-consultation/planning-development',
							internal: false,
						},
						{
							title: 'Business Development',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/business-help-consultation/business-development',
							internal: false,
						},
						{
							title: 'Management / Leadership',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/business-help-consultation/management-leadership',
							internal: false,
						},
						{
							title: 'Strategy / Competitor Analysis',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/business-help-consultation/strategy-competitor-analysis',
							internal: false,
						},
						{
							title: 'Business Communications',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/business-help-consultation/business-communications',
							internal: false,
						},
						{
							title: 'Business Valuation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/business-help-consultation/business-valuation',
							internal: false,
						},
					],
				},
				{
					title: 'Legal Assistance',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance',
							internal: false,
						},
						{
							title: 'Intellectual Property / Copyright Law',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance/intellectual-property-copyright-law',
							internal: false,
						},
						{
							title: 'Employment Law',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance/employment-law',
							internal: false,
						},
						{
							title: 'Contracts',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance/contracts',
							internal: false,
						},
						{
							title: 'Paralegal',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance/paralegal',
							internal: false,
						},
						{
							title: 'Civil Law',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance/civil-law',
							internal: false,
						},
						{
							title: 'Civil Litigation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance/civil-litigation',
							internal: false,
						},
						{
							title: 'Banking Law',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance/banking-law',
							internal: false,
						},
						{
							title: 'Tax Law',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance/tax-law',
							internal: false,
						},
						{
							title: 'Legal Documents',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance/legal-documents',
							internal: false,
						},
						{
							title: 'Insurance Law',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance/insurance',
							internal: false,
						},
						{
							title: 'Personal Injury Law',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance/personal-injury-law',
							internal: false,
						},
						{
							title: 'Legal Transcription',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance/legal-transcription',
							internal: false,
						},
						{
							title: 'Employment Contracts',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance/employment-contracts',
							internal: false,
						},
						{
							title: 'Business Formation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance/business-formation',
							internal: false,
						},
						{
							title: 'Bankruptcy',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance/bankruptcy',
							internal: false,
						},
						{
							title: 'Securities Law',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance/securities-law',
							internal: false,
						},
						{
							title: 'Real Estate Law',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance/real-estate-law',
							internal: false,
						},
						{
							title: 'Immigration Law',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance/immigration-law',
							internal: false,
						},
						{
							title: 'Criminal Law',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance/criminal-law',
							internal: false,
						},
						{
							title: 'Corporate Law',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance/corporate-law',
							internal: false,
						},
						{
							title: 'Mediation / Negotiation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance/mediation-negotiation',
							internal: false,
						},
						{
							title: 'Patents',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance/patents',
							internal: false,
						},
						{
							title: 'Trademark Consulting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance/trademark-consulting',
							internal: false,
						},
						{
							title: 'Licensing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance/licensing',
							internal: false,
						},
						{
							title: 'Legal Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance/legal-writing',
							internal: false,
						},
						{
							title: 'Legal Research',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/legal-assistance/legal-research',
							internal: false,
						},
					],
				},
				{
					title: 'Personal / Virtual Assistants',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/personal-virtual-assistants',
							internal: false,
						},
						{
							title: 'Business Help / Consulting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/personal-virtual-assistants/business-consulting',
							internal: false,
						},
						{
							title: 'Virtual Assistants',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/personal-virtual-assistants/virtual-assistants',
							internal: false,
						},
						{
							title: 'Remote Office Workers',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/personal-virtual-assistants/remote-office-workers',
							internal: false,
						},
						{
							title: 'Personal Assistant',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/personal-virtual-assistants/personal-assistant',
							internal: false,
						},
						{
							title: 'Chat Support',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/personal-virtual-assistants/chat-support',
							internal: false,
						},
						{
							title: 'Email Support',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/personal-virtual-assistants/email-support',
							internal: false,
						},
						{
							title: 'Appointment Setting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/personal-virtual-assistants/appointment-setting',
							internal: false,
						},
						{
							title: 'Records Management',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/personal-virtual-assistants/records-management',
							internal: false,
						},
						{
							title: 'Web Research / Posting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/personal-virtual-assistants/web-research-posting',
							internal: false,
						},
						{
							title: 'Web Scraping',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/personal-virtual-assistants/web-scraping',
							internal: false,
						},
						{
							title: 'Event Planning',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/personal-virtual-assistants/event-planning',
							internal: false,
						},
						{
							title: 'Travel Planning',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/personal-virtual-assistants/travel-planning',
							internal: false,
						},
						{
							title: 'Presentation Help',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/personal-virtual-assistants/presentation-help',
							internal: false,
						},
						{
							title: 'Slideshow Design',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/personal-virtual-assistants/slideshow-design',
							internal: false,
						},
						{
							title: 'Microsoft PowerPoint',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/personal-virtual-assistants/microsoft-powerpoint',
							internal: false,
						},
						{
							title: 'Scheduling',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/personal-virtual-assistants/scheduling',
							internal: false,
						},
						{
							title: 'Calendar Management',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/personal-virtual-assistants/calendar-management',
							internal: false,
						},
					],
				},
				{
					title: 'Transcription Services',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/transcription-freelancers',
							internal: false,
						},
						{
							title: 'Audio Transcription',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/transcription-freelancers/audio-transcription',
							internal: false,
						},
						{
							title: 'Medical Transcription',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/transcription-freelancers/medical-transcription',
							internal: false,
						},
						{
							title: 'Legal Transcription',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/transcription-freelancers/legal-transcription',
							internal: false,
						},
						{
							title: 'Video Transcription',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/transcription-freelancers/video-transcription',
							internal: false,
						},
						{
							title: 'Shorthand / Stenography',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/transcription-freelancers/shorthand-stenography',
							internal: false,
						},
						{
							title: 'Express Scribe',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/transcription-freelancers/express-scribe',
							internal: false,
						},
						{
							title: 'Dictation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/transcription-freelancers/dictation',
							internal: false,
						},
						{
							title: 'Court Reporting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/transcription-freelancers/court-reporting',
							internal: false,
						},
					],
				},
				{
					title: 'Sales / General Marketing',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/sales-general-marketing',
							internal: false,
						},
						{
							title: 'Google Analytics',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/sales-general-marketing/google-analytics',
							internal: false,
						},
						{
							title: 'Advertising',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/sales-general-marketing/advertising',
							internal: false,
						},
						{
							title: 'Direct Marketing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/sales-general-marketing/direct-marketing',
							internal: false,
						},
						{
							title: 'CRM',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/sales-general-marketing/crm',
							internal: false,
						},
						{
							title: 'Salesforce',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/sales-general-marketing/salesforce',
							internal: false,
						},
						{
							title: 'Sales',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/sales-general-marketing/sales',
							internal: false,
						},
						{
							title: 'Lead Generation',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/sales-general-marketing/lead-generation',
							internal: false,
						},
						{
							title: 'Public Relations (PR)',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/sales-general-marketing/public-relations-pr',
							internal: false,
						},
						{
							title: 'Inbound marketing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/sales-general-marketing/inbound-marketing',
							internal: false,
						},
					],
				},
				{
					title: 'Press Release Writing',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/press-release-writing',
							internal: false,
						},
						{
							title: 'Event Press Releases',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/press-release-writing/event-press-releases',
							internal: false,
						},
						{
							title: 'General News Releases',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/press-release-writing/general-news-releases',
							internal: false,
						},
						{
							title: 'Launch Release Writing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/press-release-writing/launch-release-writing',
							internal: false,
						},
						{
							title: 'Employment / Staff Releases',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/press-release-writing/employment-staff-releases',
							internal: false,
						},
						{
							title: 'Expert Position Releases',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/press-release-writing/expert-position-releases',
							internal: false,
						},
					],
				},
				{
					title: 'Call Centers / Telemarketing',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/call-centers-telemarketing',
							internal: false,
						},
						{
							title: 'Telemarketing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/call-centers-telemarketing/telemarketing',
							internal: false,
						},
						{
							title: 'Phone Support',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/call-centers-telemarketing/phone-support',
							internal: false,
						},
						{
							title: 'Telesales',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/call-centers-telemarketing/telesales',
							internal: false,
						},
						{
							title: 'Outbound Sales',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/call-centers-telemarketing/outbound-sales',
							internal: false,
						},
						{
							title: 'Cold Calling',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/call-centers-telemarketing/cold-calling',
							internal: false,
						},
						{
							title: 'Call Center Management',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/call-centers-telemarketing/call-center-management',
							internal: false,
						},
						{
							title: 'Five9',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/call-centers-telemarketing/five9',
							internal: false,
						},
					],
				},
				{
					title: 'Data Entry',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/data-entry',
							internal: false,
						},
						{
							title: 'Data Mining',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/data-entry/data-mining',
							internal: false,
						},
						{
							title: 'Data Analysis',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/data-entry/data-analysis',
							internal: false,
						},
						{
							title: 'Copy and Paste',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/data-entry/copy-and-paste',
							internal: false,
						},
						{
							title: 'Data Collection',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/data-entry/data-collection',
							internal: false,
						},
						{
							title: 'Data Processing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/data-entry/data-processing',
							internal: false,
						},
						{
							title: 'Data Audit',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/data-entry/data-audit',
							internal: false,
						},
						{
							title: 'Records Management',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/data-entry/records-management',
							internal: false,
						},
						{
							title: 'Order Entry',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/data-entry/order-entry',
							internal: false,
						},
						{
							title: 'Order Processing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/data-entry/order-processing',
							internal: false,
						},
						{
							title: 'eBay Sales',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/data-entry/ebay-sales',
							internal: false,
						},
						{
							title: 'Document Conversion',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/data-entry/document-conversion',
							internal: false,
						},
						{
							title: 'Data Cleaning',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/data-entry/data-cleaning',
							internal: false,
						},
						{
							title: 'Form Filling',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/data-entry/form-filling',
							internal: false,
						},
						{
							title: 'Data Encoding',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/data-entry/data-encoding',
							internal: false,
						},
						{
							title: 'Data Conversion',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/data-entry/data-conversion',
							internal: false,
						},
						{
							title: 'File Management',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/data-entry/file-management',
							internal: false,
						},
					],
				},
				{
					title: 'Microsoft Office Software',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/microsoft-office-software',
							internal: false,
						},
						{
							title: 'Microsoft Word',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/microsoft-office-software/microsoft-word',
							internal: false,
						},
						{
							title: 'Microsoft Excel',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/microsoft-office-software/microsoft-excel',
							internal: false,
						},
						{
							title: 'Microsoft Excel Pivot Tables',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/microsoft-office-software/microsoft-excel-pivot-tables',
							internal: false,
						},
						{
							title: 'Microsoft Excel Models',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/microsoft-office-software/microsoft-excel-models',
							internal: false,
						},
						{
							title: 'Microsoft Excel Dashboards',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/microsoft-office-software/microsoft-excel-dashboards',
							internal: false,
						},
						{
							title: 'Microsoft Outlook',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/microsoft-office-software/microsoft-outlook',
							internal: false,
						},
						{
							title: 'Microsoft PowerPoint',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/microsoft-office-software/microsoft-powerpoint',
							internal: false,
						},
						{
							title: 'Microsoft Excel Charts',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/microsoft-office-software/microsoft-excel-charts',
							internal: false,
						},
						{
							title: 'Microsoft Office',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/microsoft-office-software/microsoft-office',
							internal: false,
						},
						{
							title: 'Microsoft OneNote',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/microsoft-office-software/microsoft-onenote',
							internal: false,
						},
						{
							title: 'Microsoft Project',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/microsoft-office-software/microsoft-project',
							internal: false,
						},
						{
							title: 'Microsoft InfoPath',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/microsoft-office-software/microsoft-infopath',
							internal: false,
						},
					],
				},
				{
					title: 'Customer Service',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/customer-service',
							internal: false,
						},
						{
							title: 'Order Processing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/customer-service/order-processing',
							internal: false,
						},
						{
							title: 'Phone Support',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/customer-service/phone-support',
							internal: false,
						},
						{
							title: 'Email Support',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/customer-service/email-support',
							internal: false,
						},
						{
							title: 'Salesforce',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/customer-service/salesforce',
							internal: false,
						},
						{
							title: 'Account Management',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/customer-service/account-management',
							internal: false,
						},
						{
							title: 'Client Administration',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/customer-service/client-administration',
							internal: false,
						},
						{
							title: 'Client Issue Resolution',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/customer-service/client-issue-resolution',
							internal: false,
						},
						{
							title: 'Zendesk',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/customer-service/zendesk',
							internal: false,
						},
					],
				},
				{
					title: 'Word Processing / Typing',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/word-processing-typing',
							internal: false,
						},
						{
							title: 'Microsoft Word',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/word-processing-typing/microsoft-word',
							internal: false,
						},
						{
							title: 'Reports',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/word-processing-typing/reports',
							internal: false,
						},
						{
							title: 'Google Docs',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/word-processing-typing/google-docs',
							internal: false,
						},
						{
							title: 'Fillable Forms',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/word-processing-typing/fillable-forms',
							internal: false,
						},
						{
							title: 'PDF Editing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/word-processing-typing/pdf-editing',
							internal: false,
						},
						{
							title: 'Keyboarding',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/word-processing-typing/keyboarding',
							internal: false,
						},
					],
				},
				{
					title: 'Spreadsheets / Data Manipulation',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/spreadsheets-data-manipulation',
							internal: false,
						},
						{
							title: 'Microsoft Excel',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/spreadsheets-data-manipulation/microsoft-excel',
							internal: false,
						},
						{
							title: 'Spreadsheets',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/spreadsheets-data-manipulation/spreadsheets',
							internal: false,
						},
						{
							title: 'Google Spreadsheet',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/spreadsheets-data-manipulation/google-spreadsheet',
							internal: false,
						},
						{
							title: 'PDF to Excel',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/spreadsheets-data-manipulation/pdf-to-excel',
							internal: false,
						},
						{
							title: 'Graphs',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/spreadsheets-data-manipulation/graphs',
							internal: false,
						},
					],
				},
				{
					title: 'Mailings / Lists',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/mailings-lists',
							internal: false,
						},
						{
							title: 'Mail Merge',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/mailings-lists/mail-merge',
							internal: false,
						},
						{
							title: 'Faxing',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/mailings-lists/faxing',
							internal: false,
						},
						{
							title: 'Email List Building',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/mailings-lists/email-list-building',
							internal: false,
						},
						{
							title: 'Bulk Mailings / Lists',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/mailings-lists/bulk-mailings-lists',
							internal: false,
						},
						{
							title: 'Contact Management',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/mailings-lists/contact-management',
							internal: false,
						},
					],
				},
				{
					title: 'Human Resources (HR)',
					navs: [
						{
							title: 'View All',
							strong: true,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/human-resources',
							internal: false,
						},
						{
							title: 'Training / Development',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/human-resources/training-development',
							internal: false,
						},
						{
							title: 'General HR',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/human-resources/general-hr',
							internal: false,
						},
						{
							title: 'Staffing / Recruiting',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/human-resources/staffing-recruiting',
							internal: false,
						},
						{
							title: 'Benefits / Insurance',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/human-resources/benefits-insurance',
							internal: false,
						},
						{
							title: 'Conflict Resolution',
							strong: false,
							link:
								'https://www.hostjane.com/marketplace/categories/business-admin-support/human-resources/conflict-resolution',
							internal: false,
						},
					],
				},
			],
		},
		{
			title: 'Categories',
			link: 'https://www.hostjane.com/marketplace/categories/',
			internal: false,
			type: 'hasBorderTop',
			icon: CategoriesIcon,
		},
		{
			title: 'Skills',
			link: '#',
			internal: false,
			icon: SkillsIcon,
		},
		{
			title: 'Sell / Teach',
			link: '/sell',
			internal: true,
			icon: SellIcon,
		},
		{
			title: 'Community',
			link: '/community',
			internal: false,
			type: 'footerButton',
		},
	],
};
