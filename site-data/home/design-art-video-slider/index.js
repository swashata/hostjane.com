import React from 'react';

export const title = 'Design / art / video / voice';

export const explore = {
	url: '/marketplace/categories/design-art-video-voice',
	label: 'Explore all',
	internal: false,
};

export function Description() {
	return <>Tap a creative well with no gatekeepers. Hire freelance designers &amp; artists for web, traditional, digital work.</>;
}
