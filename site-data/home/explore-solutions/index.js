import { ReactComponent as JaneCloudIcon } from '../../../src/svgs/nav/JaneCloud.svg';
import { ReactComponent as JaneDediIcon } from '../../../src/svgs/nav/JaneDedi.svg';
import { ReactComponent as JaneSellsIcon } from '../../../src/svgs/nav/JaneSells.svg';
import { ReactComponent as JaneVPSIcon } from '../../../src/svgs/nav/JaneVPS.svg';

export default {
	title: 'Explore our solutions',
	items: [
		{
			icon: JaneVPSIcon,
			title: 'VPS Hosting',
			description:
				'High-performance Managed SSD VPS & Self-Managed Linux VPS.',
			list: [
				'Available with cPanel',
				'Free backups / SSL',
				'Managed WordPress option',
			],
			link: '/hosting/vps-hosting-wizard/',
			linkLabel: 'From $7.95/mo',
			internal: true,
		},
		{
			icon: JaneCloudIcon,
			title: 'Cloud Compute',
			description:
				'Managed SSD VMs. 100% SLA. Full root access & 24hr backups.',
			list: [
				'Up to 4 TB bandwidth',
				'Anti-DDoS protection',
				'24/7 tech support',
			],
			link: '/hosting/cloud-hosting-wizard/',
			linkLabel: 'From 0.0139/hr',
			internal: true,
		},
		{
			icon: JaneDediIcon,
			title: 'Dedicated Servers',
			description:
				'Single tenant, dedicated hosting with 100% SLA & choice of OS.',
			list: [
				'Up to 10 TB bandwidth',
				'24hr backups',
				'24/7 tech support',
			],
			link: '/hosting/dedicated-servers-hosting-wizard/',
			linkLabel: 'From $95/mo',
			internal: true,
		},
		{
			icon: JaneSellsIcon,
			title: 'Reseller VPS',
			description:
				'Resell managed, white-label VPS hosting with 100% SLA.',
			list: [
				'Up to 4 TB bandwidth',
				'Available with cPanel',
				'24/7 tech support',
			],
			link: '/hosting/reseller-hosting-wizard/',
			linkLabel: 'From $7.95/mo',
			internal: true,
		},
	],
};
