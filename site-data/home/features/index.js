import { ReactComponent as AskJaneIcon } from './ask-jane.svg';
import { ReactComponent as CommunityIcon } from './community.svg';
import { ReactComponent as HostJaneBlogIcon } from './hostjane-blog.svg';
import { ReactComponent as VPSFeaturesIcon } from './vps-features.svg';

export default {
	items: [
		{
			icon: VPSFeaturesIcon,
			title: 'Why HostJane?',
			description:
				"Server features & specifications.",
			link: 'https://support.hostjane.com/servers/',
			internal: false,
		},
		{
			icon: AskJaneIcon,
			title: 'Sell or Teach Skills',
			description:
				"Offer your skill as a service or lesson.",
			link:
				'/sell/',
			internal: true,
		},
		{
			icon: CommunityIcon,
			title: 'Community',
			description:
				'Ask / answer questions + chat',
			link: '/community',
			internal: false,
		},
		{
			icon: HostJaneBlogIcon,
			title: 'VPS Guides',
			description:
				'Review our VPS User Guides.',
			link: 'https://support.hostjane.com/vps-setup',
			internal: false,
		},
	],
};
