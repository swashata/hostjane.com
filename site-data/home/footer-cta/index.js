export default {
	title: 'Get up and running fast',
	sections: [
		{
			title: 'Hire Freelancers',
			buttons: [
				{
					title: 'Get started for free',
					internal: false,
					link: '/marketplace/register',
					type: 'primary',
				},
				{
					title: 'Sign in',
					internal: false,
					link: '/marketplace/login',
					type: 'secondary',
				},
			],
		},
		{
			title: 'Managed Hosting',
			buttons: [
				{
					title: 'Get started',
					internal: true,
					link: '/hosting/',
					type: 'primary',
				},
				{
					title: 'Hosting login',
					internal: false,
					link: '/webhost/client/login',
					type: 'secondary',
				},
			],
		},
	],
	list: [
		{
			label: 'Need help with an issue?',
			title: 'Contact Support',
			link:
				'https://hostjane.atlassian.net/servicedesk/customer/portal/1',
			internal: false,
		},
		{
			label: 'Turn your skills into income!',
			title: 'Become a seller',
			link: 'https://support.hostjane.com/seller-guide',
			internal: false,
		},
		{
			label: 'Migrating your VPS to HostJane?',
			title: 'Read our help guide',
			link: 'https://support.hostjane.com/servers/vps',
			internal: false,
		},
	],
	footer:
		'HostJane serves as a limited authorized payment collection agent for Sellers.',
};
