import React from 'react';
import { ReactComponent as FreelancersIcon } from './freelancers.svg';
import { ReactComponent as WebHostingIcon } from './web-hosting.svg';

export default {
	title: 'Cut costs.',
	leftColumn: {
		title: 'Marketplace',
		icon: FreelancersIcon,
		description: 'HostJane makes it easy to buy and sell skills.',
		ctas: [
			{
				type: 'primary',
				label: 'Find services',
				link: 'https://www.hostjane.com/marketplace',
				internal: false,
			},
			{
				type: 'secondary',
				label: 'Browse skills',
				link: 'https://www.hostjane.com/marketplace/skills',
				internal: false,
			},
		],
	},
	rightColumn: {
		title: 'Hosting',
		icon: WebHostingIcon,
		description: (
			<>
				VPS hosting with backups from $7.95/mo.
			</>
		),
		ctas: [
			{
				type: 'primary',
				label: 'Get Started',
				link: '/hosting/',
				internal: true,
			},
			{
				type: 'secondary',
				label: 'Start wizard',
				link: '/hosting/vps-hosting-wizard/',
				internal: true,
			},
		],
	},
};
