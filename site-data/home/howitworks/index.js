import { ReactComponent as MoneyBackIcon } from './Money-back-guarantee.svg';
import { ReactComponent as NoHiddenFeesIcon } from './no-hidden-fees.svg';
import { ReactComponent as SupportIcon } from './24-7-support.svg';

export default {
	heading: 'Work faster, with more confidence.',
	items: [
		{
			title: 'Sign up for free',
			description:
				'Then talk to sellers in secure chats to get quotes.',
			icon: MoneyBackIcon,
		},
		{
			title: 'Verify payment',
			description:
				'Your seller is only paid when you approve their work.',
			icon: NoHiddenFeesIcon,
			link: '/legal/marketplace-terms/',
			linkLabel: 'The small print',
			internalLink: true,
		},
		{
			title: 'On time delivery',
			description:
				'Your seller must fulfil your order or lesson on time.',
			icon: SupportIcon,
		},
	],
};
