export default {
	title: 'Start building on HostJane',
	description:
		'HostJane uses the secure Tier III datacenters that big companies use. During checkout, choose from 17 prime worldwide locations including New York, Singapore, and Amsterdam.',
	buttons: [
		{
			label: 'Buy Managed VPS',
			link: '/hosting/vps-hosting-wizard/',
			type: 'primary',
		},
		{
			label: 'Cloud Servers from $0.0139/hr',
			link: '/hosting/cloud-hosting-wizard/',
			type: 'secondary',
		},
	],
	continents: [
		'Africa',
		'Antarctica',
		'Asia',
		'Australia',
		'Europe',
		'North America',
		'Oceania',
		'South America',
	],
	markers: [
		// Region 1
		{
			label: 'Miami',
			latitude: 25.7617,
			longitude: -80.1918,
			region: 'Region 1: US/CA',
		},
		{
			label: 'New Jersey',
			latitude: 40.0583,
			longitude: -74.4057,
			region: 'Region 1: US/CA',
		},
		{
			label: 'Chicago',
			latitude: 41.8781,
			longitude: -87.6298,
			region: 'Region 1: US/CA',
		},
		{
			label: 'Dallas',
			latitude: 32.7767,
			longitude: -96.797,
			region: 'Region 1: US/CA',
		},
		{
			label: 'Toronto',
			latitude: 43.6532,
			longitude: -79.3832,
			region: 'Region 1: US/CA',
		},
		{
			label: 'Silicon Valley',
			latitude: 37.3875,
			longitude: -122.0575,
			region: 'Region 1: US/CA',
		},
		{
			label: 'Los Angeles',
			latitude: 34.0522,
			longitude: -118.2437,
			region: 'Region 1: US/CA',
		},
		{
			label: 'Seattle',
			latitude: 47.6062,
			longitude: -122.3321,
			region: 'Region 1: US/CA',
		},
		{
			label: 'Atlanta',
			latitude: 33.749,
			longitude: -84.388,
			region: 'Region 1: US/CA',
		},
		// Region 2
		{
			label: 'Paris',
			latitude: 48.8566,
			longitude: 2.3522,
			region: 'Region 2: EUROPE',
		},
		{
			label: 'London',
			latitude: 51.5074,
			longitude: -0.1278,
			region: 'Region 2: EUROPE',
		},
		{
			label: 'Frankfurt',
			latitude: 50.1109,
			longitude: 8.6821,
			region: 'Region 2: EUROPE',
		},
		{
			label: 'Amsterdam',
			latitude: 52.3667,
			longitude: 4.8945,
			region: 'Region 2: EUROPE',
		},
		// Region 3
		{
			label: 'Tokyo',
			latitude: 35.6762,
			longitude: 139.6503,
			region: 'Region 3: ASIA',
		},
		{
			label: 'South Korea',
			latitude: 35.9078,
			longitude: 127.7669,
			region: 'Region 3: ASIA',
		},
		{
			label: 'Singapore',
			latitude: 1.3521,
			longitude: 103.8198,
			region: 'Region 3: ASIA',
		},
		// Region 4
		{
			label: 'Sydney',
			latitude: -33.8688,
			longitude: 151.2093,
			region: 'Region 4: OCEANIA',
		},
	],
};
