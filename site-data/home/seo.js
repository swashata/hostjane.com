export const pageTitle =
	'HostJane Skills - Freelance Marketplace & VPS Hosting';
export const pageDescription =
	'Power your business with HostJane. Find freelance services. Scale with managed SSD VPS. Free to join, pay only for work you approve.';
