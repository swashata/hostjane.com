import React from 'react';

import { SafeGuardModal } from '../../modals/safeguard';
import { useModal } from '../../../src/utils/globalModal';

export const title = 'Hire people who code';

export const explore = {
	url: '/marketplace/categories/web-mobile-tech-freelancers',
	label: 'Explore all',
	internal: false,
};

export function Description() {
	const [open, openModal, closeModal] = useModal();

	return (
		<>
			Find the skills to solve your toughest problems. Each hire is covered by {' '}
			<button
				type="button"
				onClick={e => {
					e.preventDefault();
					openModal();
				}}
			>
				Purchase Protection.
			</button>
			<SafeGuardModal open={open} closeModal={closeModal} />
		</>
	);
}
