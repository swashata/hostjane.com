/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import { useModal } from '../../src/utils/globalModal';
import { SafeGuardModal } from '../modals/safeguard';

// Change the HEX code of background/text color here
// MUST BE FULL 6 DIGIT HEX FOR IT TO WORK
export const backgroundColor = '#663399';
export const textColor = '#f1defa';
export const backgroundGradient =
	'linear-gradient(90deg,#3d096b,#3d0875,#3c077f,#3a0789,#360794,#360794,#360794,#360794,#3a0789,#3c077f,#3d0875,#3d096b)';

// Here edit the JSX (HTML) For alert
// To not show any alert, uncomment this line
// export const Alert = null;
// and comment everything below
export const Alert = () => {
	const [open, openModal, closeModal] = useModal();

	return (
		<>
			<strong>UPDATE:</strong> HostJane Skills is{' '}
			<a
				href="#"
				onClick={e => {
					e.preventDefault();
					openModal();
				}}
			>
				almost here
			</a>
			<SafeGuardModal open={open} closeModal={closeModal} />
		</>
	);
};
