import { ReactComponent as FeaturesIcon } from './icons/features.svg';
import { ReactComponent as FixIssueIcon } from './icons/fix-issue.svg';
import { ReactComponent as HelpIcon } from './icons/help.svg';
import { ReactComponent as LaunchVPSIcon } from './icons/launch-vps.svg';
import { ReactComponent as MyAccountIcon } from './icons/my-account.svg';
import { ReactComponent as PrivacyIcon } from './icons/privacy.svg';

export const buttons = [
	{
		internal: false,
		url: '#hostjane-manage-server', // don't change this
		icon: FeaturesIcon,
		label: 'Features',
	},
	{
		internal: false,
		url: '#',
		icon: MyAccountIcon,
		label: 'My Account',
	},
	{
		internal: false,
		url: '#hostjane-faqs', // don't change this
		icon: HelpIcon,
		label: 'FAQs',
	},
	{
		internal: true,
		url: '/hosting/vps-hosting-wizard/',
		icon: LaunchVPSIcon,
		label: 'Wizard',
	},
	{
		internal: false,
		url: '#',
		icon: FixIssueIcon,
		label: 'Fix an issue',
	},
	{
		internal: false,
		url: '#',
		icon: PrivacyIcon,
		label: 'Help',
	},
];
