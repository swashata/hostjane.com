export const screenshotsData = {
	heading: 'Manage from anywhere',
	subHeading:
		'Create, stop, start, delete and snapshot your server in the click of a button. Dedicated support team is here for you day and night.',
};
