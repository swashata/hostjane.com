import React from 'react';

import { ReactComponent as CPanelIcon } from './icons/cpanel.svg';
import { ReactComponent as LAMPIcon } from './icons/lamp.svg';
import { ReactComponent as LEMPIcon } from './icons/lemp.svg';
import { ReactComponent as WPIcon } from './icons/wp.svg';
import { ReactComponent as CloudIcon } from '../../../src/svgs/nav/JaneCloud.svg';

export const vpsBoxData = {
	heading: 'Build your server',
	subheading:
		'Managed for you by experts—HostJane\'s powerful VPS include free backups, optional control panels, full root access & 100% uptime guaranteed.',
	boxes: [
		{
			icons: [WPIcon],
			planLabel: 'WordPress VPS',
			planSlogan: 'As low as',
			amount: '$19.99',
			duration: 'mo',
			sale: 'On sale - Save 33%',
			saleTm: 'Continued discount applies at renewal.',
			linkInternal: true,
			link: '/hosting/vps-hosting-wizard/?appType=1&app=0',
			linkLabel: 'Configure Your Server',
			features: [
				{ count: '1', label: 'Intel Core CPU' },
				{ count: '1 GB', label: 'Guaranteed RAM' },
				{ count: '25 GB', label: 'SSD Storage' },
			],
			footer: (
				<>
					<strong>WP admin</strong>, managed server.
				</>
			),
		},
		{
			icons: [CPanelIcon],
			planLabel: 'VPS cPanel',
			planSlogan: 'As low as',
			amount: '$39.99',
			duration: 'mo',
			sale: 'On sale - Save 20%',
			saleTm: 'Continued discount applies at renewal.',
			linkInternal: true,
			link: '/hosting/vps-hosting-wizard/?appType=0&app=0',
			linkLabel: 'Configure Your Server',
			features: [
				{ count: '1', label: 'Intel Core CPU' },
				{ count: '2 GB', label: 'Guaranteed RAM' },
				{ count: '55 GB', label: 'SSD Storage' },
			],
			footer: (
				<>
					<strong>cPanel license</strong>, managed server.
				</>
			),
		},
		{
			icons: [LAMPIcon, LEMPIcon],
			smallIcons: [CloudIcon],
			planLabel: 'Linux VPS',
			planSlogan: 'As low as',
			amount: '$7.95',
			duration: 'mo',
			sale: 'On sale - Save 20%',
			saleTm: 'Continued discount applies at renewal.',
			linkInternal: true,
			link: '/hosting/vps-hosting-wizard/?appType=3&app=0',
			linkLabel: 'Configure Your Server',
			features: [
				{ count: '1', label: 'Intel Core CPU' },
				{ count: '1 GB', label: 'Guaranteed RAM' },
				{ count: '25 GB', label: 'SSD Storage' },
			],
			footer: (
				<>
					<strong>Linux only</strong>, self-managed.
				</>
			),
		},
	],
};
