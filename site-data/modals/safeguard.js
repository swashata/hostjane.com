import React from 'react';
import { Link } from 'gatsby';
import {
	HIWItem,
	PaymentShield,
	Article,
	SmallBorderedParagraph,
	SmallFooterParagraph,
} from '../../src/components/StyledContents';
import { Modal, ModalBody, ModalHeader } from '../../src/components/Modal';

// TITLE OF MODAL
export const title = 'How It Works';

// REACT COMPONENT FOR MODAL CONTENT
export function ModalContent() {
	return (
		<Article>
			<SmallBorderedParagraph>
				On HostJane you only pay if you get what you paid for. Find talented sellers in a smart, easy and safer workspace for buyers.
			</SmallBorderedParagraph>
			<HIWItem fullWidth index={0} title="Create a free account">
				All you need is a valid email address. You must agree to our{' '}
				<Link to="/legal/marketplace-terms/">Marketplace Terms</Link>{' '}
				and <Link to="/legal/privacy/">Privacy Policy</Link>.
			</HIWItem>
			<HIWItem fullWidth index={1} title="Browse services">
				We advise you to message sellers before hiring their services.
			</HIWItem>
			<HIWItem fullWidth index={2} title="Place funds on hold">
				Use PayPal or a verified credit or debit card with 3-D secure to
				place your funds with HostJane.
			</HIWItem>
			<HIWItem fullWidth index={3} title="Pay on approval">
				Release funds to seller when you are satisfied with your Work
				Preview or Final Work.
				<PaymentShield title="Purchase Protection">
					Your money back if Work is unsatisfactory or not delivered
					in stated time.
				</PaymentShield>
			</HIWItem>
			<HIWItem index={4} fullWidth hasFlame title="Rate your seller">
				Give your seller a flame rating, with 5 flames being the best.
				You can add a tip for good work.
			</HIWItem>
			<SmallFooterParagraph>
				All purchases are subject to an admin fee of $2. We need this to
				pay for hosting, overheads, and providing you with 24×7 support,
				365 days.
			</SmallFooterParagraph>
		</Article>
	);
}

// DONT EDIT THIS
export function SafeGuardModal({ open, closeModal }) {
	return (
		<Modal isOpen={open} closeModal={closeModal} contentLabel={title}>
			<ModalHeader title={title} closeModal={closeModal} />
			<ModalBody>
				<ModalContent />
			</ModalBody>
		</Modal>
	);
}
