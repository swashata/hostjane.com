const sectionOne = {
	title: 'HostJane Site Map',
	columns: {
		// column one
		one: [
			// list one
			{
				title: 'Buy Hosting',
				links: [
					{
						title: 'Hosting Overview',
						link: '#',
					},
					{
						title: 'SSD Cloud Servers',
						link: '#',
					},
					{
						title: 'SSD VPS Hosting',
						link: '#',
					},
					{
						title: 'SSD Dedicated Hosting',
						link: '#',
					},
					{
						title: 'Reseller VPS Hosting',
						link: '#',
					},
				],
			},
			// list two
			{
				title: 'Hosting Portal',
				links: [
					{
						title: 'Manage Your Servers',
						link: '#',
					},
					{
						title: 'Setup Servers',
						link: '#',
					},
					{
						title: 'Hosting FAQs',
						link: '#',
					},
					{
						title: 'VPS Features',
						link: '#',
					},
					{
						title: 'Cloud Tutorials',
						link: '#',
					},
				],
			},
			// add more lists if needed
		],
		// column two
		two: [
			// list one
			{
				title: 'Help & Support',
				links: [
					{
						title: 'All Categories',
						link: '#',
					},
					{
						title: 'All Skills',
						link: '#',
					},
					{
						title: 'Contact Support',
						link: '#',
					},
					{
						title: 'Hosting Portal',
						link: '#',
					},
					{
						title: 'Community',
						link: '/community',
					},
					{
						title: 'Help Center',
						link: '#',
					},
					{
						title: 'Theme / Plugin Reviews',
						link: '#',
					},
					{
						title: 'Discord Chat',
						link: '#',
					},
				],
			},
			// list two
			{
				title: 'Legal Agreements',
				links: [
					{
						title: 'General Agreement',
						link: '#',
					},
					{
						title: 'Marketplace Terms',
						link: '#',
					},
					{
						title: 'Hosting Terms',
						link: '#',
					},
					{
						title: 'Privacy Policy',
						link: '#',
					},
					{
						title: 'Acceptable Use Policy',
						link: '#',
					},
					{
						title: 'GDPR',
						link: '#',
					},
					{
						title: 'Cookies Policy',
						link: '#',
					},
					{
						title: 'Service Level Agreement',
						link: '#',
					},
				],
			},
			// add more lists if needed
		],
		// end column two
		// column three
		three: [
			// list one
			{
				title: 'Sell on HostJane',
				links: [
					{
						title: 'Become a Seller',
						link: '#',
					},
					{
						title: 'Seller Forum',
						link: '#',
					},
					{
						title: 'My Revenues',
						link: '#',
					},
					{
						title: 'My Orders',
						link: '#',
					},
					{
						title: 'Create New Listing',
						link: '#',
					},
					{
						title: 'Studio Settings',
						link: '#',
					},
					{
						title: 'Payout Settings',
						link: '#',
					},
				],
			},
			// list two
			{
				title: 'Connect with HostJane',
				links: [
					{
						title: 'HostJane Facebook',
						link: '#',
					},
					{
						title: '@Janeispowerful',
						link: '#',
					},
					{
						title: 'HostJane Pinterest',
						link: '#',
					},
					{
						title: 'HostJane LinkedIn',
						link: '#',
					},
				],
			},
			// add more lists if needed
		],
		// end column three
	},
};

const sectionTwo = {
	title: 'Web / Mobile / Tech',
	columns: {
		// column one
		one: [
			// list one
			{
				title: 'Highest Demand',
				links: [
					{
						title: 'Web Development',
						link: '#',
					},
					{
						title: 'Programming / Software',
						link: '#',
					},
					{
						title: 'Apps / Mobile',
						link: '#',
					},
					{
						title: 'Database Design / Administration',
						link: '#',
					},
					{
						title: 'WordPress',
						link: '#',
					},
					{
						title: 'Networking / System Admin',
						link: '#',
					},
					{
						title: 'QA / Testing',
						link: '#',
					},
				],
			},
			// add more lists if needed
		],
		// column two
		two: [
			// list one
			{
				title: 'Featured Services',
				links: [
					{
						title: 'Information Security',
						link: '#',
					},
					{
						title: 'Ideas / Help / Consultation',
						link: '#',
					},
					{
						title: 'Technical Support',
						link: '#',
					},
					{
						title: 'Bug Fixing Services',
						link: '#',
					},
					{
						title: 'ERP / CRM / SCM',
						link: '#',
					},
					{
						title: 'Management / Training',
						link: '#',
					},
					{
						title: 'SAP',
						link: '#',
					},
				],
			},
			// add more lists if needed
		],
		// end column two
		// column three
		three: [
			// list one
			{
				title: 'Key Services',
				links: [
					{
						title: 'Telephony / Telecommunications',
						link: '#',
					},
					{
						title: 'Math / Science / Algorithms',
						link: '#',
					},
					{
						title: 'Game Development',
						link: '#',
					},
				],
			},
			// add more lists if needed
		],
		// end column three
	},
};

const sectionThree = {
	title: 'Design / Art / Video / Voice',
	columns: {
		// column one
		one: [
			// list one
			{
				title: 'Highest Demand',
				links: [
					{
						title: 'Web Design / Apps',
						link: '#',
					},
					{
						title: 'Graphic Design / Logos',
						link: '#',
					},
					{
						title: 'Freelance Artists / Other Art',
						link: '#',
					},
					{
						title: 'Video / Animation',
						link: '#',
					},
					{
						title: 'Illustration',
						link: '#',
					},
					{
						title: 'Voice Over / Acting',
						link: '#',
					},
					{
						title: 'Studio Musicians / Session Singers',
						link: '#',
					},
				],
			},
			// add more lists if needed
		],
		// column two
		two: [
			// list one
			{
				title: 'Featured Services',
				links: [
					{
						title: 'Business / Advertising Design',
						link: '#',
					},
					{
						title: 'Book / Magazine Design',
						link: '#',
					},
					{
						title: 'Audio / Sound / Music',
						link: '#',
					},
					{
						title: 'T-Shirts / Merchandise Design',
						link: '#',
					},
					{
						title: 'Packaging / Label Design',
						link: '#',
					},
					{
						title: 'CAD / Technical Drawings',
						link: '#',
					},
					{
						title: 'Image Restoration / Editing',
						link: '#',
					},
				],
			},
			// add more lists if needed
		],
		// end column two
		// column three
		three: [
			// list one
			{
				title: 'Key Services',
				links: [
					{
						title: 'Photography',
						link: '#',
					},
					{
						title: 'File Conversions',
						link: '#',
					},
					{
						title: 'Printing / Production',
						link: '#',
					},
					{
						title: 'Painting',
						link: '#',
					},
					{
						title: 'Cartoons / Comic Art',
						link: '#',
					},
					{
						title: 'Fashion',
						link: '#',
					},
				],
			},
			// add more lists if needed
		],
		// end column three
	},
};

const sectionFour = {
	title: 'Online Tutors',
	columns: {
		// column one
		one: [
			// list one
			{
				title: 'Highest Demand',
				links: [
					{
						title: 'Language Tutors',
						link: '#',
					},
					{
						title: 'Online Interpreting Services',
						link: '#',
					},
					{
						title: 'Music Lessons',
						link: '#',
					},
					{
						title: 'Investing & Trading Lessons',
						link: '#',
					},
					{
						title: 'Finance / Accounting Lessons',
						link: '#',
					},
					{
						title: 'Business Training',
						link: '#',
					},
					{
						title: 'IT / Computing',
						link: '#',
					},
				],
			},
			// add more lists if needed
		],
		// column two
		two: [
			// list one
			{
				title: 'Featured Services',
				links: [
					{
						title: 'Test Prep Tutors',
						link: '#',
					},
					{
						title: 'Math Lessons',
						link: '#',
					},
					{
						title: 'Science Lessons',
						link: '#',
					},
					{
						title: 'English Lessons',
						link: '#',
					},
					{
						title: 'Creative / Design Lessons',
						link: '#',
					},
					{
						title: 'Teacher Training',
						link: '#',
					},
					{
						title: 'History Lessons',
						link: '#',
					},
				],
			},
			// add more lists if needed
		],
		// end column two
		// column three
		three: [
			// list one
			{
				title: 'Key Services',
				links: [
					{
						title: 'Health & Fitness Teachers',
						link: '#',
					},
					{
						title: 'Office Productivity',
						link: '#',
					},
					{
						title: 'Personal Development',
						link: '#',
					},
				],
			},
			// add more lists if needed
		],
		// end column three
	},
};

const sectionFive = {
	title: 'Writing / Translation',
	columns: {
		// column one
		one: [
			// list one
			{
				title: 'Highest Demand',
				links: [
					{
						title: 'Translation',
						link: '#',
					},
					{
						title: 'Creative Writing',
						link: '#',
					},
					{
						title: 'Book Writing',
						link: '#',
					},
					{
						title: 'General Writing',
						link: '#',
					},
					{
						title: 'Copywriting',
						link: '#',
					},
					{
						title: 'Editing / Proofreading',
						link: '#',
					},
					{
						title: 'Web Content',
						link: '#',
					},
				],
			},
			// add more lists if needed
		],
		// column two
		two: [
			// list one
			{
				title: 'Featured Services',
				links: [
					{
						title: 'Article / News Writing',
						link: '#',
					},
					{
						title: 'Subtitling',
						link: '#',
					},
					{
						title: 'Research',
						link: '#',
					},
					{
						title: 'Writing for Industries',
						link: '#',
					},
					{
						title: 'Scripts / Speeches / Storyboards',
						link: '#',
					},
					{
						title: 'Jobs / Resumes',
						link: '#',
					},
					{
						title: 'Review Writing',
						link: '#',
					},
				],
			},
			// add more lists if needed
		],
		// end column two
		// column three
		three: [
			// list one
			{
				title: 'Key Services',
				links: [
					{
						title: 'Grants / Proposals',
						link: '#',
					},
					{
						title: 'Songs / Poems',
						link: '#',
					},
				],
			},
			// add more lists if needed
		],
		// end column three
	},
};

const sectionSix = {
	title: 'Business / Admin',
	columns: {
		// column one
		one: [
			// list one
			{
				title: 'Highest Demand',
				links: [
					{
						title: 'Digital Marketing',
						link: '#',
					},
					{
						title: 'Accounting / Finance / Tax',
						link: '#',
					},
					{
						title: 'Business Help / Consulting',
						link: '#',
					},
					{
						title: 'Legal Assistance',
						link: '#',
					},
					{
						title: 'Personal / Virtual Assistants',
						link: '#',
					},
					{
						title: 'Transcription Services',
						link: '#',
					},
					{
						title: 'Sales / General Marketing',
						link: '#',
					},
				],
			},
			// add more lists if needed
		],
		// column two
		two: [
			// list one
			{
				title: 'Featured Services',
				links: [
					{
						title: 'Press Release Writing',
						link: '#',
					},
					{
						title: 'Call Centers / Telemarketing',
						link: '#',
					},
					{
						title: 'Data Entry',
						link: '#',
					},
					{
						title: 'Microsoft Office Software',
						link: '#',
					},
					{
						title: 'Customer Service',
						link: '#',
					},
					{
						title: 'Word Processing / Typing',
						link: '#',
					},
					{
						title: 'Spreadsheets / Data Manipulation',
						link: '#',
					},
				],
			},
			// add more lists if needed
		],
		// end column two
		// column three
		three: [
			// list one
			{
				title: 'Key Services',
				links: [
					{
						title: 'Mailings / Lists',
						link: '#',
					},
					{
						title: 'Human Resources (HR)',
						link: '#',
					},
				],
			},
			// add more lists if needed
		],
		// end column three
	},
};

export default {
	pageTitle: 'HostJane Site Map',
	seoDescription: 'Sitemap for HostJane\'s hosting and skills marketplace giving buyers and sellers organized access to the platform.',
	sections: [
		// section one
		sectionOne,
		// section two
		sectionTwo,
		// section three
		sectionThree,
		// section four
		sectionFour,
		// section five
		sectionFive,
		// section six
		sectionSix,
	],
};
