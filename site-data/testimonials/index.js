export default {
	title: 'Small businesses trust HostJane',
	description:
		'Build, innovate, and scale your brand with HostJane. Host lightning-fast websites and applications. Collaborate and be more productive with expert freelancers.',
};
