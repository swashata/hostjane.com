import React from 'react';
import styled from 'styled-components';
import { MDXProvider } from '@mdx-js/react';
import { MDXRenderer } from 'gatsby-plugin-mdx';

import { Wrapper } from '../../Home/common';
import SiteContainer from '../../SiteContainer';
import { Article } from '../../StyledContents';

const StoryWrapper = styled(Wrapper)`
	${SiteContainer} {
		@media screen and (min-width: ${props =>
				props.theme.siteApp.breakpoints.tablet}px) {
			padding-left: 40px;
			padding-right: 40px;
		}
	}

	${Article} {
		font-size: 18px;
		line-height: 1.6;
		blockquote {
			padding: 0 0 0 20px;
			margin: 15px;
			font-size: 18px;
			line-height: 1.6;
			border-left: 5px solid #e3e4e5;
		}
		h4 {
			font-size: 26px;
			margin-top: 0;
			font-weight: 500;
			line-height: 1.6;
		}
		a {
			color: #0057d8;
			text-decoration: none;
			border-bottom: 1px dashed #0057d8;
			&:hover {
				color: #595959;
				text-decoration: underline;
			}
		}
	}
`;

const Columns = styled.div`
	display: flex;
	flex-flow: row wrap;
	justify-content: space-between;
`;

const Column = styled.div`
	flex: 0 0 100%;
	width: 100%;
	margin-bottom: 20px;
	&:last-child {
		margin-bottom: 0;
	}
	@media screen and (min-width: 1024px) {
		flex-basis: calc(50% - 20px);
		width: calc(50% - 20px);
		margin-bottom: 0;
	}
`;

const AlignmentImage = styled.img`
	padding: 0;
	padding-top: ${props => (props.paddingTop ? '20px' : 0)};
	margin: ${props =>
		props.align === 'left' ? '0 10px 10px 0' : '0 0 10px 10px'};
	width: ${props => props.width || '100px'};
	display: inline-block;
	float: ${props => props.align};
`;

export default function Story({ children }) {
	return (
		<StoryWrapper>
			<SiteContainer>
				<Article>
					<Columns>
						<MDXProvider components={{ Column, AlignmentImage }}>
							<MDXRenderer>{children}</MDXRenderer>
						</MDXProvider>
					</Columns>
				</Article>
			</SiteContainer>
		</StoryWrapper>
	);
}
