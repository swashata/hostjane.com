import React, { useState } from 'react';
import styled from 'styled-components';

import { iconMaps } from '../SiteNav';

const AlertWrap = styled.div`
	position: relative;
	display: flex;
	flex-flow: row nowrap;
	position: relative;
	padding: 16px;
	margin: 0 0 ${props => props.theme.gutter.small}px 0;
	border: 1px solid ${props => props.theme.primary.border};
	background: ${props => props.theme.primary.background};
	border-radius: 2px;
	align-items: center;
	@media ${props => props.theme.breakpoints.tabletQuery} {
		margin-bottom: ${props => props.theme.gutter.large}px;
		padding: 16px 24px;
	}
`;

const AlertIcon = styled.div`
	display: none;
	@media ${props => props.theme.breakpoints.tabletQuery} {
		display: block;
		width: 112px;
		height: 112px;
		flex: 0 0 112px;
		margin: 0 20px 0 0;
		svg {
			height: 112px;
			width: 112px;
		}
	}
`;

const AlertBody = styled.div`
	flex: 0 0 100%;
	@media ${props => props.theme.breakpoints.tabletQuery} {
		flex-basis: calc(100% - 132px); /* 112px svg and 20px margin */
		margin: 20px 0;
	}
`;

const AlertTitle = styled.h4`
	margin: 0 0 16px 0;
	line-height: 1.2;
	font-size: 18px;
	color: ${props => props.theme.neutrals.darker};
`;

const AlertMessage = styled.div`
	font-size: 16px;
	line-height: 1.5;
	color: ${props => props.theme.neutrals.secondaryText};
`;

const AlertDismissButton = styled.button`
	position: absolute;
	display: flex;
	align-items: center;
	justify-content: center;
	top: 4px;
	right: 4px;
	height: 24px;
	width: 24px;
	font-size: 22px;
	text-align: center;
	line-height: 1;
	color: ${props => props.theme.neutrals.secondaryText};
	background: transparent;
	border: 0 none;
	padding: 0;
	margin: 0;
	cursor: pointer;
	&:hover {
		color: ${props => props.theme.neutrals.darker};
	}
	@media ${props => props.theme.breakpoints.tabletQuery} {
		font-size: 28px;
		height: 32px;
		width: 32px;
	}
`;

export default function Alert({ icon, title, message }) {
	const [hide, setHide] = useState(false);
	if (hide) {
		return null;
	}
	return (
		<AlertWrap>
			<AlertDismissButton
				type="button"
				onClick={e => {
					e.preventDefault();
					setHide(true);
				}}
			>
				&times;
			</AlertDismissButton>
			<AlertIcon>{iconMaps[icon]}</AlertIcon>
			<AlertBody>
				<AlertTitle>{title}</AlertTitle>
				<AlertMessage dangerouslySetInnerHTML={{ __html: message }} />
			</AlertBody>
		</AlertWrap>
	);
}
