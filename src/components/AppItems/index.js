import React from 'react';
import styled from 'styled-components';
import classNames from 'classnames';

import { ReactComponent as CPanelWHMIcon } from '../../svgs/wizard/Apps/CPanelWHM.svg';
import { ReactComponent as DockerIcon } from '../../svgs/wizard/Apps/Docker.svg';
import { ReactComponent as DrupalIcon } from '../../svgs/wizard/Apps/Drupal.svg';
import { ReactComponent as GitLabIcon } from '../../svgs/wizard/Apps/GitLab.svg';
import { ReactComponent as JoomlaIcon } from '../../svgs/wizard/Apps/Joomla.svg';
import { ReactComponent as LAMPIcon } from '../../svgs/wizard/Apps/LAMP.svg';
import { ReactComponent as LEMPIcon } from '../../svgs/wizard/Apps/LEMP.svg';
import { ReactComponent as MagentoIcon } from '../../svgs/wizard/Apps/Magento.svg';
import { ReactComponent as MediaWikiIcon } from '../../svgs/wizard/Apps/MediaWiki.svg';
import { ReactComponent as MineCraftIcon } from '../../svgs/wizard/Apps/MineCraft.svg';
import { ReactComponent as NextCloudIcon } from '../../svgs/wizard/Apps/NextCloud.svg';
import { ReactComponent as NoApplicationIcon } from '../../svgs/wizard/Apps/NoApplication.svg';
import { ReactComponent as OpenVPNIcon } from '../../svgs/wizard/Apps/OpenVPN.svg';
import { ReactComponent as OwnCloudIcon } from '../../svgs/wizard/Apps/OwnCloud.svg';
import { ReactComponent as PleskWebAdminSEIcon } from '../../svgs/wizard/Apps/PleskWebAdminSE.svg';
import { ReactComponent as PleskWebHostIcon } from '../../svgs/wizard/Apps/PleskWebHost.svg';
import { ReactComponent as PleskWebProIcon } from '../../svgs/wizard/Apps/PleskWebPro.svg';
import { ReactComponent as PrestaShopIcon } from '../../svgs/wizard/Apps/PrestaShop.svg';
import { ReactComponent as WebminIcon } from '../../svgs/wizard/Apps/Webmin.svg';
import { ReactComponent as WordPressIcon } from '../../svgs/wizard/Apps/WordPress.svg';
import { ReactComponent as CentOSWPLIcon } from '../../svgs/wizard/Apps/CentOS.svg';
import { ReactComponent as CentOSIcon } from '../../svgs/wizard/OS/centOS.svg';
import { ReactComponent as UbuntuIcon } from '../../svgs/wizard/OS/ubuntu.svg';
import { ReactComponent as FreeBSDIcon } from '../../svgs/wizard/OS/freeBSD.svg';
import { ReactComponent as FedoraIcon } from '../../svgs/wizard/OS/fedora.svg';
import { ReactComponent as DebianIcon } from '../../svgs/wizard/OS/debian.svg';
import { ReactComponent as CoreOSIcon } from '../../svgs/wizard/OS/coreOS.svg';

const iconMap = {
	cpanelwhm: <CPanelWHMIcon />,
	docker: <DockerIcon />,
	drupal: <DrupalIcon />,
	gitlab: <GitLabIcon />,
	joomla: <JoomlaIcon />,
	lamp: <LAMPIcon />,
	lemp: <LEMPIcon />,
	magento: <MagentoIcon />,
	mediawiki: <MediaWikiIcon />,
	minecraft: <MineCraftIcon />,
	nextcloud: <NextCloudIcon />,
	noapplication: <NoApplicationIcon />,
	openvpn: <OpenVPNIcon />,
	owncloud: <OwnCloudIcon />,
	pleskwebadminse: <PleskWebAdminSEIcon />,
	pleskwebhost: <PleskWebHostIcon />,
	pleskwebpro: <PleskWebProIcon />,
	prestashop: <PrestaShopIcon />,
	webmin: <WebminIcon />,
	wordpress: <WordPressIcon />,
	centoswpl: <CentOSWPLIcon />,
	centos: <CentOSIcon />,
	ubuntu: <UbuntuIcon />,
	freebsd: <FreeBSDIcon />,
	fedora: <FedoraIcon />,
	debian: <DebianIcon />,
	coreos: <CoreOSIcon />,
};

export { ItemsGrid as AppItems } from '../ItemsGrid';

const AppIcon = styled.div`
	height: 56px;
	width: 56px;
	flex: 0 0 56px;
	svg {
		height: 100%;
		width: 100%;
		transition: filter 300ms ${props => props.theme.effects.easeOutSine};
		filter: grayscale(1);
	}
	position: relative; /* because we have a pos-absolute backdrop with gradient */
`;

const AppHeading = styled.header`
	flex: 0 0 calc(100% - 72px);
	margin: 0 0 0 16px;
	position: relative; /* because we have a pos-absolute backdrop with gradient */
`;

const AppSubtitle = styled.h4`
	font-size: 12px;
	line-height: 12px;
	color: #7b8794;
	margin: 10px 0 0;
	padding: 0;
	font-weight: 400;
`;

const AppTitle = styled.h3`
	font-size: 22px;
	line-height: 1;
	font-weight: bold;
	color: ${props => props.theme.neutrals.darkest};
	margin: 0;
	padding: 0;
`;

const AppLicense = styled.div`
	font-size: 12px;
	position: absolute;
	bottom: 8px;
	right: 8px;
	color: ${props => props.theme.neutrals.light};
	font-style: italic;
`;

const AppItemWrap = styled.div`
	min-height: 85px;
	padding: 8px;
	display: flex;
	flex-flow: row wrap;
	align-items: center;
	cursor: pointer;
	background: ${props => props.theme.background};
	box-shadow: 0 0 0 1px ${props => props.theme.neutrals.lightTwo},
		0 0 4px 0 transparent;
	transition: all 300ms ${props => props.theme.effects.easeOutSine};
	border-radius: 2px;
	position: relative;

	&::before {
		position: absolute;
		content: '';
		display: block;
		top: 0;
		left: 0;
		right: 0;
		bottom: 0;
		background-image: linear-gradient(
			45deg,
			${props => props.theme.primary.darkest} 0%,
			${props => props.theme.primary.light} 100%
		);
		opacity: 0;
		transition: all 300ms ${props => props.theme.effects.easeOutSine};
		border-radius: 2px;
	}

	&:hover {
		box-shadow: 0 1px 2px 0 ${props => props.theme.neutrals.lightOne},
			0 0 4px 0 ${props => props.theme.neutrals.lightOne};
		${AppIcon} {
			svg {
				filter: grayscale(0);
			}
		}
	}

	&&:active,
	&&.active {
		box-shadow: 0 1px 2px 0 ${props => props.theme.neutrals.lightOne},
			0 0 6px 0 ${props => props.theme.neutrals.lightOne};
		${AppIcon} {
			svg {
				filter: grayscale(0);
			}
		}
	}
	&.active {
		${AppTitle} {
			color: ${props => props.theme.neutrals.lightTwo};
		}
		${AppSubtitle} {
			color: ${props => props.theme.neutrals.lightTwo};
		}
		${AppLicense} {
			color: ${props => props.theme.neutrals.lightTwo};
		}
		&::before {
			opacity: 1;
		}
	}
`;

export function AppItem({
	active = false,
	license = false,
	subtitle = null,
	icon,
	title,
	onClick,
}) {
	return (
		<div>
			<AppItemWrap
				tabIndex="0"
				className={classNames({ active })}
				onClick={onClick}
			>
				<AppIcon>{iconMap[icon]}</AppIcon>
				<AppHeading>
					<AppTitle>{title}</AppTitle>
					{subtitle ? <AppSubtitle>{subtitle}</AppSubtitle> : null}
				</AppHeading>
				{license ? <AppLicense>includes license</AppLicense> : null}
			</AppItemWrap>
		</div>
	);
}
