import styled from 'styled-components';

const Banner = styled.div`
	background: ${props => props.theme.background};
	border-bottom: 1px solid ${props => props.theme.neutrals.lightOne};
	font-size: 14px;
	line-height: 1.5;
	color: ${props => props.theme.neutrals.secondaryText};
	margin: -${props => props.theme.gutter.small}px;
	margin-bottom: ${props => props.theme.gutter.small}px;
	padding: 8px 16px;

	@media ${props => props.theme.breakpoints.tabletQuery} {
		margin: -${props => props.theme.gutter.large}px;
		margin-bottom: ${props => props.theme.gutter.large}px;
		padding: 16px 32px;
	}

	> div {
		max-width: ${props => props.theme.maxWidth}px;
		margin: 0 auto;
	}

	.news-label {
		padding-right: 8px;
		color: ${props => props.theme.supporting.red};
		font-weight: 800;
		text-transform: uppercase;
	}
`;

export default Banner;
