import React from 'react';
import styled from 'styled-components';
import { Link } from 'gatsby';

import { iconMaps } from '../SiteNav';
import Tooltip from '../Tooltip';

export const BoxGrid = styled.section`
	display: grid;
	grid-gap: ${props => props.theme.gutter.small}px;
	grid-template-columns: 1fr;
	@media ${props => props.theme.breakpoints.tabletQuery} {
		grid-template-columns: 1fr 1fr;
	}
	@media ${props => props.theme.breakpoints.desktopQuery} {
		grid-template-columns: 1fr 1fr 1fr 1fr;
	}
`;

const BoxIcon = styled.div`
	height: 48px;
	width: 48px;
	flex: 0 0 64px;
	margin: 0 15px 0 0;
	svg {
		height: 100%;
		width: 100%;
		transition: filter 300ms ${props => props.theme.effects.easeOutSine};
		filter: grayscale(1);
	}

	@media ${props => props.theme.breakpoints.tabletQuery} {
		height: 64px;
		width: 64px;
		flex-basis: 80px;
		margin: 0 0 15px 0;
	}
`;

const BoxTitle = styled.h2`
	flex: 0 0 calc(100% - 79px);
	font-size: 18px;
	text-align: left;
	margin: 0;
	color: ${props => props.theme.neutrals.darker};
	transition: all 300ms ${props => props.theme.effects.easeOutSine};
	@media ${props => props.theme.breakpoints.tabletQuery} {
		font-size: 22px;
		flex-basis: 100%;
		text-align: center;
	}
`;

const BoxLinkWrap = styled.div`
	background: ${props => props.theme.background};
	box-shadow: 0 0 0 1px ${props => props.theme.neutrals.lightTwo},
		0 0 4px 0 transparent;
	transition: all 300ms ${props => props.theme.effects.easeOutSine};
	border-radius: 2px;
	height: 80px;
	display: flex;
	flex-flow: row wrap;
	align-items: center;
	justify-content: flex-start;
	padding: 15px;

	@media ${props => props.theme.breakpoints.tabletQuery} {
		height: auto;
		padding: 30px 15px;
		justify-content: center;
	}

	&:hover {
		box-shadow: 0 1px 2px 0 ${props => props.theme.neutrals.lightOne},
			0 0 4px 0 ${props => props.theme.neutrals.lightOne};
		${BoxIcon} {
			svg {
				filter: grayscale(0);
			}
		}
		${BoxTitle} {
			color: ${props => props.theme.primary.dark};
		}
	}

	&:active,
	&.active {
		box-shadow: 0 1px 2px 0 ${props => props.theme.neutrals.lightOne},
			0 0 6px 0 ${props => props.theme.neutrals.lightOne};
		${BoxIcon} {
			svg {
				filter: grayscale(0);
			}
		}
	}
`;

export function BoxLink({ title, link, icon, desc }) {
	return (
		<Tooltip label={desc} interactive={false}>
			<Link
				to={link}
				style={{ textDecoration: 'none', display: 'block' }}
			>
				<BoxLinkWrap>
					<BoxIcon>{iconMaps[icon]}</BoxIcon>
					<BoxTitle>{title}</BoxTitle>
				</BoxLinkWrap>
			</Link>
		</Tooltip>
	);
}
