import styled from 'styled-components';

export default styled.div`
	max-width: ${props => (props.fullWidth ? '100%' : props.theme.maxWidth)}px;
	margin: 0 auto;
`;
