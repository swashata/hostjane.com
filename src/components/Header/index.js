import React from 'react';
import styled from 'styled-components';

import Tooltip from '../Tooltip';

const Heading = styled.h1`
	font-size: 22px;
	line-height: 1.2;
	margin: 0 0 ${props => props.theme.gutter.small}px 0;
	color: ${props => props.theme.neutrals.secondaryText};
	font-weight: bold;
	display: flex;
	align-items: center;

	[data-tooltipped] {
		height: 18px;
		display: flex !important;
		width: 18px;
		margin-left: 10px;
	}

	@media ${props => props.theme.breakpoints.tabletQuery} {
		font-size: 32px;
		margin-bottom: ${props => props.theme.gutter.large}px;
		[data-tooltipped] {
			margin-top: 6px;
		}
	}
`;

const Description = styled.div`
	color: ${props => props.theme.neutrals.light};
	font-size: 14px;
	line-height: 1.5em;
	margin: -${props => props.theme.gutter.small / 2}px 0 ${props =>
			props.theme.gutter.small}px 0;
	p {
		margin: 0 0 1em 0;
	}
	@media ${props => props.theme.breakpoints.tabletQuery} {
		font-size: 18px;
		margin: -${props => props.theme.gutter.large / 2}px 0 ${props =>
				props.theme.gutter.large}px 0;
	}
`;

const TooltipButton = styled.button`
	border: 0 none;
	margin: 0;
	padding: 0;
	height: 18px;
	width: 18px;
	background: ${props => props.theme.neutrals.lightTwo};
	color: ${props => props.theme.neutrals.light};
	font-size: 10px;
	line-height: 18px;
	text-align: center;
	border-radius: 2px;
	cursor: pointer;
	&:hover {
		background-color: ${props => props.theme.primary.dark};
		color: ${props => props.theme.neutrals.lightest};
	}
`;

export default function Header({
	tooltip = null,
	description = null,
	children,
}) {
	return (
		<header>
			<Heading className="siteheader">
				<span className="siteheader__title">{children}</span>
				{tooltip !== null ? (
					<Tooltip label={tooltip}>
						<TooltipButton type="button">?</TooltipButton>
					</Tooltip>
				) : null}
			</Heading>
			{description !== null ? (
				<Description>{description}</Description>
			) : null}
		</header>
	);
}
