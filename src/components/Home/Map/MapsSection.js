/* eslint-disable jsx-a11y/mouse-events-have-key-events */
import React, { useState } from 'react';
import {
	ComposableMap,
	Geographies,
	Geography,
	Marker,
} from 'react-simple-maps';
import styled from 'styled-components';
import classNames from 'classnames';
import ResizeObserver from 'resize-observer-polyfill';
import useMeasure from 'react-use-measure';

const geoUrl = '/world-continents.json';
// const geoUrl =
// 	'https://raw.githubusercontent.com/zcreativelabs/react-simple-maps/master/topojson-maps/world-110m.json';

const TooltipContainer = styled.div`
	position: absolute;
	left: ${props =>
		props.on === 'right' ? `${props.offset + 40}px` : 'auto'};
	right: ${props =>
		props.on === 'right'
			? 'auto'
			: `${props.parentWidth - props.offset + 40 - 24}px`};
	top: ${props => props.top - 8}px;
	display: inline-block;
	padding: 10px;
	max-width: 250px;
	z-index: 9;
	background-color: #fff;
	order: 1px solid #ccc;
	border-radius: 3px;
	box-shadow: 0 2px 4px rgba(0, 0, 0, 0.13);
	border: 1px solid #ccc;
	animation: anim-popover-aui-show-left 0.13s ease-in-out 0s 1 normal both;

	.hostjane-map-tooltip__label {
		margin: 0;
		line-height: 27px;
		font-size: 21px;
		font-weight: 700;
	}

	.hostjane-map-tooltip__region {
		padding-top: 5px;
		border-top: 1px solid #f2f4f4;
		margin: 5px 0 0 0;
		color: #879196;
		font-size: 14px;
	}

	.hostjane-map-tooltip__arrow {
		position: absolute;
		width: 0;
		height: 0;
		line-height: 0;
		font-size: 0;
		border: 9px solid transparent;
		border-left-width: 0;
		left: -9px;
		border-right: 9px solid #ccc;
		top: 10px;
		margin-top: 0;
	}
	.hostjane-map-tooltip__arrowinner {
		position: absolute;
		width: 0;
		height: 0;
		line-height: 0;
		font-size: 0;
		border: 9px solid transparent;
		border-left-width: 0;
		border-right: 9px solid #fff;
		top: -9px;
		left: 1px;
	}

	&.hostjane-map-tooltip--left {
		animation-name: anim-popover-aui-show-right;
		.hostjane-map-tooltip__arrow {
			border-right-width: 0;
			right: -9px;
			left: auto;
			border-left: 9px solid #ccc;
		}
		.hostjane-map-tooltip__arrowinner {
			border: 9px solid transparent;
			border-right-width: 0;
			border-left: 9px solid #fff;
			left: -9px;
		}
	}

	@keyframes anim-popover-aui-show-left {
		0% {
			opacity: 0;
			transform: translate3d(14px, 0, 0);
		}

		100% {
			opacity: 1;
			transform: translateZ(0);
		}
	}
	@keyframes anim-popover-aui-show-right {
		0% {
			opacity: 0;
			transform: translate3d(-14px, 0, 0);
		}

		100% {
			opacity: 1;
			transform: translateZ(0);
		}
	}
`;
function CustomTooltip({ label, region, x, y, parentBounds }) {
	if (label && region) {
		const offset = x;
		let on = 'right';
		const top = y;
		if (offset + 250 + 100 > parentBounds.width) {
			on = 'left';
		}
		return (
			<TooltipContainer
				className={classNames(
					'hostjane-map-tooltip',
					`hostjane-map-tooltip--${on}`
				)}
				offset={offset}
				top={top}
				on={on}
				parentWidth={parentBounds.width}
			>
				<h2 className="hostjane-map-tooltip__label">{label}</h2>
				<p className="hostjane-map-tooltip__region">{region}</p>
				<div className="hostjane-map-tooltip__arrow">
					<div className="hostjane-map-tooltip__arrowinner" />
				</div>
			</TooltipContainer>
		);
	}
	return null;
}

function CustomMarker({
	longitude,
	latitude,
	label,
	region,
	setTooltipState,
	parentBounds,
}) {
	const [ref, bounds] = useMeasure({
		debounce: 0,
		polyfill: ResizeObserver,
		scroll: true,
	});

	const marketClassName = classNames('hostjane-map__marker-activity');
	const [active, setActive] = useState(false);

	return (
		<Marker
			coordinates={[longitude, latitude]}
			// using combination of mouseOver and mouseLeave for perf
			// mouseOver fires when the mouse moves and is over this
			onMouseEnter={() => {
				if (!active) {
					setTooltipState({
						label,
						region,
						x: bounds.left - parentBounds.left,
						y: bounds.top - parentBounds.top,
					});
					setActive(true);
				}
			}}
			onMouseOver={() => {
				if (!active) {
					setTooltipState({
						label,
						region,
						x: bounds.left - parentBounds.left,
						y: bounds.top - parentBounds.top,
					});
					setActive(true);
				}
			}}
			// mouseLeave fires only once when mouse leaves this
			onMouseLeave={() => {
				setTooltipState({
					label: null,
					region: null,
					x: null,
					y: null,
				});
				setActive(false);
			}}
			className="hostjane-map__map-marker"
		>
			<g ref={ref}>
				<circle strokeWidth={0} r={8} fill="transparent" />
				<circle
					className={marketClassName}
					stroke="#ffa724"
					strokeWidth={3.5}
					r={9}
					fill="transparent"
				/>
				<circle
					className={marketClassName}
					stroke="#eb5f07"
					strokeWidth={4}
					r={5.6}
					fill="transparent"
				/>
				<circle
					className="marker-circle"
					stroke="#ffa724"
					strokeWidth={2}
					r={3}
					fill="transparent"
				/>
			</g>
		</Marker>
	);
}

const StyledComposableMap = styled(ComposableMap)`
	display: block;
	margin: 0 auto 40px;
	fill: #48637f;

	.hostjane-map__geo-path {
		fill: #48637f;
		transition: fill 500ms 300ms ease-out;
		/* cursor: pointer; */

		&:hover {
			fill: #99cbe4;
		}
	}

	.hostjane-map__map-marker {
		cursor: pointer;
		position: relative;
		z-index: 10; /** 1 more than tooltip container */

		circle.hostjane-map__marker-activity {
			transition: transform 200ms ease-out, opacity 200ms ease-out;
			transform: scale(0, 0);
			opacity: 0;
		}

		&:hover {
			z-index: 11;
			circle.hostjane-map__marker-activity {
				opacity: 1;
				transform: scale(1, 1);
			}
		}
	}
`;

export default function LazyLoaded({ bounds, data }) {
	const [tooltipState, setTooltipState] = useState({
		label: null,
		region: null,
		x: null,
		y: null,
	});

	const [activeContinent, setActiveContinent] = useState(null);

	return (
		<>
			<StyledComposableMap
				className="hostjane-map"
				projectionConfig={{
					scale: 150,
					center: [10, -26],
					rotate: [-10, 0, 0],
				}}
				viewBox="0 0 800 400"
				projection="geoEqualEarth"
			>
				<Geographies geography={geoUrl}>
					{({ geographies }) =>
						geographies.map(geo => {
							return (
								<Geography
									key={geo.rsmKey}
									geography={geo}
									className="hostjane-map__geo-path"
									onClick={e => {
										e.preventDefault();
										setActiveContinent(con => {
											if (
												con === geo.properties.CONTINENT
											) {
												return null;
											}
											return geo.properties.CONTINENT;
										});
									}}
								/>
							);
						})
					}
				</Geographies>
				{data.markers.map(mark => (
					<CustomMarker
						key={`${mark.latitude}-${mark.longitude}`}
						longitude={mark.longitude}
						latitude={mark.latitude}
						label={mark.label}
						region={mark.region}
						setTooltipState={setTooltipState}
						parentBounds={bounds}
					/>
				))}
			</StyledComposableMap>

			<CustomTooltip {...tooltipState} parentBounds={bounds} />
		</>
	);
}
