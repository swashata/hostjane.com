import React, { lazy, Suspense } from 'react';
import styled from 'styled-components';
import classNames from 'classnames';
import ResizeObserver from 'resize-observer-polyfill';
import useMeasure from 'react-use-measure';

import { useIsTablet } from '../../../utils/breakpoints';
import SiteContainer from '../../SiteContainer';
import { Heading, Wrapper, Description, StyledAnchor } from '../common';

const MapWrapper = styled(Wrapper)`
	background-color: rgb(30, 40, 50);
	position: relative;

	${Heading} {
		color: #fff;
	}

	${Description} {
		color: #fff;
	}
`;

const MapLinks = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;
	flex-flow: row wrap;
`;

const Loader = styled.div`
	padding: 40px;
	display: flex;
	height: 200px;
	justify-content: center;
	align-items: center;
	background-color: rgb(30, 40, 50);
	color: #fff;
	margin-bottom: 30px;
`;

const MapsSection = lazy(() => import('./MapsSection.js'));

export default function Map({ data }) {
	const isTablet = useIsTablet();

	const [ref, bounds] = useMeasure({
		debounce: 0,
		polyfill: ResizeObserver,
		scroll: true,
	});

	return (
		<MapWrapper ref={ref}>
			<SiteContainer>
				<Heading>{data.title}</Heading>
				<Description>{data.description}</Description>
				{isTablet ? (
					<Suspense
						fallback={<Loader>Updating server data...</Loader>}
					>
						<MapsSection data={data} bounds={bounds} />
					</Suspense>
				) : null}
				<MapLinks>
					{data.buttons.map((button, index) => (
						<StyledAnchor
							// eslint-disable-next-line react/no-array-index-key
							key={index}
							href={button.link}
							className={classNames(
								'hostjane-button',
								`hostjane-button--${button.type || 'secondary'}`
							)}
						>
							{button.label}
						</StyledAnchor>
					))}
				</MapLinks>
			</SiteContainer>
		</MapWrapper>
	);
}
