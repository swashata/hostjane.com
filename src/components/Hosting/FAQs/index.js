import React, { useMemo } from 'react';

import SectionWrapper from '../../SectionWrapper';
import FAQList from '../../FAQList';
import SiteContainer from '../../SiteContainer';

export default function FAQs({ faqs, heading, id = undefined }) {
	const list = useMemo(
		() =>
			faqs.map(faq => ({
				title: faq.node.frontmatter.title,
				content: faq.node.body,
				id: faq.node.id,
			})),
		[faqs]
	);
	return (
		<SectionWrapper id={id}>
			<SiteContainer>
				<FAQList list={list} heading={heading} />
			</SiteContainer>
		</SectionWrapper>
	);
}
