import React from 'react';
import styled from 'styled-components';

import SectionWrapper from '../../SectionWrapper';
import SiteContainer from '../../SiteContainer';
import Alert from '../../Alert';

const HostingAlertWrapper = styled.div`
	padding: 0 36px;
	@media ${props => props.theme.breakpoints.desktopQuery} {
		padding: 0 46px;
	}
`;

export default function HostingAlert(props) {
	return (
		<SectionWrapper>
			<SiteContainer>
				<HostingAlertWrapper>
					<Alert {...props} />
				</HostingAlertWrapper>
			</SiteContainer>
		</SectionWrapper>
	);
}
