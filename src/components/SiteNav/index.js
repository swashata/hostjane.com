/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import styled from 'styled-components';
import { Link, graphql, useStaticQuery } from 'gatsby';
import { useLocation } from '@reach/router';

import { ReactComponent as HostJaneIcon } from '../../svgs/nav/HostJane.svg';
import { ReactComponent as JaneCloudIcon } from '../../svgs/nav/JaneCloud.svg';
import { ReactComponent as JaneDediIcon } from '../../svgs/nav/JaneDedi.svg';
import { ReactComponent as JaneSellsIcon } from '../../svgs/nav/JaneSells.svg';
import { ReactComponent as JaneVPSIcon } from '../../svgs/nav/JaneVPS.svg';
import { ReactComponent as AccountIcon } from '../../svgs/homenav/account.svg';
import { ReactComponent as CategoriesIcon } from '../../svgs/homenav/categories.svg';
import { ReactComponent as MessagesIcon } from '../../svgs/homenav/messages.svg';
import { ReactComponent as SkillsSidebarIcon } from '../../svgs/homenav/skills-sidebar.svg';
import { ReactComponent as WebHostingIcon } from '../../svgs/homenav/web-hosting.svg';

const Nav = styled.nav`
	position: fixed;
	z-index: 9999;
	bottom: 0;
	width: 100%;
	height: 64px;
	background: ${props => props.theme.background};
	box-shadow: 2px -1px 2px 0 ${props => props.theme.neutrals.lightOne},
		6px 0 4px 0 ${props => props.theme.neutrals.lightOne};

	ul {
		display: grid;
		list-style: none;
		margin: 0;
		padding: 0;
		line-height: 1.4; /** match with marketplace site */
		/* We put at max five icons in a row */
		grid-template-columns: 1fr 1fr 1fr 1fr 1fr;

		.sitenav__link {
			padding: 4px;
			text-decoration: none;
			color: ${props => props.theme.neutrals.secondaryText};
			display: flex;
			width: 100%;
			flex-flow: row wrap;
			align-items: center;
			justify-content: center;
			opacity: 0.75;
			transition: opacity 200ms ease-out;
			min-height: 64px;
			&:hover {
				opacity: 1;
			}
			&.sitenav__link--home {
				opacity: 1;
			}
			&.sitenav__link--active {
				opacity: 1;
				font-weight: bold;
				background-color: ${props => props.theme.neutrals.lightest};
				color: ${props => props.theme.supporting.icon};
			}
		}
		.sitenav__icon {
			height: 42px;
			width: 42px;
			margin-top: auto;

			.sitenav__largeicon {
				height: 28px;
				width: 28px;
				margin: 7px auto;
				display: block;
			}
		}
		.sitenav__label {
			flex-basis: 100%;
			text-align: center;
			font-size: 10px;
			margin-bottom: auto;
		}
	}

	/** Fix it as sidebar on larger devices */
	@media ${props => props.theme.breakpoints.tabletQuery} {
		/* static position because from this width, it will managed by upper container */
		position: static;
		width: 110px;
		height: 100%; /* reset the height from 64px, grid will make sure it takes all space */
		min-height: 590px; /* height of nav bar with 5 items in it */
		box-shadow: 1px 0 2px 0 ${props => props.theme.neutrals.lightOne},
			0 0 6px 0 ${props => props.theme.neutrals.lightOne};
		transform: translateZ(0);

		ul {
			grid-template-columns: 1fr;
			position: sticky;
			top: 0;

			.sitenav__link {
				height: 118px;
				width: 100%;
			}

			.sitenav__icon {
				height: 64px;
				width: 64px;

				.sitenav__largeicon {
					height: 40px;
					width: 40px;
					margin: 10px auto;
				}
			}
			.sitenav__label {
				font-size: 14px;
				font-weight: bold;
			}
		}
	}
`;

const NavUlWrap = styled.div`
	@media ${props => props.theme.breakpoints.tabletQuery} {
		/* we want the menu to be stuck to top */
		position: fixed;
		top: 0;
		width: 110px;
		height: 100%;
	}
`;

export const iconMaps = {
	hostjane: <HostJaneIcon className="sitenav__icon" />,
	cloud: <JaneCloudIcon className="sitenav__icon" />,
	vps: <JaneVPSIcon className="sitenav__icon" />,
	dedicated: <JaneDediIcon className="sitenav__icon" />,
	reseller: <JaneSellsIcon className="sitenav__icon" />,
	accountIcon: (
		<span className="sitenav__icon">
			<AccountIcon className="sitenav__largeicon" />
		</span>
	),
	categoriesIcon: (
		<span className="sitenav__icon">
			<CategoriesIcon className="sitenav__largeicon" />
		</span>
	),
	messagesIcon: (
		<span className="sitenav__icon">
			<MessagesIcon className="sitenav__largeicon" />
		</span>
	),
	skillsSidebarIcon: (
		<span className="sitenav__icon">
			<SkillsSidebarIcon className="sitenav__largeicon" />
		</span>
	),
	webHostingIcon: (
		<span className="sitenav__icon">
			<WebHostingIcon className="sitenav__largeicon" />
		</span>
	),
};

const NAV_QUERY = graphql`
	query NavQuery {
		site {
			siteMetadata {
				navMainLink
				navMainLinkLabel
				navMainAltLink
				navMainAltLinkLabel
				nav {
					path
					icon
					label
					altLabel
				}
				mainSidebarNav {
					link
					internal
					icon
					label
				}
			}
		}
	}
`;

export function HomePageSideNav() {}

export default function SiteNav() {
	const data = useStaticQuery(NAV_QUERY);
	const location = useLocation();

	let wizardMainLink = data.site.siteMetadata.navMainLink;
	let wizardMainLabel = data.site.siteMetadata.navMainLinkLabel;

	if (
		location.pathname &&
		location.pathname !== '/hosting/' &&
		location.pathname !== '/hosting'
	) {
		wizardMainLink = data.site.siteMetadata.navMainAltLink;
		wizardMainLabel = data.site.siteMetadata.navMainAltLinkLabel;
	}

	return (
		<Nav id="hostjane-nav" className="sitenav">
			<NavUlWrap>
				<ul>
					{location.pathname &&
					location.pathname.includes('/hosting') ? (
						<>
							<li>
								<Link
									to={wizardMainLink}
									className="sitenav__link sitenav__link--home"
									activeClassName="sitenav__link--active"
								>
									{iconMaps.hostjane}
									<span className="sitenav__label">
										{wizardMainLabel}
									</span>
								</Link>
							</li>
							{data.site.siteMetadata.nav.map(navItem => (
								<li key={navItem.path}>
									<Link
										to={navItem.path}
										className="sitenav__link"
										activeClassName="sitenav__link--active"
									>
										{iconMaps[navItem.icon]}
										<span className="sitenav__label">
											{navItem.label}
										</span>
									</Link>
								</li>
							))}
						</>
					) : (
						<>
							{data.site.siteMetadata.mainSidebarNav.map(
								navItem => (
									<li key={navItem.link}>
										{navItem.internal ? (
											<Link
												to={navItem.link}
												className="sitenav__link"
												activeClassName="sitenav__link--active"
											>
												{iconMaps[navItem.icon]}
												<span className="sitenav__label">
													{navItem.label}
												</span>
											</Link>
										) : (
											<a
												href={navItem.link}
												className="sitenav__link"
											>
												{iconMaps[navItem.icon]}
												<span className="sitenav__label">
													{navItem.label}
												</span>
											</a>
										)}
									</li>
								)
							)}
						</>
					)}
				</ul>
			</NavUlWrap>
		</Nav>
	);
}
