import React from 'react';
import { Tooltip as TippyTooltip } from 'react-tippy';

import 'react-tippy/dist/tippy.css';

export default function Tooltip({ children, label, interactive = true }) {
	return (
		<TippyTooltip
			size="regular"
			theme="light"
			arrow
			arrowSize="big"
			distance={20}
			inertia
			position="bottom"
			trigger={interactive ? 'click' : 'mouseenter focus'}
			interactive={interactive}
			html={
				<div
					style={{ maxWidth: '300px' }}
					dangerouslySetInnerHTML={{ __html: label }}
				/>
			}
		>
			{children}
		</TippyTooltip>
	);
}
