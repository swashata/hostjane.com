import styled from 'styled-components';

const TypoContent = styled.article`
	/*! Typebase.less v0.1.0 | MIT License */
	color: ${props => props.theme.neutrals.darker};
	line-height: 1.5rem;
	/* Copy & Lists */
	p {
		line-height: 1.5rem;
		margin-top: 1.5rem;
		margin-bottom: 0;
	}
	ul,
	ol {
		margin-top: 1.5rem;
		margin-bottom: 1.5rem;
	}
	ul li,
	ol li {
		line-height: 1.5rem;
	}
	ul ul,
	ol ul,
	ul ol,
	ol ol {
		margin-top: 0;
		margin-bottom: 0;
	}
	blockquote {
		line-height: 1.5rem;
		margin-top: 1.5rem;
		margin-bottom: 1.5rem;
	}
	/* Headings */
	h1,
	h2,
	h3,
	h4,
	h5,
	h6 {
		/* Change heading typefaces here */
		margin-top: 1.5rem;
		margin-bottom: 0;
		line-height: 1.5rem;
		color: ${props => props.theme.neutrals.darkest};
		text-transform: uppercase;
	}
	h1 {
		font-size: 3.242rem;
		line-height: 4.5rem;
		margin-top: 3rem;
	}
	h2 {
		font-size: 2rem;
		line-height: 3rem;
		margin-top: 3rem;
	}
	h3 {
		font-size: 1.414rem;
	}
	h4 {
		font-size: 0.707rem;
	}
	h5 {
		font-size: 0.4713333333333333rem;
	}
	h6 {
		font-size: 0.3535rem;
	}
	/* Tables */
	table {
		margin-top: 1.5rem;
		border-spacing: 0px;
		border-collapse: collapse;
	}
	table td,
	table th {
		padding: 0;
		line-height: 33px;
	}
	/* Code blocks */
	code {
		vertical-align: bottom;
	}
	/* Leading paragraph text */
	.lead {
		font-size: 1.414rem;
	}
	/* Hug the block above you */
	.hug {
		margin-top: 0;
	}
	pre {
		max-width: 100%;
		max-height: 400px;
		overflow: auto;
	}
	/* Images and SVGs */
	img,
	svg {
		max-width: 100%;
		height: auto;
	}
`;

export default TypoContent;
