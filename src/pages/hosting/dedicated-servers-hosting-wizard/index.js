import React from 'react';
// the only component we need
import AppForm from '../../../components/AppForm';

// import data
import pricings from '../../../../form-data/janededi/pricings';
import data from '../../../../form-data/janededi';

export default function VPSHostingWizard(props) {
	return <AppForm pricings={pricings} data={data} {...props} />;
}
