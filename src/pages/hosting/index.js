import React from 'react';
import { graphql } from 'gatsby';

import Layout from '../../components/Layout';
import SEO from '../../components/SEO';
import MainNavigation from '../../components/MainNavigation';
import DismissableBanner from '../../components/DismissableBanner';
import MainFooter from '../../components/MainFooter';
import FooterCTA from '../../components/Home/FooterCTA';
import HostJaneHostingHero from '../../components/HostJaneHostingHero';
import ServerPlan from '../../components/Hosting/ServerPlan';

import data from '../../../form-data/index';
import footerCtaData from '../../../site-data/home/footer-cta';
import {
	Alert as TopAlert,
	backgroundColor,
	textColor,
	backgroundGradient,
} from '../../../site-data/hosting/alert';
import { buttons } from '../../../site-data/hosting/main-hero/buttons';
import { vpsBoxData } from '../../../site-data/hosting/vps-box';
import { screenshotsData } from '../../../site-data/hosting/screenshots';
import Screenshots from '../../components/Hosting/Screenshots';
import FAQs from '../../components/Hosting/FAQs';
import { faqData } from '../../../site-data/hosting/faqs';

export default function HostingIndex(props) {
	const {
		data: { mainHero, hostingScreenshots, hostingFAQs },
	} = props;
	return (
		<Layout {...props} hasFooter={false} banner={data.banner} fullWidth>
			<SEO title="VPS Hosting | Managed VPS and Cloud Servers" />
			<DismissableBanner
				backgroundColor={backgroundColor}
				textColor={textColor}
				backgroundGradient={backgroundGradient}
			>
				{TopAlert === null ? null : <TopAlert />}
			</DismissableBanner>
			<MainNavigation />
			<HostJaneHostingHero
				heading={mainHero.frontmatter.heading}
				content={mainHero.body}
				imageFluidData={
					mainHero.frontmatter.image.childImageSharp.fluid
				}
				linkLabel={mainHero.frontmatter.linkLabel}
				linkHash="#hostjane-buy-hosting"
				buttons={buttons}
			/>
			<ServerPlan
				heading={vpsBoxData.heading}
				subHeading={vpsBoxData.subheading}
				id="hostjane-buy-hosting"
				boxes={vpsBoxData.boxes}
				icon={data.alertIcon}
				message={data.alertMessage}
				title={data.alertTitle}
			/>
			<Screenshots
				heading={screenshotsData.heading}
				subHeading={screenshotsData.subHeading}
				screenshots={hostingScreenshots.edges}
				id="hostjane-manage-server"
			/>
			<FAQs
				id="hostjane-faqs"
				faqs={hostingFAQs.edges}
				heading={faqData.title}
			/>
			<FooterCTA data={footerCtaData} />
			<MainFooter />
		</Layout>
	);
}

export const pageQuery = graphql`
	query {
		site {
			siteMetadata {
				title
				nav {
					path
					icon
					title
					desc
				}
			}
		}
		mainHero: mdx(
			fileAbsolutePath: {
				glob: "**/site-data/hosting/main-hero/content.mdx"
			}
		) {
			body
			rawBody
			frontmatter {
				heading
				linkLabel
				image {
					childImageSharp {
						fluid(maxWidth: 1200) {
							...GatsbyImageSharpFluid_withWebp_noBase64
						}
					}
				}
			}
		}
		hostingScreenshots: allMdx(
			filter: {
				fileAbsolutePath: {
					glob: "**/site-data/hosting/screenshots/contents/**"
				}
			}
			sort: { order: ASC, fields: frontmatter___order }
		) {
			edges {
				node {
					id
					frontmatter {
						title
						image {
							childImageSharp {
								fluid(maxWidth: 800) {
									...GatsbyImageSharpFluid_withWebp_tracedSVG
								}
							}
						}
					}
					body
				}
			}
		}
		hostingFAQs: allMdx(
			filter: {
				fileAbsolutePath: {
					glob: "**/site-data/hosting/faqs/contents/**"
				}
			}
			sort: { order: ASC, fields: frontmatter___order }
		) {
			pageInfo {
				totalCount
			}
			edges {
				node {
					id
					frontmatter {
						title
					}
					body
				}
			}
		}
	}
`;
