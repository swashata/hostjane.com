import React from 'react';
// the only component we need
import AppForm from '../../../components/AppForm';

// import data
import pricings from '../../../../form-data/janesells/pricings';
import data from '../../../../form-data/janesells';

export default function VPSHostingWizard(props) {
	return <AppForm pricings={pricings} data={data} {...props} />;
}
