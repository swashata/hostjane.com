import React from 'react';
// the only component we need
import AppForm from '../../../components/AppForm';

// import data
import pricings from '../../../../form-data/janevps/pricings';
import data from '../../../../form-data/janevps';

export default function VPSHostingWizard(props) {
	return <AppForm pricings={pricings} data={data} {...props} />;
}
