import React from 'react';
import { graphql } from 'gatsby';

import Layout from '../components/Layout';
import SEO from '../components/SEO';
import DismissableBanner from '../components/DismissableBanner';
import MainNavigation from '../components/MainNavigation';
import Hero from '../components/Home/Hero';
import HowItWorks from '../components/Home/HowItWorks';
import Map from '../components/Home/Map';
import Testimonial from '../components/Home/Testimonial';
import ServiceSlider from '../components/Home/ServiceSlider';
import Explore from '../components/Home/Explore';
import Features from '../components/Home/Features';
import MainFooter from '../components/MainFooter';
import FooterCTA from '../components/Home/FooterCTA';

import { pageDescription, pageTitle } from '../../site-data/home/seo';
import {
	Alert,
	backgroundColor,
	textColor,
	backgroundGradient,
} from '../../site-data/home/alert';
import homeHeroData from '../../site-data/home/hero';
import mapData from '../../site-data/home/map';
import howItWorksData from '../../site-data/home/howitworks';
import testimonialData from '../../site-data/testimonials';
import {
	title as webMobileSwTitle,
	Description as WebMobileSwDescription,
	explore as webMobileSwExplore,
} from '../../site-data/home/web-mobile-sw-slider';
import {
	title as designArtVideoTitle,
	Description as designArtVideoDescription,
	explore as designArtVideoExplore,
} from '../../site-data/home/design-art-video-slider';
import exploreData from '../../site-data/home/explore-solutions';
import featuresData from '../../site-data/home/features';
import footerCtaData from '../../site-data/home/footer-cta';

function HomePage({ data, ...props }) {
	return (
		<Layout {...props} hasFooter={false} fullWidth>
			<SEO
				title={pageTitle}
				description={pageDescription}
				addPrefix={false}
			/>
			<DismissableBanner
				backgroundColor={backgroundColor}
				textColor={textColor}
				backgroundGradient={backgroundGradient}
			>
				{Alert === null ? null : <Alert />}
			</DismissableBanner>
			<MainNavigation />
			<Hero data={homeHeroData} />
			<ServiceSlider
				mode="dark"
				slides={data.webMobileSw.edges}
				title={webMobileSwTitle}
				Description={WebMobileSwDescription}
				explore={webMobileSwExplore}
			/>
			<HowItWorks data={howItWorksData} />
			<ServiceSlider
				slides={data.designArtVideo.edges}
				title={designArtVideoTitle}
				Description={designArtVideoDescription}
				explore={designArtVideoExplore}
			/>
			<Testimonial data={testimonialData} />
			<Explore data={exploreData} />
			<Map data={mapData} />
			<Features data={featuresData} />
			<FooterCTA data={footerCtaData} />
			<MainFooter />
		</Layout>
	);
}

export default HomePage;

export const pageQuery = graphql`
	query Services {
		webMobileSw: allMdx(
			sort: { fields: frontmatter___order, order: ASC }
			filter: {
				fileAbsolutePath: {
					glob: "**/web-mobile-sw-slider/contents/**"
				}
			}
		) {
			edges {
				node {
					id
					frontmatter {
						title
						link
						image {
							childImageSharp {
								fluid(maxHeight: 320, maxWidth: 320) {
									...GatsbyImageSharpFluid_withWebp_noBase64
								}
							}
						}
						isNew
						newBadge
					}
					body
				}
			}
		}
		designArtVideo: allMdx(
			sort: { fields: frontmatter___order, order: ASC }
			filter: {
				fileAbsolutePath: {
					glob: "**/design-art-video-slider/contents/**"
				}
			}
		) {
			edges {
				node {
					id
					frontmatter {
						title
						link
						image {
							childImageSharp {
								fluid(maxHeight: 320, maxWidth: 320) {
									...GatsbyImageSharpFluid_withWebp_noBase64
								}
							}
						}
						isNew
						newBadge
					}
					body
				}
			}
		}
	}
`;
